<?= $this->include('users/layout/header'); ?>

<!-- SECTION -->
<div class="section">
	<!-- container -->
	<div class="container">

		<?php if (isset($nama_barang)) : ?>
			<!-- Pemberitahuan sedang mencari -->
			<div class="row">
				<div id="store" class="col-md-9">
					<h5>Dalam pencarian "<?= $nama_barang; ?>"</h3>
				</div>

			</div>
		<?php endif; ?>
		<!-- row -->
		<div class="row">
			<!-- ASIDE -->
			<div id="aside" class="col-md-3">
				<!-- aside Widget -->
				<div class="aside">
					<h3 class="aside-title">Kategori</h3>
					<div class="checkbox-filter">
						<?php foreach ($kategori as $value) : ?>

							<div class="input-checkbox">
								<a href="<?= base_url() ?>/kategori/<?= $value['nama_kategori']; ?>">
									<label for="category-1">
										<span></span>
										<?= $value['nama_kategori']; ?>
										<!-- <small>(120)</small> -->
									</label>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<!-- /aside Widget -->

				<!-- aside Widget -->
				<!-- <div class="aside">
							<h3 class="aside-title">Price</h3>
							<div class="price-filter">
								<div id="price-slider"></div>
								<div class="input-number price-min">
									<input id="price-min" type="number" readonly>
								</div>
								<span>-</span>
								<div class="input-number price-max">
									<input id="price-max" type="number" readonly>
								</div>
							</div>
						</div> -->
				<!-- /aside Widget -->

				<!-- aside Widget -->
				<div class="aside">
					<h3 class="aside-title">Merk</h3>
					<div class="checkbox-filter">

						<?php foreach ($merk as $value) : ?>

							<div class="input-checkbox">
								<a href="<?= base_url() ?>/merk/<?= $value['nama_merk']; ?>">
									<label for="category-1">
										<span></span>
										<?= $value['nama_merk']; ?>
										<!-- <small>(120)</small> -->
									</label>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<!-- /ASIDE -->

			<!-- STORE -->
			<div id="store" class="col-md-9">
				<!-- store products -->
				<div class="row">
					<?php
					$no = 1;
					foreach ($barang as $value) : ?>
						<!-- product -->
						<div class="col-md-4 col-xs-6">
							<div class="product">
								<div class="product-img">
									<img src="<?= base_url(); ?>/upload/<?= $value['gambar'] ?>" height="230" alt="">
									<?php if ($value['stok'] == 0) {
									?>
										<div class="product-label">
											<span class="new">Habis</span>
										</div>
									<?php } else {
									?>
										<div class="product-label">
											<span class="new">Tersedia</span>
										</div>
									<?php } ?>
								</div>
								<div class="product-body">
									<?php $uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
									if ($uriSegments[1] == 'store') {
										echo "<p class='product-category'>PRODUK</p>";
									} else if ($uriSegments[1] == 'kategori') {
										echo "<p class='product-category'>$value[nama_kategori]</p>";
									} else {
										echo "<p class='product-category'>$value[nama_merk]</p>";
									}
									?>
									<h3 class="product-name"><a href="<?= base_url(); ?>/produk?id_barang=<?= $value['id_barang']; ?>"><?= $value['nama_barang']; ?></a></h3>
									<h4 class="product-price">Rp <?= number_format($value['harga_jual'], 0, ',', '.'); ?></h4>

								</div>
								<div class="add-to-cart">
									<button class="add-to-cart-btn"><a href="<?= base_url(); ?>/produk?id_barang=<?= $value['id_barang']; ?>"><i class="fa fa-shopping-cart"></i> add to cart</a></button>
								</div>
							</div>
						</div>
						<!-- /product -->
					<?php endforeach; ?>
				</div>
				<!-- /store products -->
			</div>
			<!-- /STORE -->
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /SECTION -->


<?= $this->include('users/layout/footer'); ?>

</body>

</html>