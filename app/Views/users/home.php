<?= $this->include('users/layout/header'); ?>

<!-- SECTION -->
<div class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<!-- shop -->
			<div class="col-md-4 col-xs-6">
				<div class="shop">
					<div class="shop-img">
						<img src="<?= base_url(); ?>/assets/img/Besi.jpg" alt="" width="500" height="300">
					</div>
					<div class="shop-body">
						<h3>Koleksi<br>Besi</h3>
						<a href="<?= base_url() ?>/kategori/Besi" class="cta-btn">Cari Tahu <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div>
			</div>
			<!-- /shop -->
			<!-- shop -->
			<div class="col-md-4 col-xs-6">
				<div class="shop">
					<div class="shop-img">
						<img src="<?= base_url(); ?>/assets/img/Cat.jpg" alt="" width="500" height="300">
					</div>
					<div class="shop-body">
						<h3>Aneka<br>Cat</h3>
						<a href="<?= base_url() ?>/kategori/Cat" class="cta-btn">Cari Tahu <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div>
			</div>
			<!-- /shop -->
			<!-- shop -->
			<div class="col-md-4 col-xs-6">
				<div class="shop">
					<div class="shop-img">
						<img src="<?= base_url(); ?>/assets/img/Pipa.jpg" alt="" width="500" height="300">
					</div>
					<div class="shop-body">
						<h3>Bermacam<br>Pipa PVC</h3>
						<a href="<?= base_url() ?>/kategori/Pipa PVC" class="cta-btn">Cari Tahu <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div>
			</div>
			<!-- /shop -->
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /SECTION -->

<!-- SECTION -->
<div class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">

			<!-- section title -->
			<div class="col-md-12">
				<div class="section-title">
					<h3 class="title">Produk Kami</h3>
				</div>
			</div>
			<!-- /section title -->

			<!-- Products tab & slick -->
			<div class="col-md-12">
				<div class="row">
					<div class="products-tabs">
						<!-- tab -->
						<div id="tab1" class="tab-pane active">
							<div class="products-slick" data-nav="#slick-nav-1">

								<?php foreach ($barang as $value) : ?>
									<!-- product -->
									<div class="product">
										<a href="<?= base_url(); ?>/produk?id_barang=<?= $value['id_barang']; ?>">
											<div class="product-img">
												<img src="<?= base_url(); ?>/upload/<?= $value['gambar'] ?>" width="120" height="250" alt="">
												<?php if ($value['stok'] == 0) {
												?>
													<div class="product-label">
														<span class="new">Habis</span>
													</div>
												<?php } else {
												?>
													<div class="product-label">
														<span class="new">Tersedia</span>
													</div>
												<?php } ?>
											</div>
										</a>
										<div class="product-body">
											<p class="product-category"><?= $value['nama_kategori'] ?></p>
											<h3 class="product-name"><a href="<?= base_url(); ?>/produk?id_barang=<?= $value['id_barang']; ?>"><?= $value['nama_barang']; ?></a></h3>
											<h4 class="product-price">Rp <?= number_format($value['harga_jual'], 0, ',', '.'); ?></h4>

										</div>
										<div class="add-to-cart">
											<button class="add-to-cart-btn"><a href="<?= base_url(); ?>/produk?id_barang=<?= $value['id_barang']; ?>"><i class="fa fa-shopping-cart"></i> add to cart</a></button>
										</div>
									</div>
									<!-- /product -->
								<?php endforeach; ?>
							</div>
							<div id="slick-nav-1" class="products-slick-nav"></div>
						</div>
						<!-- /tab -->
					</div>
				</div>
			</div>
			<!-- Products tab & slick -->
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /SECTION -->

<!-- SECTION -->
<div class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<div class="col-md-4 col-xs-6">
				<div class="section-title">
					<h4 class="title">Kategori</h4>
					<div class="section-nav">
						<div id="slick-nav-3" class="products-slick-nav"></div>
					</div>
				</div>

				<div class="products-widget-slick" data-nav="#slick-nav-3">
					<div>
						<?php foreach ($barang_kategori as $key) : ?>
							<!-- product widget -->
							<div class="product-widget">
								<div class="product-img">
									<img src="<?= base_url(); ?>/upload/<?= $key['gambar'] ?>" width="55" height="65">
								</div>
								<div class="product-body">
									<p class="product-category"><?= $key['nama_kategori'] ?></p>
									<h3 class="product-name"><a href="<?= base_url(); ?>/produk?id_barang=<?= $key['id_barang']; ?>"><?= $key['nama_barang'] ?></a></h3>
									<h4 class="product-price">Rp <?= number_format($key['harga_jual'], 0, ',', '.'); ?></h4>
								</div>
							</div>
							<!-- /product widget -->
						<?php endforeach; ?>
					</div>
					<div>
						<?php foreach ($barang_kategori_2 as $key) : ?>
							<!-- product widget -->
							<div class="product-widget">
								<div class="product-img">
									<img src="<?= base_url(); ?>/upload/<?= $key['gambar'] ?>" width="55" height="65">
								</div>
								<div class="product-body">
									<p class="product-category"><?= $key['nama_kategori'] ?></p>
									<h3 class="product-name"><a href="<?= base_url(); ?>/produk?id_barang=<?= $key['id_barang']; ?>"><?= $key['nama_barang'] ?></a></h3>
									<h4 class="product-price">Rp <?= number_format($key['harga_jual'], 0, ',', '.'); ?></h4>
								</div>
							</div>
							<!-- /product widget -->
						<?php endforeach; ?>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-xs-6">
				<div class="section-title">
					<h4 class="title">Merk</h4>
					<div class="section-nav">
						<div id="slick-nav-4" class="products-slick-nav"></div>
					</div>
				</div>

				<div class="products-widget-slick" data-nav="#slick-nav-4">
					<div>
						<?php foreach ($barang_merk as $key) : ?>
							<!-- product widget -->
							<div class="product-widget">
								<div class="product-img">
									<img src="<?= base_url(); ?>/upload/<?= $key['gambar'] ?>" width="55" height="65">
								</div>
								<div class="product-body">
									<p class="product-category"><?= $key['nama_merk'] ?></p>
									<h3 class="product-name"><a href="<?= base_url(); ?>/produk?id_barang=<?= $key['id_barang']; ?>"><?= $key['nama_barang'] ?></a></h3>
									<h4 class="product-price">Rp <?= number_format($key['harga_jual'], 0, ',', '.'); ?></h4>
								</div>
							</div>
							<!-- /product widget -->
						<?php endforeach; ?>
					</div>
					<div>
						<?php foreach ($barang_merk_2 as $key) : ?>
							<!-- product widget -->
							<div class="product-widget">
								<div class="product-img">
									<img src="<?= base_url(); ?>/upload/<?= $key['gambar'] ?>" width="55" height="65">
								</div>
								<div class="product-body">
									<p class="product-category"><?= $key['nama_merk'] ?></p>
									<h3 class="product-name"><a href="<?= base_url(); ?>/produk?id_barang=<?= $key['id_barang']; ?>"><?= $key['nama_barang'] ?></a></h3>
									<h4 class="product-price">Rp <?= number_format($key['harga_jual'], 0, ',', '.'); ?></h4>
								</div>
							</div>
							<!-- /product widget -->
						<?php endforeach; ?>
					</div>
				</div>
			</div>

			<div class="clearfix visible-sm visible-xs"></div>

			<div class="col-md-4 col-xs-6">
				<div class="section-title">
					<h4 class="title">Paling Laku</h4>
					<div class="section-nav">
						<div id="slick-nav-5" class="products-slick-nav"></div>
					</div>
				</div>

				<div class="products-widget-slick" data-nav="#slick-nav-5">
					<div>
						<?php foreach ($barang_laku as $key) : ?>
							<!-- product widget -->
							<div class="product-widget">
								<div class="product-img">
									<img src="<?= base_url(); ?>/upload/<?= $key['gambar'] ?>" width="55" height="65">
								</div>
								<div class="product-body">
									<p class="product-category">Terjual: <?= $key['jumlah_terjual'] ?></p>
									<h3 class="product-name"><a href="<?= base_url(); ?>/produk?id_barang=<?= $key['id_barang']; ?>"><?= $key['nama_barang'] ?></a></h3>
									<h4 class="product-price">Rp <?= number_format($key['harga_jual'], 0, ',', '.'); ?></h4>
								</div>
							</div>
							<!-- /product widget -->
						<?php endforeach; ?>
					</div>
					<div>
						<?php foreach ($barang_laku_2 as $key) : ?>
							<!-- product widget -->
							<div class="product-widget">
								<div class="product-img">
									<img src="<?= base_url(); ?>/upload/<?= $key['gambar'] ?>" width="55" height="65">
								</div>
								<div class="product-body">
									<p class="product-category">Terjual: <?= $key['jumlah_terjual'] ?></p>
									<h3 class="product-name"><a href="<?= base_url(); ?>/produk?id_barang=<?= $key['id_barang']; ?>"><?= $key['nama_barang'] ?></a></h3>
									<h4 class="product-price">Rp <?= number_format($key['harga_jual'], 0, ',', '.'); ?></h4>
								</div>
							</div>
							<!-- /product widget -->
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /SECTION -->
<!-- NEWSLETTER -->
<div id="newsletter" class="section">
</div>
<!-- /NEWSLETTER -->

<?= $this->include('users/layout/footer'); ?>
</body>

</html>