<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title><?= $title; ?></title>

	<!-- Google font -->
	<link href="<?php echo base_url('assets/css/font-google-users.css'); ?>" rel="stylesheet">

	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/datatables.min.css'); ?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/slick.css'); ?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/slick-theme.css'); ?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/nouislider.min.css'); ?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>" />
	<link href="<?= base_url() ?>/assets/css/sweetalert2.min.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

</head>

<body>
	<!-- HEADER -->
	<header>
		<!-- TOP HEADER -->
		<div id="top-header">
			<div class="container">
				<ul class="header-links pull-left">
					<li><a href="#"><i class="fa fa-phone"></i> +6208122735000</a></li>
					<li><a href="#"><i class="fa fa-envelope-o"></i> dadimakmur@email.com</a></li>
					<li><a href="#"><i class="fa fa-map-marker"></i> Jl. Degolan, Krawitan, Umbulmartani</a></li>
				</ul>
				<ul class="header-links pull-right">
					<?php if (isset($_SESSION['email'])) { ?>
						<li><a href="<?= base_url() ?>/profil"><i class="fa fa-user-o"></i> Akun</a></li>
						<li><a href="#" data-toggle="modal" data-target="#logoutModal"><i class="fa fa-share"></i> Keluar</a></li>
					<?php } else { ?>
						<li><a href="<?= base_url() ?>/login"><i class="fa fa-user-o"></i> Login</a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<!-- /TOP HEADER -->

		<!-- MAIN HEADER -->
		<div id="header">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- LOGO -->
					<div class="col-md-3">
						<div class="header-logo">
							<a href="<?= base_url(); ?>" class="logo">
								<img src="<?= base_url(); ?>/assets/img/logo.png" alt="">
							</a>
						</div>
					</div>
					<!-- /LOGO -->

					<!-- SEARCH BAR -->
					<div class="col-md-6">
						<div class="header-search">
							<form action="<?= base_url() ?>/users/Store/cari" method="POST">
								<input class="input-select" name="nama_barang" placeholder="Cari disini">
								<button class="search-btn">Cari</button>
							</form>
						</div>
					</div>

					<!-- /SEARCH BAR -->

					<!-- ACCOUNT -->
					<div class="col-md-3 clearfix">
						<div class="header-ctn">
							<!-- Cart -->
							<?php if (isset($_SESSION['email'])) { ?>
								<div>
									<a href="<?= base_url('transaksi') ?>">
										<span><i class="fa fa-shopping-bag"></i> Transaksi</span>
									</a>
								</div>

								<div class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
										<i style="font-size:25px" class="fa fa-shopping-cart fa-3x"></i>
										<span> </span>
										<?php
										$keranjang = session('cart');
										?>
										<div class="qty" id='qty'><?= isset($keranjang) ? count($keranjang) : '0'; ?></div>
									</a>

									<?php
									$keranjang = session('cart');
									if ($keranjang != null) { ?>

										<div class="cart-dropdown">
											<div class="cart-list">
												<?php
												$total_belanja = 0;
												foreach ($keranjang as $value) :
												?>
													<div class="product-widget">
														<div class="product-img">
															<img src="<?= base_url() ?>/upload/<?= $value['photo'] ?>" alt="">
														</div>
														<div class="product-body">
															<h3 class="product-name"><a href="#"><?= $value['name'] ?></a></h3>
															<h4 class="product-price"><span class="qty"><?= $value['quantity'] ?> x</span>Rp <?= number_format($value['price'], 0, ',', '.') ?></h4>
														</div>
														<button class="delete"><i class="fa fa-close"></i></button>
													</div>
												<?php
													$total = $value['price'] * $value['quantity'];
													$total_belanja += $total;
												endforeach; ?>
											</div>
											<div class="cart-summary">
												<small><?= count($keranjang); ?> Item dipilih</small>
												<h5>TOTAL: Rp <?= number_format($total_belanja, 0, ',', '.') ?></h5>
											</div>
											<div class="cart-btns">
												<a href="<?= base_url(); ?>/keranjang">Lihat Keranjang</a>
												<a href="<?= base_url(); ?>/pembayaran">Checkout <i class="fa fa-arrow-circle-right"></i></a>
											</div>
										</div>
									<?php } else { ?>
										<div class="cart-dropdown">
											<a href="<?= base_url(); ?>/keranjang">Keranjang Kosong.</a>
										</div>
									<?php } ?>
								</div>
								<!-- /Cart -->
							<?php } ?>

							<!-- Menu Toogle -->
							<div class="menu-toggle">
								<a href="#">
									<i class="fa fa-bars"></i>
									<span>Menu</span>
								</a>
							</div>
							<!-- /Menu Toogle -->
						</div>
					</div>
					<!-- /ACCOUNT -->
				</div>
				<!-- row -->
			</div>
			<!-- container -->
		</div>
		<!-- /MAIN HEADER -->
	</header>
	<!-- /HEADER -->
	<!--================Header Menu Area =================-->

	<!-- NAVIGATION -->
	<nav id="navigation">
		<!-- container -->
		<div class="container">
			<!-- responsive-nav -->
			<div id="responsive-nav">
				<!-- NAV -->
				<ul class="main-nav nav navbar-nav">
					<li class="active"><a href="<?= base_url("/home") ?>">Home</a></li>
					<li><a href="<?= base_url("/store") ?>">Kategori</a></li>
					<!-- <li><a href="#">Brand</a></li> -->
				</ul>
				<!-- /NAV -->
			</div>
			<!-- /responsive-nav -->
		</div>
		<!-- /container -->
	</nav>
	<!-- /NAVIGATION -->