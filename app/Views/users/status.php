<?= $this->include('users/layout/header'); ?>
<!-- BREADCRUMB -->
<div id="breadcrumb" class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<div class="col-md-12">
				<h3 class="breadcrumb-header">Status</h3>
				<ul class="breadcrumb-tree">
					<li><a href="#">Home</a></li>
					<li class="active">Status Transaksi</li>
				</ul>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /BREADCRUMB -->
<!-- SECTION -->
<div class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<div class="col-md-12 order-details">
				<?php
				session()->set($profil);
				unset($_SESSION['cart']);
				if ($status == 'sukses') {
				?>
					<div class="section-title text-center">
						<h3 class="title">SUKSES</h3>
					</div>
					<div class="col-md-12 col-centered" style="text-align: center;">
						<div class="order-summary">
							<div class="alert alert-success" role="alert">
								<p style="font-size: 16px;">
									Terimakasih telah melakukan pembelanjaan di TB. DADI MAKMUR <br>
									Barang pesanan akan segera kami kirim ke alamat anda.

								</p>
							</div>
						</div>
					</div>
				<?php } elseif ($status == 'pending') {
				?>
					<div class="section-title text-center">
						<h3 class="title">PENDING</h3>
					</div>
					<div class="col-md-12 col-centered" style="text-align: center;">
						<div class="order-summary">
							<div class="alert alert-info" role="alert">
								<p style="font-size: 16px;">
									Terimakasih telah melakukan pembelanjaan di TB. DADI MAKMUR <br>
									Segera lakukan pembayaran, detail cara pembayaran bisa anda lihat di email.
								</p>
							</div>
						</div>
					</div>
				<?php } elseif ($status == 'gagal') { ?>
					<div class="section-title text-center">
						<h3 class="title">GAGAL</h3>
					</div>
					<div class="col-md-12 col-centered" style="text-align: center;">
						<div class="order-summary">
							<div class="alert alert-danger" role="alert">
								<p style="font-size: 16px;">
									Terjadi kesalahan, transaksi gagal <br>
									Silahkan ulangi.
								</p>
							</div>
						</div>
					</div>
				<?php } else {
				?>
					<div class="section-title text-center">
						<h3 class="title">ERROR</h3>
					</div>
					<div class="col-md-12 col-centered" style="text-align: center;">
						<div class="order-summary">
							<div class="alert alert-danger" role="alert">
								<p style="font-size: 16px;">
									Terjadi kesalahan, transaksi gagal <br>
									Silahkan ulangi.
								</p>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /SECTION -->
<?= $this->include('users/layout/footer'); ?>