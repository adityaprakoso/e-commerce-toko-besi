<?= $this->include('users/layout/header'); ?>

<!-- BREADCRUMB -->
<div id="breadcrumb" class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<div class="col-md-12">
				<h3 class="breadcrumb-header">Halaman Pengguna</h3>
				<ul class="breadcrumb-tree">
					<li><a href="#">Home</a></li>
					<li class="active">Keranjang</li>
				</ul>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /BREADCRUMB -->
<!-- SECTION -->
<div class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">

			<div class="col-md-7 col-sm-pull-1 order-details">
				<div class="section-title text-center">
					<h3 class="title">Keranjang Belanja</h3>
				</div>

				<table class="table" width="100%" border="0">
					<thead>
						<tr>
							<th>Produk</th>
							<th>Harga</th>
							<th>Jumlah</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php if (!empty($cart)) {
							$no = 0;
							foreach ($cart as $value) {
								$no++;
						?>
								<tr>
									<td> <img src="<?= base_url() ?>/upload/<?= $value['photo'] ?>" alt="" width="100"> <?= $value['name'] ?> </td>
									<td style="vertical-align: middle;">Rp <?= number_format($value['price'], 0, ',', '.'); ?></td>
									<td style="vertical-align: middle;">
										<input type="number" name="jumlah" id="jumlah<?= $no; ?>" min="<?= $value['min'] ?>" max="<?= $value['max'] ?>" onkeyup="cek_jumlah(this.id, <?= $value['min'] ?>,<?= $value['max'] ?>,<?= $no ?>)" oninput="cek_jumlah(this.id, <?= $value['min'] ?>,<?= $value['max'] ?>,<?= $no ?>)" value="<?= $value['quantity'] ?>">
									</td>
									<td style="vertical-align: middle;"><button class="btn btn-danger" onclick="hapus(<?= $value['id'] ?>)"><span class="fa fa-trash"></span></button></td>
								</tr>

							<?php
							}
						} else { ?>
							<br>
							<tr>
								<td align="center" colspan="3">Keranjang Kosong</td>

							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>

			<!-- Order Details -->
			<div class="col-md-5 order-details">
				<div class="section-title text-center">
					<h3 class="title">Total Belanja</h3>
				</div>
				<div class="order-summary">
					<div class="order-col">
						<div><strong>Barang</strong></div>
						<div><strong>TOTAL</strong></div>
					</div>
					<div class="order-products">

						<?php if (isset($cart)) :
							$total = 0;
							$total_belanja = 0;
							foreach ($cart as $value) {
								$total = $value['quantity'] * $value['price'];
						?>

								<div class="order-col">
									<div><?= $value['quantity'] ?> x <?= $value['name'] ?></div>
									<div>Rp <?= number_format($total, 0, ',', '.'); ?></div>
								</div>

						<?php
								$total_belanja = $total_belanja + $total;
							}
						endif; ?>

					</div>
					<div class="order-col">
						<div>Ongkos kirim</div>
						<div><strong>Gratis</strong></div>
					</div>
					<div class="order-col">
						<div><strong>TOTAL</strong></div>
						<div><strong class="order-total">Rp <?= number_format($total_belanja, 0, ',', '.'); ?></strong></div>
					</div>
				</div>

				<a href="<?= base_url(); ?>/pembayaran" class="primary-btn order-submit">Checkout</a>
			</div>
			<!-- /Order Details -->
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /SECTION -->

<?= $this->include('users/layout/footer'); ?>

<script>
	function cek_jumlah(id, min, max, id_cart) {
		jumlah = document.getElementById(id).value;
		if (jumlah > max) {
			document.getElementById(id).value = max
		} else if (jumlah < min) {
			document.getElementById(id).value = min
		}
		setJumlahBarang(id_cart, jumlah);
	}

	function setJumlahBarang(id, jumlah) { //untuk menyesuaikan jumlah item dalam cart
		id -= 1; //id item cart
		$.ajax({
			type: "post",
			url: "<?php echo base_url(); ?>/users/Keranjang/set_jumlah",
			data: {
				id: id,
				jumlah: jumlah
			},
			success: function(data) {
				location.reload()
			}
		});
	}

	function hapus(id) { //menghapus item dari keranjang
		Swal.fire({
			icon: 'warning',
			title: 'Konfirmasi',
			text: 'Hapus barang dari keranjang?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, hapus!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					type: "post",
					url: "<?php echo base_url(); ?>/users/Keranjang/hapus",
					data: {
						id: id
					},
					success: function(data) {
						document.location = "<?= base_url() ?>/keranjang";
					}
				});
			}
		});
	}
</script>