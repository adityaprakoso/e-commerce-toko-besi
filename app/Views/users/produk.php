<?= $this->include('users/layout/header'); ?>


<!-- SECTION -->
<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- Product main img -->
            <div class="col-md-5">
                <div id="product-main-img">
                    <div class="product-preview">
                        <img src="<?= base_url(); ?>/upload/<?= $barang[0]['gambar'] ?>" alt="">
                    </div>
                </div>
            </div>
            <!-- /Product main img -->

            <!-- Product details -->
            <div class="col-md-5 col-md-push-1">
                <div class="product-details">
                    <h2 class="product-name"><?= $barang[0]['nama_barang']; ?></h2>
                    <div>
                        <h3 class="product-price">Rp <?= number_format($barang[0]['harga_jual'], 0, ',', '.') ?> </h3>
                        <span class="product-available">Stok: <?= $barang[0]['stok'] ?></span>
                    </div>
                    <div class="add-to-cart">
                        <div class="qty-label">
                            Jumlah
                            <div class="input-number">
                                <input type="number" id="jumlah" name="jumlah" min="<?= $barang[0]['minimal_pembelian']; ?>" max="<?= $barang[0]['stok'] ?>">
                                <span class="qty-up">+</span>
                                <span class="qty-down">-</span>
                            </div>
                        </div>
                        <?php if ($barang[0]['stok'] > 0) { ?>
                            <button class="add-to-cart-btn" onclick="tambah(<?= $barang[0]['id_barang'] ?>);"><i class="fa fa-shopping-cart"></i> Tambah</button>
                        <?php } else { ?>
                            <button class="add-to-cart-btn" onclick="tambah(<?= $barang[0]['id_barang'] ?>);" disabled><i class="fa fa-shopping-cart"></i> Stok Habis</button>
                        <?php } ?>
                    </div>
                    <p>Minimal Pembelian: <?= $barang[0]['minimal_pembelian'] . " " . $barang[0]['satuan'] ?></p>

                </div>
            </div>
            <!-- /Product details -->

            <!-- Product tab -->
            <div class="col-md-12">
                <div id="product-tab">
                    <!-- product tab nav -->
                    <ul class="tab-nav">
                        <li class="active"><a data-toggle="tab" href="#tab1">Deskripsi</a></li>
                    </ul>
                    <!-- /product tab nav -->
                    <!-- product tab content -->
                    <div class="tab-content">
                        <!-- tab1  -->
                        <div id="tab1" class="tab-pane fade in active">
                            <div class="row">
                                <div class="col-md-12" style="text-align:center">
                                    <p><?= $barang[0]['deskripsi'] ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /tab1  -->
                    </div>
                    <!-- /product tab content  -->
                </div>
            </div>
            <!-- /product tab -->
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
<!-- /SECTION -->

<body onload="cekMinPembelian()">
    <?= $this->include('users/layout/footer'); ?>
    <script>
        // $(function () {
        //   $("jumlah").keydown(function () {
        //     // Save old value.
        //     if (!$(this).val() || (parseInt($(this).val()) <= <?= $barang[0]['stok'] ?> && parseInt($(this).val()) > 0))
        //     $(this).data("old", $(this).val());
        //   });
        //   $("jumlah").keyup(function () {
        //     // Check correct, else revert back to old value.
        //     if (!$(this).val() || (parseInt($(this).val()) <= <?= $barang[0]['stok'] ?> && parseInt($(this).val()) > 0))
        //       ;
        //     else
        //       $(this).val($(this).data("old"));
        //   });
        // });

        function cekMinPembelian() {
            $('#jumlah').val(<?= $barang[0]['minimal_pembelian']; ?>);
        }

        jumlah.oninput = function() {
            if (this.value > <?= $barang[0]['stok']; ?>) {
                this.value = <?= $barang[0]['stok']; ?>;
            } else if (this.value < <?= $barang[0]['minimal_pembelian']; ?>) {
                this.value = <?= $barang[0]['minimal_pembelian']; ?>;
            }
        }

        jumlah.onchange = function() {
            if (this.value > <?= $barang[0]['stok']; ?>) {
                this.value = <?= $barang[0]['stok']; ?>;
            } else if (this.value < <?= $barang[0]['minimal_pembelian']; ?>) {
                this.value = <?= $barang[0]['minimal_pembelian']; ?>;
            }
        }

        function tambah(id_barang) {
            jumlah = $('#jumlah').val();
            // $('#qty').html(jumlah);
            $.ajax({
                type: "post",
                url: "<?php echo base_url(); ?>/users/Keranjang/tambah",
                data: {
                    id_barang: id_barang,
                    jumlah: jumlah
                },

                success: function(data) {
                    var $response = $(data);
                    if (data == "tidak") {
                        document.location = "<?= base_url() ?>/login";
                    } else {
                        // $('#qty').html($response.filter('#jumlah').text());
                        Swal.fire({
                            icon: 'success',
                            title: 'Berhasil masuk keranjang',
                            showConfirmButton: false,
                            timer: 1000
                        }).then((result) => {
                            document.location = "<?= base_url() ?>/produk?id_barang=" + id_barang;
                        })
                    }
                }
            });
        }
    </script>
</body>

</html>