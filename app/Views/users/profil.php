<?= $this->include('users/layout/header'); ?>

<!-- BREADCRUMB -->
<div id="breadcrumb" class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<div class="col-md-12">
				<h3 class="breadcrumb-header">Profil</h3>
				<ul class="breadcrumb-tree">
					<li><a href="#">Akun</a></li>
					<li class="active">Profil</li>
				</ul>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /BREADCRUMB -->

<!-- SECTION -->
<div class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<div class="col-md-7">
				<!-- Billing Details -->
				<div class="billing-details">
					<?php if (session()->getFlashdata('swal') != "") { ?>
						<div class="alert alert-success" role="alert">
							Akun berhasil dirubah.
						</div>
					<?php
					}
					?>
					<form action="<?= base_url(); ?>/profil/update" method="POST">
						<input type="hidden" value="<?= $profil['id_pelanggan'] ?>" name="id">
						<div class="form-group">
							<label for="">Nama</label>
							<input class="input" type="text" value="<?= $profil['nama_lengkap'] ?>" name="nama_lengkap" required>
						</div>
						<div class="form-group">
							<label for="">Email</label>
							<input class="input" type="email" value="<?= $profil['email'] ?>" name="email" required>
						</div>
						<div class="form-group">
							<label for="">Alamat</label>
							<input class="input" type="text" value="<?= $profil['alamat'] ?>" name="alamat" required>
						</div>
						<div class="form-group">
							<label for="">No Hp</label>
							<input class="input" type="text" value="<?= $profil['no_hp'] ?>" name="no_hp" required>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-info">Simpan</button>
						</div>
					</form>
				</div>
				<!-- /Billing Details -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
	<!-- /SECTION -->
	<?= $this->include('users/layout/footer'); ?>
	</body>

	</html>