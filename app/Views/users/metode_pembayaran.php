<?= $this->include('users/layout/header'); ?>
<!-- BREADCRUMB -->
<div id="breadcrumb" class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<div class="col-md-12">
				<h3 class="breadcrumb-header">Pembayaran</h3>
				<ul class="breadcrumb-tree">
					<li><a href="#">Home</a></li>
					<li class="active">Pembayaran</li>
				</ul>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /BREADCRUMB -->
<!-- SECTION -->
<div class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">

			<div class="col-md-7">
				<!-- Billing Details -->
				<div class="billing-details">
					<div class="section-title">
						<h3 class="title">Alamat Pengiriman</h3>
					</div>
					<div class="form-group">
						<input class="input" type="text" readonly value="<?= $profil['nama_lengkap'] ?>" name="nama_lengkap" placeholder="Nama Lengkap">
					</div>
					<div class="form-group">
						<input class="input" type="email" readonly value="<?= $profil['email'] ?>" name="email" placeholder="Email">
					</div>
					<div class="form-group">
						<input class="input" type="text" readonly value="<?= $profil['alamat'] ?>" name="alamat" placeholder="Alamat">
					</div>
					<div class="form-group">
						<input class="input" type="text" readonly value="<?= $profil['no_hp'] ?>" name="no_hp" placeholder="No. HP">
					</div>

				</div>
				<!-- /Billing Details -->

			</div>

			<!-- Order Details -->
			<div class="col-md-5 order-details">
				<div class="section-title text-center">
					<h3 class="title">Total Belanja</h3>
				</div>
				<div class="order-summary">
					<div class="order-col">
						<div><strong>Barang</strong></div>
						<div><strong>TOTAL</strong></div>
					</div>
					<div class="order-products">

						<?php if (isset($cart)) :
							$total = 0;
							$total_belanja = 0;
							foreach ($cart as $value) {
								$total = $value['quantity'] * $value['price'];
						?>

								<div class="order-col">
									<div><?= $value['quantity'] ?>x <?= $value['name'] ?></div>
									<div>Rp <?= number_format($total, 0, ',', '.'); ?></div>
								</div>

						<?php
								$total_belanja = $total_belanja + $total;
							}
						endif; ?>

					</div>
					<div class="order-col">
						<div>Ongkos kirim</div>
						<div><strong>Gratis</strong></div>
					</div>
					<div class="order-col">
						<div><strong>TOTAL</strong></div>
						<div><strong class="order-total">Rp <?= number_format($total_belanja, 0, ',', '.'); ?></strong></div>
					</div>
				</div>

				<form id="payment-form" method="post" action="<?= base_url(); ?>/pembayaran/simpanTransaksi">
					<input type="hidden" name="result_data" id="result-json">
					<input type="hidden" name="status" id="status">
					<!-- <pre><div id="result-data">JSON result will appear here after payment:<br></div></pre> -->
				</form>
				<button id="pay-button" class="primary-btn order-submit">Lanjut ke Pembayaran</button>
			</div>
			<!-- /Order Details -->
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /SECTION -->

<script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="SB-Mid-client-XnM_5fvI86Dac37x"></script>
<script type="text/javascript">
	document.getElementById('pay-button').onclick = function() {
		// SnapToken acquired from previous step
		snap.pay('<?= $snapToken ?>', {
			// Optional
			onSuccess: function(result) {
				$('#result-json').val(JSON.stringify(result, null, 2));
				$('#status').val('sukses');
				$("#payment-form").submit();
				// var string = window.btoa('sukses');
				// window.location.assign("<?= base_url(); ?>/transaksi/status?status=" + string);
				// document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
			},
			// Optional
			onPending: function(result) {
				$('#result-json').val(JSON.stringify(result, null, 2));
				$('#status').val('pending');
				$("#payment-form").submit();
				// document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
			},
			// Optional
			onError: function(result) {
				$('#result-json').val(JSON.stringify(result, null, 2));
				$('#status').val('error');
				$("#payment-form").submit();
				// document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
			}
		});
	};
</script>
<?= $this->include('users/layout/footer'); ?>