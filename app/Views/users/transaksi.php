<?= $this->include('users/layout/header'); ?>

<!-- BREADCRUMB -->
<div id="breadcrumb" class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<div class="col-md-12">
				<h3 class="breadcrumb-header">Halaman Pengguna</h3>
				<ul class="breadcrumb-tree">
					<li><a href="#">Home</a></li>
					<li class="active">History Pembelian</li>
				</ul>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /BREADCRUMB -->
<!-- SECTION -->
<div class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">

			<div class="order-details">
				<div class="section-title text-center">
					<h3 class="title" style="text-align:left;">Transaksi</h3>
				</div>

				<table class="table" width="100%" id="myTable">
					<thead>
						<tr>
							<th>No</th>
							<th>ID Order</th>
							<th>Tanggal</th>
							<th>Total Tagihan</th>
							<th>Status</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no = 1;
						foreach ($transaksi as $value) : ?>
							<tr>
								<td><?= $no++; ?></td>
								<td><?= $value['order_id'] ?></td>
								<td><?= date("d-m-Y", strtotime($value['tanggal'])); ?></td>
								<td><?= $value['total_bayar'] ?></td>
								<td><?= $value['status'] ?></td>
								<td>
									<button class="btn btn-success" onclick="detailTransaksi('<?= $value['order_id'] ?>','<?= $value['transaction_id'] ?>');">Detail</button>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /SECTION -->

<!-- Modal Ubah Data -->
<div class="modal fade" id="detailTransaksi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">

			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Detail Transaksi</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-md-4">
						<div class="panel panel-info">
							<div class="panel-heading">
								<h1 class="panel-title">Detail Order</h1>
							</div>
							<div class="panel-body">
								<p>
									Order ID : <span class="label label-default"><label id="order_id"></label></span><br>
									Tipe Pembayaran : <span class="label label-default"><label id="payment_type"></label></span><br>
									Jumlah : <span class="label label-warning"><label id="total_bayar"></label></span><br>
									Waktu : <span class="label label-default"><label id="tanggal"></label></span><br>
									Status : <span id="spanStatus"><label id="status"></label></span><br>
									Pesan : <span class="label label-default"><label id="status_message"></label></span>
								</p>
							</div>
						</div>
					</div>

					<div class="col-md-8">
						<div class="panel panel-info">
							<div class="panel-heading">
								<h1 class="panel-title">Detail Item</h1>
							</div>
							<div class="panel-body">
								<div class="fetched-data"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" id="cancel" onclick="cancelTransaksi()">Cancel</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>

			</div>
		</div>
	</div>
</div>
<!--- End of Modal --->
<?= $this->include('users/layout/footer'); ?>
<script>
	$(document).ready(function() {
		$('#myTable').DataTable();
	});

	function cancelTransaksi() {
		var id = $('#order_id').text();
		Swal.fire({
			title: 'Yakin?',
			text: "Transaksi akan dibatalkan",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Batal'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					type: "post",
					url: "<?php echo base_url('users/transaksi/batal'); ?>",
					data: {
						id: id
					},
					success: function(data) {
						Swal.fire({
							icon: 'success',
							title: 'Transaksi telah dibatalkan',
							showConfirmButton: false,
							timer: 1200
						}).then((result) => {
							document.location = "<?= base_url() ?>/transaksi";
						})
					}
				});
			}
		})
	}
</script>

<script>
	function detailTransaksi(order_id, transaction_id) {

		$.ajax({
			type: "post",
			url: "<?php echo base_url('admin/pemesanan/getDetail'); ?>",
			data: {
				order_id: order_id,
				transaction_id: transaction_id
			},

			success: function(data) {
				var $response = $(data);
				$('#transaction_id').text($response.filter('#transaction_id').text());
				$('#order_id').text($response.filter('#order_id').text());
				$('#cancelOrder').val($response.filter('#order_id').text());
				$('#payment_type').text($response.filter('#payment_type').text());
				$('#tanggal').text($response.filter('#tanggal').text());
				$('#total_bayar').text($response.filter('#total_bayar').text());
				$('#status').text($response.filter('#status').text());
				$('#statusCancel').val($response.filter('#status').text());

				if ($response.filter('#status').text() == "Success") {
					$('#cancel').hide();
					if ($('#spanStatus').hasClass('label label-success') || $('#spanStatus').hasClass('label label-info') || $('#spanStatus').hasClass('label label-danger') || $('#spanStatus').hasClass('label label-primary')) {
						$('#spanStatus').removeClass('label label-success');
						$('#spanStatus').removeClass('label label-info');
						$('#spanStatus').removeClass('label label-danger');
						$('#spanStatus').addClass('label label-success');
					} else {
						$('#spanStatus').addClass('label label-success');
					}

				} else if ($response.filter('#status').text() == "pending") {
					$('#cancel').show();
					if ($('#spanStatus').hasClass('label label-success') || $('#spanStatus').hasClass('label label-info') || $('#spanStatus').hasClass('label label-danger') || $('#spanStatus').hasClass('label label-primary')) {
						$('#spanStatus').removeClass('label label-primary');
						$('#spanStatus').removeClass('label label-success');
						$('#spanStatus').removeClass('label label-info');
						$('#spanStatus').removeClass('label label-danger');
						$('#spanStatus').addClass('label label-info');
					} else {
						$('#spanStatus').addClass('label label-info');
					}
				} else if ($response.filter('#status').text() == "Cancel") {
					$('#cancel').hide();
					if ($('#spanStatus').hasClass('label label-success') || $('#spanStatus').hasClass('label label-info') || $('#spanStatus').hasClass('label label-danger') || $('#spanStatus').hasClass('label label-primary')) {
						$('#spanStatus').removeClass('label label-primary');
						$('#spanStatus').removeClass('label label-success');
						$('#spanStatus').removeClass('label label-info');
						$('#spanStatus').removeClass('label label-danger');
						$('#spanStatus').addClass('label label-danger');
					} else {
						$('#spanStatus').addClass('label label-danger');
					}
				} else if ($response.filter('#status').text() == "Siap Kirim") {
					$('#cancel').hide();
					if ($('#spanStatus').hasClass('label label-success') || $('#spanStatus').hasClass('label label-info') || $('#spanStatus').hasClass('label label-danger') || $('#spanStatus').hasClass('label label-primary')) {
						$('#spanStatus').removeClass('label label-primary');
						$('#spanStatus').removeClass('label label-success');
						$('#spanStatus').removeClass('label label-info');
						$('#spanStatus').removeClass('label label-danger');
						$('#spanStatus').addClass('label label-success');
					} else {
						$('#spanStatus').addClass('label label-success');
					}
				} else if ($response.filter('#status').text() == "Selesai") {
					$('#cancel').hide();
					if ($('#spanStatus').hasClass('label label-success') || $('#spanStatus').hasClass('label label-info') || $('#spanStatus').hasClass('label label-danger') || $('#spanStatus').hasClass('label label-primary')) {
						$('#spanStatus').removeClass('label label-primary');
						$('#spanStatus').removeClass('label label-success');
						$('#spanStatus').removeClass('label label-info');
						$('#spanStatus').removeClass('label label-danger');
						$('#spanStatus').addClass('label label-primary');
					} else {
						$('#spanStatus').addClass('label label-primary');
					}
				}

				if ($response.filter('#status_message').text() == "Transaksi sedang diproses") {
					$('#status_message').text($response.filter('#transaction_status').text());
				} else {
					$('#status_message').text($response.filter('#status_message').text());
				}

				$('#nama_lengkap').text($response.filter('#nama_lengkap').text());
				$('#email').text($response.filter('#email').text());
				$('#no_hp').text($response.filter('#no_hp').text());
				$('#alamat').text($response.filter('#alamat').text());
				$('#bank').text($response.filter('#bank').text());
				$('#fraud_status').text($response.filter('#fraud_status').text());
				$('#va_number').text($response.filter('#va_number').text());
				$('.fetched-data').html($response.filter('#tabel'));
				$('#detailTransaksi').modal('show');
			}
		});
	}
</script>
</body>

</html>