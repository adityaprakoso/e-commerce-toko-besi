<?= $this->include('admin/layout/header'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <p class="mb-4">Deskripsi data Pemesanan.</p>
  <?php
  function rupiah($value)
  {
    $nilai = "Rp " . number_format($value, 2, ',', '.');
    return $nilai;
  }
  ?>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Daftar Pemesanan</h6>
    </div>
    <div class="card-body">
      <div class="col-xs-12">
        <div class="box table-responsive">
          <div class="box-header with-border">
            <h3 class="box-title">
              <ul class="nav nav-tabs">
                <li class=" btn btn-link mb-3 active"><a data-toggle="tab" href="#success"><span class="fa fa-check"></span> Success</a></li>
                <li class=" btn btn-link mb-3 active"><a data-toggle="tab" href="#pending"><span class="fa fa-history"></span> Pending</a></li>
                <li class=" btn btn-link mb-3 active"><a data-toggle="tab" href="#cancel"><span class="fa fa-ban"></span> Cancel</a></li>
                <li class=" btn btn-link mb-3 active"><a data-toggle="tab" href="#expired"><span class="fa fa-exclamation"></span> Expired</a></li>
              </ul>
            </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="tab-content">
              <div id="success" class="tab-pane in active">
                <table class="table table-hover table-bordered">
                  <thead>
                    <tr>
                      <th style="text-align: center;">No.</th>
                      <th style="text-align: center;">Order ID</th>
                      <th style="text-align: center;">Pelanggan</th>
                      <th style="text-align: center;">Tanggal</th>
                      <th style="text-align: center;">Total Transaksi</th>
                      <th style="text-align: center;">Status</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $no = 1;
                    foreach ($transaksi_success as $value) : ?>
                      <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $value['order_id'] ?></td>
                        <td><?= $value['nama_lengkap'] ?></td>
                        <td><?= date("d-m-Y", strtotime($value['tanggal'])); ?></td>
                        <td><?= rupiah($value['total_bayar']) ?></td>
                        <td><?= $value['status'] ?></td>
                        <td>
                          <?php
                          if ($value['status'] == "Success") {
                          ?>
                            <button class="btn btn-warning" onclick="kirimBarang('<?= $value['transaction_id'] ?>');">Siap Kirim</button>
                          <?php } ?>
                          <button class="btn btn-success" onclick="modalDetail('<?= $value['order_id'] ?>','<?= $value['transaction_id'] ?>');">Detail</button>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>

              <div id="pending" class="tab-pane fade">
                <table class="table table-hover table-bordered">
                  <thead>
                    <th style="text-align: center;">No.</th>
                    <th style="text-align: center;">Order ID</th>
                    <th style="text-align: center;">Pelanggan</th>
                    <th style="text-align: center;">Tanggal</th>
                    <th style="text-align: center;">Total Transaksi</th>
                    <th style="text-align: center;">Status</th>
                    <th>Aksi</th>
                  </thead>
                  <tbody>
                    <?php
                    $no = 1;
                    foreach ($transaksi_pending as $value) : ?>
                      <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $value['order_id'] ?></td>
                        <td><?= $value['nama_lengkap'] ?></td>
                        <td><?= date("d-m-Y", strtotime($value['tanggal'])); ?></td>
                        <td><?= rupiah($value['total_bayar']) ?></td>
                        <td><?= $value['status'] ?></td>
                        <td>
                          <button class="btn btn-success" onclick="modalDetail('<?= $value['order_id'] ?>');">Detail</button>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>

              <div id="cancel" class="tab-pane fade">
                <table class="table table-hover table-bordered">
                  <thead>
                    <th style="text-align: center;">No.</th>
                    <th style="text-align: center;">Order ID</th>
                    <th style="text-align: center;">Pelanggan</th>
                    <th style="text-align: center;">Tanggal</th>
                    <th style="text-align: center;">Total Transaksi</th>
                    <th style="text-align: center;">Status</th>
                    <th>Aksi</th>
                  </thead>
                  <tbody>
                    <?php
                    $no = 1;
                    foreach ($transaksi_cancel as $value) : ?>
                      <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $value['order_id'] ?></td>
                        <td><?= $value['nama_lengkap'] ?></td>
                        <td><?= date("d-m-Y", strtotime($value['tanggal'])); ?></td>
                        <td><?= rupiah($value['total_bayar']) ?></td>
                        <td><?= $value['status'] ?></td>
                        <td>
                          <button class="btn btn-success" onclick="modalDetail('<?= $value['order_id'] ?>');">Detail</button>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>

              <div id="expired" class="tab-pane fade">
                <table class="table table-hover table-bordered">
                  <thead>
                    <th style="text-align: center;">No.</th>
                    <th style="text-align: center;">Order ID</th>
                    <th style="text-align: center;">Pelanggan</th>
                    <th style="text-align: center;">Tanggal</th>
                    <th style="text-align: center;">Total Transaksi</th>
                    <th style="text-align: center;">Status</th>
                    <th>Aksi</th>
                  </thead>
                  <tbody>
                    <?php
                    $no = 1;
                    foreach ($transaksi_expired as $value) : ?>
                      <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $value['order_id'] ?></td>
                        <td><?= $value['nama_lengkap'] ?></td>
                        <td><?= date("d-m-Y", strtotime($value['tanggal'])); ?></td>
                        <td><?= rupiah($value['total_bayar']) ?></td>
                        <td><?= $value['status'] ?></td>
                        <td>
                          <button class="btn btn-success" onclick="modalDetail('<?= $value['order_id'] ?>');">Detail</button>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>

            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->

</div>

<!-- Modal Ubah Data -->
<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detail pemesanan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <div class="row">
          <div class="col-md-7">
            <div class="card border-dark mb-3" style="max-width: 30rem;">
              <div class="card-header">Detail Order</div>
              <div class="card-body text-dark">
                <!-- <h5 class="card-title">Dark card title</h5> -->
                <p>
                  Order ID : <label id="order_id"></label><br>
                  Tipe Pembayaran : <label id="payment_type"></label><br>
                  Jumlah : <label id="total_bayar"></label><br>
                  Waktu : <label id="tanggal"></label><br>
                  Status : <label id="status"></label><br>
                  Pesan : <label id="status_message"></label>
                </p>
              </div>
            </div>
          </div>

          <div class="col-md-5">
            <div class="card border-dark mb-3" style="max-width: 30rem;">
              <div class="card-header">Detail Pelanggan</div>
              <div class="card-body text-dark">
                <!-- <h5 class="card-title">Dark card title</h5> -->
                <p>
                  Nama : <label id="nama_lengkap"></label><br>
                  Email : <label id="email"></label><br>
                  Telepon : <label id="no_hp"></label><br>
                  Alamat : <label id="alamat"></label><br>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="card border-dark mb-3" style="max-width: 30rem;">
              <div class="card-header">Detail Pembayaran</div>
              <div class="card-body text-dark">
                <!-- <h5 class="card-title">Dark card title</h5> -->
                <p>
                  Transaction ID : <label id="transaction_id"></label><br>
                  Virtual Account : <label id="va_number"></label><br>
                  Bank : <label id="bank"></label><br>
                  Fraud Status : <label id="fraud_status"></label><br>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card border-dark mb-3" style="max-width: 60rem;">
              <div class="card-header">Detail Item</div>
              <div class="card-body text-dark">
                <div class="fetched-data"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<!--- End of Modal --->

<?= $this->include('admin/layout/footer'); ?>

<script>
  function kirimBarang(transaction_id) {
    Swal.fire({
      title: 'Konfirmasi',
      text: "Barang siap dikirim?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, kirim!'
    }).then((result) => {
      if (result.value) {
        window.location.href = '<?= base_url(); ?>/admin/pemesanan/kirimBarang/' + transaction_id;
      }
    })
  }
</script>

<script>
  $('table.table').DataTable({
    scrollCollapse: true,
    paging: true
  });
</script>

<script>
  function modalDetail(order_id) {

    $.ajax({
      type: "post",
      url: "<?php echo base_url('admin/pemesanan/getDetail'); ?>",
      data: {
        order_id: order_id,
      },

      success: function(data) {
        var $response = $(data);
        $('#transaction_id').text($response.filter('#transaction_id').text());
        $('#order_id').text($response.filter('#order_id').text());
        $('#payment_type').text($response.filter('#payment_type').text());
        $('#tanggal').text($response.filter('#tanggal').text());
        $('#total_bayar').text($response.filter('#total_bayar').text());
        $('#status').text($response.filter('#status').text());
        if ($response.filter('#status_message').text() == "Transaksi sedang diproses") {
          $('#status_message').text($response.filter('#transaction_status').text());
        } else {
          $('#status_message').text($response.filter('#status_message').text());
        }
        $('#nama_lengkap').text($response.filter('#nama_lengkap').text());
        $('#email').text($response.filter('#email').text());
        $('#no_hp').text($response.filter('#no_hp').text());
        $('#alamat').text($response.filter('#alamat').text());
        $('#bank').text($response.filter('#bank').text());
        $('#fraud_status').text($response.filter('#fraud_status').text());
        $('#va_number').text($response.filter('#va_number').text());
        $('.fetched-data').html($response.filter('#tabel'));
        $('#modalDetail').modal('show');
      }
    });
  }
</script>

</body>

</html>
<!-- End of Main Content -->