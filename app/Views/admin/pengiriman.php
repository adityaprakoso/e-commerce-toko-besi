<?= $this->include('admin/layout/header'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <p class="mb-4">Deskripsi data Barang Siap Kirim</p>
    <?php
    function rupiah($value)
    {
        $nilai = "Rp " . number_format($value, 2, ',', '.');
        return $nilai;
    }
    ?>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Daftar Barang Siap Kirim</h6>
        </div>
        <div class="card-body">
            <div>
                <!-- <button class="btn btn-primary mb-3" onclick=""><span class="fa fa-print"></span> Cetak</button> -->
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="text-align: center;">No.</th>
                            <th style="text-align: center;">Order ID</th>
                            <th style="text-align: center;">Pelanggan</th>
                            <th style="text-align: center;">Tanggal</th>
                            <th style="text-align: center;">Total Transaksi</th>
                            <th style="text-align: center;">Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($pengiriman as $value) : ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $value['order_id'] ?></td>
                                <td><?= $value['nama_lengkap'] ?></td>
                                <td><?= date("d-m-Y", strtotime($value['tanggal'])); ?></td>
                                <td><?= rupiah($value['total_bayar']) ?></td>
                                <td><?= $value['status'] ?></td>
                                <td>
                                    <?php if ($value['status'] == 'Siap Kirim') {
                                    ?>
                                        <button class="btn btn-success" onclick="selesai('<?= $value['transaction_id'] ?>');">Kirim</button>
                                    <?php } else {
                                    ?>
                                        <a class="btn btn-secondary" href="<?= base_url(); ?>/pengiriman/cetak?id=<?= base64_encode($value['order_id']);  ?>" target="_blank">Cetak</a>
                                    <?php  } ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
</div>

<!-- Modal Ubah Data -->
<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Retur</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form class="" action="#" method="post">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <p>Kode Retur : <label id="id_retur"></label></p>
                            </div>
                            <div class="col-md-6">
                                <p>Supplier : <label id="nama_supplier"></label></p>
                            </div>
                        </div>
                        <br>
                        <div class="fetched-data"></div>
                    </div>
                </form>
                <!--- End of Modal Body --->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--- End of Modal --->

<?= $this->include('admin/layout/footer'); ?>

<script>
    function selesai(id) {
        Swal.fire({
            title: 'Konfirmasi',
            text: "Barang siap dikirimkan",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Kirim'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "post",
                    url: "<?php echo base_url('admin/pengiriman/selesai'); ?>",
                    data: {
                        id: id
                    },
                    success: function() {
                        window.location.href = '<?= base_url(); ?>/admin/pengiriman';
                    }
                });
            }
        });
    }
</script>
</body>

</html>
<!-- End of Main Content -->