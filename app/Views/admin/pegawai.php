<?php

use CodeIgniter\Filters\CSRF; ?>
<?= $this->include('admin/layout/header'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Pegawai</h1>
    <p class="mb-4">Deskripsi data pegawai.</p>
    <div class="mb-3"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Tambah</button></div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Pegawai</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Lengkap</th>
                            <th>Username</th>
                            <th>Alamat</th>
                            <th>No. HP</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($pegawai as $value) : ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $value['nama_lengkap'] ?></td>
                                <td><?= $value['username'] ?></td>
                                <td><?= $value['alamat'] ?></td>
                                <td><?= $value['no_hp'] ?></td>
                                <td>
                                    <button class="btn btn-success" onclick="modalUbah('<?= $value['id_pegawai'] ?>');">Detail</button>
                                    <!-- <button class="btn btn-danger" onclick="hapus('<?= $value['id_pegawai'] ?>');">Hapus</button> -->
                                </td>

                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>

<!-- Modal Tambah Data -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Pegawai</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form id="form_tambah" action="<?= base_url(); ?>/admin/pegawai/tambah" method="POST">
                    <?= csrf_field(); ?>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Lengkap</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-user" name="nama_lengkap" placeholder="Nama Lengkap">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Username</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-user" name="username" placeholder="Username">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control form-control-user" name="password" placeholder="password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control form-control-user" name="konfirmasi_password" placeholder="Ulangi Password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">No. HP</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-user" name="no_hp" placeholder="Nomor Handphone">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Alamat</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="alamat" placeholder="Alamat Lengkap"></textarea>
                        </div>
                    </div>
                    <!--- End of Modal Body --->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" onclick="tambah();" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!--- End of Modal --->


<!-- Modal Ubah Data-->
<div class="modal fade" id="modalUbah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ubah Data Pegawai</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form id="form_tambah" action="<?= base_url(); ?>/admin/pegawai/ubah" method="POST">
                    <?= csrf_field(); ?>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Lengkap</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-user" name="id_pegawai" id="id_pegawai" placeholder="Nama Lengkap" hidden>
                            <input type="text" class="form-control form-control-user" name="nama_lengkap" id="nama_lengkap" placeholder="Nama Lengkap">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Username</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-user" name="username" id="username" placeholder="Username">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control form-control-user" name="password" placeholder="password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control form-control-user" name="konfirmasi_password" placeholder="Ulangi Password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">No. HP</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-user" name="no_hp" id="no_hp" placeholder="Nomor Handphone">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Alamat</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="alamat" id="alamat" placeholder="Alamat Lengkap"></textarea>
                        </div>
                    </div>
                    <!--- End of Modal Body --->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!--- End of Modal --->

<?= $this->include('admin/layout/footer'); ?>
<!-- End of Main Content -->

<!-- Javascript -->
<script>
    function tambah() {
        Swal.fire({
            title: 'Apa anda yakin data disimpan?',
            text: "Pastikan data sudah benar!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya!'
        }).then((result) => {
            if (result.value) {
                $("#form_tambah").submit();
            }
        })
    }

    function hapus($id) {
        Swal.fire({
            title: 'Yakin?',
            text: "Data akan dihapus!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.href = '<?= base_url(); ?>/admin/pegawai/hapus/' + $id;
            }
        })
    }

    function modalUbah(id) {
        $.ajax({
            type: "post",
            url: "<?php echo base_url('admin/pegawai/detail'); ?>",
            data: {
                id_pegawai: id
            },
            success: function(data) {
                var $response = $(data);
                $('#id_pegawai').val($response.filter('#id_pegawai').text());
                $('#nama_lengkap').val($response.filter('#nama_lengkap').text());
                $('#username').val($response.filter('#username').text());
                $('#no_hp').val($response.filter('#no_hp').text());
                $('#alamat').val($response.filter('#alamat').text());

                $("#modalUbah").modal('show');
            }
        });
    }
</script>

</body>

</html>