<?= $this->include('admin/layout/header'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <?php if ($title != 'Barang') { ?>
    <h1 class="h3 mb-2 text-gray-800"><?= $supplier[0]['nama_supplier']; ?></h1>
  <?php } else { ?>
    <h1 class="h3 mb-2 text-gray-800">Data Barang</h1>
  <?php } ?>
  <p class="mb-4">Deskripsi Data Barang PT. Dadi Makmur</p>

  <?php if ($title != 'Barang') { ?>
    <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#myModal">Tambah</button>
  <?php } ?>

  <?php
  function rupiah($value)
  {
    $nilai = "Rp " . number_format($value, 2, ',', '.');
    return $nilai;
  }
  ?>


  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Daftar Barang</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>No.</th>
              <th style="text-align: center;">Gambar</th>
              <th style="text-align: center;">Nama Barang</th>
              <th style="text-align: center;">Harga Beli</th>
              <th style="text-align: center;">Harga Jual</th>
              <th>Stok</th>
              <?php if ($title == 'Barang') {
              ?>
                <th style="text-align: center;">Minimal Pembelian</th>
              <?php } ?>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 1;
            foreach ($barang as $value) : ?>
              <tr>
                <td><?= $no++; ?></td>
                <td> <img src="<?= base_url() ?>/upload/<?= $value['gambar'] ?>" alt="" width="100"> </td>
                <td><?= $value['nama_barang'] ?></td>
                <td><?= rupiah($value['harga_beli']) ?></td>
                <td><?= rupiah($value['harga_jual']) ?></td>
                <td><?= $value['stok'] ?></td>
                <?php if ($title == 'Barang') { ?>
                  <td><?= $value['minimal_pembelian'] ?></td>
                <?php } ?>
                <td>
                  <?php if ($title != 'Barang') { ?>
                    <button class="btn btn-success" onclick="modalUbah('<?= $value['id_barang'] ?>','stok');"><span class="fa fa-cart-plus"></span> Stok</button>
                    <button class="btn btn-warning" onclick="modalUbah('<?= $value['id_barang'] ?>','retur');"><span class="fa fa-reply-all"></span> Retur</button>
                  <?php } else { ?>
                    <button class="btn btn-success" onclick="modalUbah('<?= $value['id_barang'] ?>','ubah');">Detail</button>
                  <?php } ?>
                  <!-- <button class="btn btn-danger" onclick="hapus('<?= $value['id_barang'] ?>');"><span class="fa fa-trash"></span> Hapus</button> -->
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
</div>

<!-- Modal tambah data -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <form action="<?= base_url(); ?>/pembelian/tambah" method="POST" enctype="multipart/form-data">
          <?= csrf_field(); ?>

          <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Nama Barang</label>
            <div class="col-sm-10">
              <?php if ($title != 'Barang') { ?>
                <input type="text" class="form-control" name="id_supplier" value="<?= $supplier[0]['id_supplier'] ?>" hidden>
              <?php } ?>
              <input type="text" class="form-control" name="nama_barang" id="nama_barang" placeholder="Nama Barang">
            </div>
          </div>

          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Merk</label>
            <div class="col-sm-10">
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                    Tambah
                  </a>
                </div>
                <select class="form-control" id="merk_barang" name="merk_barang">
                  <option value="" selected disabled>.:pilih:.</option>
                  <?php foreach ($merk as $value) : ?>
                    <option value="<?= $value['id_merk'] ?>"><?= $value['nama_merk'] ?> - <?= $value['id_merk'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-2 col-form-label"></label>
            <div class="col-sm-10">
              <div class="collapse" id="collapseExample">
                <div class="card card-body">

                  <div class="form-group row">
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="kode_new" id="kode_new" placeholder="Kode Merk">
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="merk_new" id="merk_new" placeholder="Nama Merk">
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <button type="button" class="btn btn-primary btn-sm" onclick="tambahMerk()" name="btn_merk_new" id="btn_merk_new">Simpan</button>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group row">
            <legend class="col-form-label col-sm-2 pt-0">Kategori Barang</legend>
            <div class="col-sm-10">
              <select class="form-control" id="kategori_barang" name="kategori_barang">
                <option value="" selected disabled>.:pilih:.</option>
                <?php foreach ($kategori as $value) : ?>
                  <option value="<?= $value['id_kategori'] ?>"><?= $value['nama_kategori'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>

          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Harga Beli</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="harga_beli" placeholder="Harga Beli">
            </div>
          </div>

          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Harga Jual</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="harga_jual" placeholder="Harga Jual">
            </div>
          </div>

          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Satuan</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="satuan" placeholder="Satuan barang">
            </div>
          </div>

          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Minimal Pembelian</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" name="minimal_pembelian" id="minimal_pembelian" placeholder="Minimal Pembelian Barang">
            </div>
          </div>

          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Gambar</label>
            <div class="col-sm-10">
              <img src="<?= base_url(); ?>/assets/img/no_image.png" alt="" width="100" id="gambar_modal"><br><br>
              <input type="file" class="form-control-file" name="file_gambar" id="file_gambar">
            </div>
          </div>

          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Deskripsi</label>
            <div class="col-sm-10">
              <textarea class="form-control" name="deskripsi" id="deskripsi" rows="3"></textarea>
            </div>
          </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!--- End of Modal --->

<!-- Modal Ubah Data -->
<div class="modal fade" id="modalUbah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ubah Data Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form action="<?= base_url(); ?>/pembelian/ubah" method="POST" enctype="multipart/form-data">
          <?= csrf_field(); ?>
          <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Nama Barang</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="id_barang" id="id_barang_ubah" placeholder="Nama Barang" hidden>
              <input type="text" class="form-control" name="nama_barang" id="nama_barang_ubah" placeholder="Nama Barang">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Merk</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="merk_barang" id="merk_barang_ubah" placeholder="Merk Barang">
            </div>
          </div>

          <div class="form-group row">
            <legend class="col-form-label col-sm-2 pt-0">Kategori Barang</legend>
            <div class="col-sm-10">
              <select class="form-control" id="kategori_barang_ubah" name="kategori_barang">
                <?php foreach ($kategori as $value) : ?>
                  <option value="<?= $value['id_kategori'] ?>"><?= $value['nama_kategori'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>

          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Harga Beli</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="harga_beli" id="harga_beli_ubah" placeholder="Harga Barang">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Harga Jual</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="harga_jual" id="harga_jual_ubah" placeholder="Harga Barang">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Stok</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" name="stok" id="stok_ubah" placeholder="Stok Barang" disabled>
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Satuan</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="satuan" id="satuan_ubah" placeholder="Satuan Barang">
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Minimal Pembelian</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" name="minimal_pembelian" id="minimal_pembelian_ubah" placeholder="Minimal Pembelian Barang">
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Gambar</label>
            <div class="col-sm-10">
              <img src="<?= base_url(); ?>/assets/img/no_image.png" alt="" width="100" id="gambar_modal_ubah"><br><br>
              <input type="file" class="form-control-file" name="file_gambar" id="file_gambar_ubah">
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Deskripsi</label>
            <div class="col-sm-10">
              <textarea class="form-control" name="deskripsi" id="deskripsi_ubah" rows="3"></textarea>
            </div>
          </div>
          <!--- End of Modal Body --->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!--- End of Modal --->


<!-- Modal STOK Barang -->
<div class="modal fade" id="modal_tambah_stok" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Stok Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url(); ?>/pembelian/stok" method="POST">
          <?= csrf_field(); ?>
          <input type="text" class="form-control" name="id_barang" id="id_barang_stok" hidden>
          <input type="text" class="form-control" name="id_supplier" id="id_supplier_stok" hidden>
          <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Nama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="nama_barang" id="nama_barang_stok" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Stok</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="stok" id="stok" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Harga Beli</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" name="harga_beli" id="harga_beli_stok">
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Harga Jual</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" name="harga_jual" id="harga_jual_stok">
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Jumlah</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" name="jumlah" id="jumlah_stok" placeholder="Jumlah yang dibeli" required="" onkeyup="cekTotal()">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Diskon(%)</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" min="0" max="100" name="diskon" id="diskon" placeholder="Potongan harga" required="" onkeyup="cekTotal()">
              <label style="font-size: 12px;margin: 10px;color: red;">*Isi 0 jika tidak ada diskon</label>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Total</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" name="total_bayar" id="total_bayar" placeholder="Total bayar" readonly>
              <label style="font-size: 12px;margin: 10px;color: red;">*Belum termasuk PPN 10%</label>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!--- End of Modal --->

<!-- Modal Retur Barang -->
<div class="modal fade" id="modal_retur" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Retur Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url(); ?>/pembelian/retur" method="POST">
          <?= csrf_field(); ?>
          <input type="text" class="form-control" name="id_barang" id="id_barang_retur" hidden>
          <input type="text" class="form-control" name="id_supplier" id="id_supplier_retur" hidden>
          <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Nama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="nama_barang" id="nama_barang_retur" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Stok</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="stok" id="stok_retur" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Harga Beli</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" name="harga_beli" id="harga_beli_retur" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Jumlah</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" name="jumlah" id="jumlah_retur" placeholder="Jumlah yang diretur" required="" onkeyup="cekRetur()">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-2 col-form-label">Keterangan</label>
            <div class="col-sm-10">
              <textarea class="form-control form-control-user" name="keterangan" required=""></textarea>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Total Pengembalian</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" name="total_retur" id="total_retur" placeholder="Total" readonly>
              <label style="font-size: 12px;margin: 10px;color: red;">*Berdasarkan harga pembelian</label>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!--- End of Modal --->


<?= $this->include('admin/layout/footer'); ?>
<script>
  function tambahMerk() {
    var kode = $("#kode_new").val();
    var merk = $("#merk_new").val();

    if (kode == '' || merk == '') {
      $('#collapseExample').collapse('show');
    } else {
      $.ajax({
        type: 'POST',
        url: '<?= base_url(); ?>/admin/merk/tambahMerk',
        data: {
          kode: kode,
          merk: merk
        },
        success: function(data) {
          $('#collapseExample').collapse('hide');
          $('#merk_barang').append($('<option>', {
            value: data.id,
            text: merk + ' - ' + kode
          }));
        }
      });
    }
  }
</script>

<script>
  function cekRetur() {
    var beli = $('#harga_beli_retur').val();
    var jumlah = $('#jumlah_retur').val();
    var total = (beli * jumlah);
    $('#total_retur').val(total);
  }

  function cekTotal() {
    var max = 100;
    var min = 0;
    if ($('#diskon').val() > max) {
      $('#diskon').val(max);
    } else if ($('#diskon').val() < min) {
      $('#diskon').val(min);
    }

    var beli = $('#harga_beli_stok').val();
    var jumlah = $('#jumlah_stok').val();
    var diskon = $('#diskon').val() / 100;

    var total = (beli * jumlah);
    var harga_diskon = total * diskon;
    var harga_akhir = total - harga_diskon;

    $('#total_bayar').val(harga_akhir);

  }

  // ==== Fungsi ambil URL === ///
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#gambar_modal').attr('src', e.target.result);
        $('#gambar_modal_ubah').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#file_gambar").change(function(e) {
    readURL(this);
  });

  $("#file_gambar_ubah").change(function(e) {
    readURL(this);
  });

  function hapus($id) {
    Swal.fire({
      title: 'Yakin?',
      text: "Data barang akan dihapus.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, hapus data!'
    }).then((result) => {
      if (result.value) {
        window.location.href = '<?= base_url(); ?>/admin/pembelian/hapus/' + $id;
      }
    })
  }


  // === Fungsi modalUbah === //
  function modalUbah(id, status) {
    $.ajax({
      type: "post",
      url: "<?php echo base_url(); ?>/admin/pembelian/detail",
      data: {
        id_barang: id,
        status: status
      },

      success: function(data) {
        var $response = $(data);
        if ($response.filter('#status').text() == 'stok') {
          $('#id_supplier_stok').val($response.filter('#id_supplier').text());
          $('#id_barang_stok').val($response.filter('#id_barang').text());
          $('#nama_barang_stok').val($response.filter('#nama_barang').text());
          $('#harga_beli_stok').val($response.filter('#harga_beli').text());
          $('#harga_jual_stok').val($response.filter('#harga_jual').text());
          $('#stok').val($response.filter('#stok').text());
          $("#modal_tambah_stok").modal('show');

        } else if ($response.filter('#status').text() == 'retur') {

          $('#id_supplier_retur').val($response.filter('#id_supplier').text());
          $('#id_barang_retur').val($response.filter('#id_barang').text());
          $('#nama_barang_retur').val($response.filter('#nama_barang').text());
          $('#harga_beli_retur').val($response.filter('#harga_beli').text());
          $('#harga_jual_retur').val($response.filter('#harga_jual').text());
          $('#stok_retur').val($response.filter('#stok').text());
          $("#modal_retur").modal('show');

        } else {
          $('#id_barang_ubah').val($response.filter('#id_barang').text());
          $('#nama_barang_ubah').val($response.filter('#nama_barang').text());
          $('#merk_barang_ubah').val($response.filter('#id_merk').text());
          $('#kategori_barang_ubah').val($response.filter('#kategori_barang').text());
          $('#harga_beli_ubah').val($response.filter('#harga_beli').text());
          $('#harga_jual_ubah').val($response.filter('#harga_jual').text());
          $('#stok_ubah').val($response.filter('#stok').text());
          $('#satuan_ubah').val($response.filter('#satuan').text());
          $('#minimal_pembelian_ubah').val($response.filter('#minimal_pembelian').text());
          $('#gambar_modal_ubah').attr('src', '<?= base_url(); ?>/upload/' + $response.filter('#gambar').text());
          $('#deskripsi_ubah').val($response.filter('#deskripsi').text());
          $("#modalUbah").modal('show');
        }
      }
    });
  }
</script>
</body>

</html>
<!-- End of Main Content -->