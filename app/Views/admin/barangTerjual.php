<?php

use CodeIgniter\Filters\CSRF; ?>
<?= $this->include('admin/layout/header'); ?>

<?php
function rupiah($value)
{
    $nilai = "Rp " . number_format($value, 2, ',', '.');
    return $nilai;
}
?>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Barang Terjual</h1>
    <p class="mb-4">Deskripsi daftar barang yang telah terjual.</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Laporan</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Order ID</th>
                            <th>Barang</th>
                            <th>Jumlah</th>
                            <th>Pokok</th>
                            <th>Jual</th>
                            <th>Laba</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($barangTerjual as $value) : ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= date("d-m-Y", strtotime($value['tanggal'])) ?></td>
                                <td><?= $value['order_id'] ?></td>
                                <td><?= $value['nama_barang'] ?></td>
                                <td><?= $value['jumlah'] ?></td>

                                <?php
                                $pokok = $value['jumlah'] * $value['harga_beli'];
                                $jual = $value['jumlah'] * $value['harga_jual'];
                                $laba = $jual - $pokok;
                                ?>

                                <td><?= rupiah($pokok) ?></td>
                                <td><?= rupiah($jual) ?></td>
                                <td><?= rupiah($laba) ?></td>

                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>

<?= $this->include('admin/layout/footer'); ?>
<!-- End of Main Content -->


</body>

</html>