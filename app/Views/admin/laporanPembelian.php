<?= $this->include('admin/layout/header'); ?>
<?php
function rupiah($value)
{
  $nilai = "Rp " . number_format($value, 2, ',', '.');
  return $nilai;
}
?>


<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Laporan Pembelian</h1>
  <p class="mb-4">Data pembelian barang dari supplier.</p>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Laporan</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>No.</th>
              <th>Tanggal</th>
              <th>Jumlah</th>
              <th>PPN</th>
              <th>Total Pembelian</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 1;
            foreach ($laporan as $value) : ?>
              <tr>
                <td><?= $no++; ?></td>
                <td><?= $value['tanggal'] ?></td>
                <td><?= rupiah($value['total_bayar']) ?></td>
                <td><?= rupiah($value['ppn']) ?></td>
                <td><?= rupiah($value['harga_akhir']) ?></td>
                <td>
                  <a class="btn btn-success" href="<?= base_url(); ?>/laporanPembelian/cetak?id=<?= base64_encode($value['id_pembelian']);  ?>" target="_blank">Cetak</a>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
</div>
<?= $this->include('admin/layout/footer'); ?>
</body>

</html>
<!-- End of Main Content -->