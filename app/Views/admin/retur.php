<?= $this->include('admin/layout/header'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <p class="mb-4">Deskripsi data Retur Barang</p>
    <?php
    function rupiah($value)
    {
        $nilai = "Rp " . number_format($value, 2, ',', '.');
        return $nilai;
    }
    ?>
    <div class="card shadow mb-4">
        <div class="card">
            <div class="card-header">
                Filter
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <label for="">Supplier</label><br>
                        <select class="custom-select" id="inputSupplier">
                            <option value="" selected disabled>.: pilih :.</option>
                            <?php foreach ($supplier as $value) : ?>
                                <option value="<?= $value['id_supplier'] ?>"><?= $value['nama_supplier'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        <div class="invalid-feedback">
                            Pilih supplier dahulu
                        </div>
                    </div>

                    <div class="col-md-3">
                        <label for="">Tanggal Mulai</label><br>
                        <input id="startDate" width="210"><br>
                        <label for="">Tanggal Selesai</label><br>
                        <input id="endDate" width="210">
                    </div>

                    <div class="col-md-3" style="margin-block-start: auto;">
                        <button class="btn btn-primary mb-3" onclick="btnCari()">Cari</button>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Daftar Retur Barang</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="tabelRetur" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="text-align: center;">No.</th>
                            <th style="text-align: center;">Kode Retur</th>
                            <th style="text-align: center;">Supplier</th>
                            <th style="text-align: center;">Tanggal Retur</th>
                            <th style="text-align: center;">Pegawai</th>
                            <th style="text-align: center;">Total Pengembalian</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
</div>

<!-- Modal Ubah Data -->
<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Retur</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form class="" action="#" method="post">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <p>Kode Retur : <label id="id_retur"></label></p>
                            </div>
                            <div class="col-md-6">
                                <p>Supplier : <label id="nama_supplier"></label></p>
                            </div>
                        </div>
                        <br>
                        <div class="fetched-data"></div>
                    </div>
                </form>
                <!--- End of Modal Body --->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--- End of Modal --->

<?= $this->include('admin/layout/footer'); ?>
<script>
    function rubah(angka) {
        var reverse = angka.toString().split('').reverse().join(''),
            ribuan = reverse.match(/\d{1,3}/g);
        ribuan = ribuan.join('.').split('').reverse().join('');
        return 'Rp ' + ribuan;
    }
</script>
<script>
    function listData() {
        dataTable = $('#tabelRetur').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true,
            "order": [
                [3, 'desc']
            ],
            "ajax": {
                url: "<?php echo base_url('admin/retur/getRetur'); ?>",
                type: "POST",
                data: function(data) {
                    data.supplier = $('#inputSupplier').val();
                    data.startDate = $('#startDate').val();
                    data.endDate = $('#endDate').val();
                }
            },
            dom: 'Bfrtip',
            buttons: [{
                    extend: 'copy',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdf',
                    messageTop: 'Dokumen ini milik TB. Dadi Makmur',
                    download: 'open',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'print',
                    download: 'open',
                    messageTop: 'Dokumen ini milik TB. Dadi Makmur',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'colvis',
                    collectionLayout: 'fixed two-column'
                },
            ],
            "deferRender": true,
            "columns": [{
                    data: null,
                    sortable: false,
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    'data': 'id_retur'
                },
                {
                    'data': 'nama_supplier'
                },
                {
                    'data': 'tanggal'
                },
                {
                    'data': 'nama_lengkap'
                },
                {
                    'render': function(data, type, row) {
                        var html = '<p>' + rubah(row.total) + '</p>';
                        return html;
                    }
                },
                {
                    'render': function(data, type, row) {
                        var html = '<button class="btn btn-success" onclick="modalDetail(\'' + row.id_retur + '\',\'' + row.nama_supplier + '\')">Detail</button>';
                        return html;
                    }
                },
            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }, {
                "targets": [6],
                "orderable": false
            }],
        });
    }

    function btnCari() {
        if ($('#inputSupplier').val() == null) {
            $('#inputSupplier').addClass('is-invalid');
        } else if ($('#startDate').val() == "") {
            $('#startDate').addClass('is-invalid');
        } else if ($('#endDate').val() == "") {
            $('#endDate').addClass('is-invalid');
        } else {
            dataTable.draw();
        }
    }

    var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    $('#startDate').datepicker({
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        // minDate: today,
        maxDate: function() {
            return $('#endDate').val();
        }
    });
    $('#endDate').datepicker({
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        minDate: function() {
            return $('#startDate').val();
        }
    });

    $(document).ready(function() {
        listData();
    });
</script>

<script>
    function modalDetail(id, supplier) {
        $.ajax({
            type: "post",
            url: "<?php echo base_url('admin/retur/getDetail'); ?>",
            data: {
                id: id
            },

            success: function(data) {
                var $response = $(data);
                document.getElementById('id_retur').textContent = id;
                document.getElementById('nama_supplier').textContent = supplier;
                $('.fetched-data').html(data);
                $('#modalDetail').modal('show');
            }
        });
    }
</script>
</body>

</html>
<!-- End of Main Content -->