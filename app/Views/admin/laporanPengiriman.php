<?= $this->include('admin/layout/header'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <p class="mb-4">Laporan Pengiriman Barang</p>
    <?php
    function rupiah($value)
    {
        $nilai = "Rp " . number_format($value, 2, ',', '.');
        return $nilai;
    }
    ?>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Daftar Barang Telah Dikirim</h6>
        </div>
        <div class="card-body">
            <div>
                <!-- <button class="btn btn-primary mb-3" onclick=""><span class="fa fa-print"></span> Cetak</button> -->
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="text-align: center;">No.</th>
                            <th style="text-align: center;">Order ID</th>
                            <th style="text-align: center;">Pelanggan</th>
                            <th style="text-align: center;">Tanggal</th>
                            <th style="text-align: center;">Total Transaksi</th>
                            <th style="text-align: center;">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($pengiriman as $value) : ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $value['order_id'] ?></td>
                                <td><?= $value['nama_lengkap'] ?></td>
                                <td><?= date("d-m-Y", strtotime($value['tanggal'])); ?></td>
                                <td><?= rupiah($value['total_bayar']) ?></td>
                                <td><?= $value['status'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
</div>


<?= $this->include('admin/layout/footer'); ?>

</body>

</html>
<!-- End of Main Content -->