<!-- Footer -->
<footer class="sticky-footer bg-white">
  <div class="container my-auto">
    <div class="copyright text-center my-auto">
      <span>Copyright &copy; 2020</span>
    </div>
  </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Yakin keluar?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Pilih logout untuk kembali ke halaman login.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-primary" href="<?= base_url() ?>/admin/home/logout">Logout</a>
      </div>
    </div>
  </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="<?= base_url() ?>/assets/jquery/jquery.min.js"></script>
<script src="<?= base_url() ?>/assets/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url() ?>/assets/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url() ?>/assets/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<!-- <script src="<?= base_url() ?>/assets/chart.js/Chart.min.js"></script> -->

<!-- Page level custom scripts -->
<!-- <script src="<?= base_url() ?>/assets/js/demo/chart-area-demo.js"></script> -->
<!-- <script src="<?= base_url() ?>/assets/js/demo/chart-pie-demo.js"></script> -->
<script src="<?= base_url() ?>/assets/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>/assets/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url() ?>/assets/datatables/dataTables.button.min.js"></script>
<script src="<?= base_url() ?>/assets/datatables/button.flash.min.js"></script>
<script src="<?= base_url() ?>/assets/datatables/jszip.min.js"></script>
<script src="<?= base_url() ?>/assets/datatables/pdfmake.min.js"></script>
<script src="<?= base_url() ?>/assets/datatables/vfs_fonts.js"></script>
<script src="<?= base_url() ?>/assets/datatables/button.html5.min.js"></script>
<script src="<?= base_url() ?>/assets/datatables/button.print.min.js"></script>
<script src="<?= base_url() ?>/assets/datatables/button.colVis.min.js"></script>
<script src="<?= base_url() ?>/assets/js/demo/datatables-demo.js"></script>

<script src="<?= base_url() ?>/assets/js/sweetalert2.min.js"></script>
<script src="<?= base_url() ?>/assets/js/gijgo.min.js"></script>



<!-- Javascript untuk sweetalert -->
<div class="flash-data" data-swaldata="<?= session()->getFlashdata('swal'); ?>"></div>

<script>
  const flashData = $('.flash-data').data('swaldata');
  console.log(flashData);
  if (flashData == 'ditambah' || flashData == 'diubah') {
    Swal.fire(
      'Data <?= $title; ?>',
      'Data telah berhasil ' + flashData,
      'success'
    )
  } else if (flashData == 'stok') {
    Swal.fire(
      'Data Barang',
      'Stok telah ditambahkan.',
      'success'
    )
  } else if (flashData == 'dihapus') {
    Swal.fire(
      'Data <?= $title; ?>',
      'Data telah berhasil ' + flashData,
      'danger'
    )
  } else if (flashData == 'diretur') {
    Swal.fire(
      'Data <?= $title; ?>',
      'Data telah berhasil ' + flashData,
      'success'
    )
  }
</script>