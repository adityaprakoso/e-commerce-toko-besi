<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Surat Jalan</title>
</head>
<?php
function rupiah($value)
{
    $nilai = "Rp " . number_format($value, 2, ',', '.');
    return $nilai;
}

function penyebut($nilai)
{
    $nilai = abs($nilai);
    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($nilai < 12) {
        $temp = " " . $huruf[$nilai];
    } else if ($nilai < 20) {
        $temp = penyebut($nilai - 10) . " belas";
    } else if ($nilai < 100) {
        $temp = penyebut($nilai / 10) . " puluh" . penyebut($nilai % 10);
    } else if ($nilai < 200) {
        $temp = " seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
        $temp = penyebut($nilai / 100) . " ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
        $temp = " seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
        $temp = penyebut($nilai / 1000) . " ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
        $temp = penyebut($nilai / 1000000) . " juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
        $temp = penyebut($nilai / 1000000000) . " milyar" . penyebut(fmod($nilai, 1000000000));
    } else if ($nilai < 1000000000000000) {
        $temp = penyebut($nilai / 1000000000000) . " trilyun" . penyebut(fmod($nilai, 1000000000000));
    }
    return $temp;
}

function terbilang($nilai)
{
    if ($nilai < 0) {
        $hasil = "minus " . trim(penyebut($nilai));
    } else {
        $hasil = trim(penyebut($nilai));
    }
    return $hasil;
}
?>

<body>
    <h2 align="center"><u>Bukti Pemesanan dan Surat Jalan</u></h2>
    <table align="right" cellpadding="6">
        <tr>
            <td width="70%">
                Tanggal
            </td>
            <td width="30%">
                <?= ': ', date('d-m-Y', strtotime($transaksi[0]['tanggal'])); ?>
            </td>
        </tr>
        <tr>
            <td>
                Pembeli
            </td>
            <td>
                <?= ': ', $transaksi[0]['nama_lengkap'] ?>
            </td>
        </tr>
        <tr>
            <td>
                Alamat
            </td>
            <td>
                <?= ': ', $transaksi[0]['alamat'] ?>
            </td>
        </tr>
        <tr>
            <td>
                No. Hp
            </td>
            <td>
                <?= ': ', $transaksi[0]['no_hp'] ?>
            </td>
        </tr>
    </table><br>

    <table border="0" cellpadding="4">
        <hr style="width:100%;">
        <tr>
            <th style="font:bold">No.</th>
            <th>Nama Barang</th>
            <th>Satuan</th>
            <th>Harga Satuan</th>
            <th>Jumlah</th>
            <th>Sub Total</th>
        </tr>
        <hr style="width:100%;">
        <?php
        $sub_total = 0;
        $total_harga = 0;
        $no = 1;
        foreach ($detail as $value) :
            $sub_total = $value['harga_jual'] * $value['jumlah'];
        ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= $value['nama_barang'] ?></td>
                <td><?= $value['satuan'] ?></td>
                <td><?= rupiah($value['harga_jual']) ?></td>
                <td><?= $value['jumlah'] ?></td>
                <td><?= rupiah($sub_total) ?></td>
            </tr>
        <?php
            $total_harga += $sub_total;
        endforeach; ?>
        <hr style="width:100%;">
    </table>
    <div>
    </div>
    <table align="right" cellpadding="4">
        <tr>
            <td>
                Grand Total :
            </td>
            <td>
                <?= rupiah($total_harga) ?>
            </td>
        </tr>
        <tr>
            <td>
                Terbilang :
            </td>
            <td>
                <?= terbilang($total_harga) ?>
            </td>
        </tr>
        <hr style="width:100%;">
    </table>
    <div>
    </div>
    <div>
    </div>
    <table border="0" style=" width:100%;" cellpadding="5">
        <tr>
            <td>
            </td>
            <td align="right">
                <?php $bulan = array("", "Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                ?>
                Yogyakarta, <?php echo date("j") . " " . $bulan[date("n")] . " " . date("Y"); ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td align="center">
                Penerima,
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td align="center">
                <u><?= $transaksi[0]['nama_lengkap'] ?></u>
            </td>
        </tr>
    </table>
</body>

</html>