<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" style="
    background-color: #cbccd6;
    background-image: linear-gradient(180deg,#e74a3b 10%,#e4b746 100%);
    background-size: cover;" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
    <div class="sidebar-brand-icon">
      <i class="fas fa-building"></i>
    </div>
    <div class="sidebar-brand-text mx-3">TB. Dadi Makmur</div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item active">
    <a class="nav-link" href="<?= base_url(); ?>/admin">
      <i class="fas fa-fw fa-home"></i>
      <span>Dashboard</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
    Masterdata
  </div>

  <!-- Nav Item - Pages Collapse Menu -->
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
      <i class="fas fa-fw fa-cube"></i>
      <span>Data Barang</span>
    </a>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Komponen:</h6>
        <a class="collapse-item" href="<?= base_url(); ?>/supplier">Barang Masuk</a>
        <a class="collapse-item" href="<?= base_url(); ?>/pembelian">Daftar Barang</a>
        <!-- <a class="collapse-item" href="<?= base_url(); ?>/merk">Merk</a> -->
        <a class="collapse-item" href="<?= base_url(); ?>/kategoribarang">Kategori</a>
      </div>
    </div>
  </li>

  <li class="nav-item">
    <a class="nav-link collapsed" href="<?= base_url(); ?>/daftarsupplier" aria-expanded="true">
      <i class="fas fa-fw fa-address-book"></i>
      <span>Data Supplier</span>
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePengguna" aria-expanded="true" aria-controls="collapseTwo">
      <i class="fas fa-fw fa-users"></i>
      <span>Data Pengguna</span>
    </a>
    <div id="collapsePengguna" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Komponen:</h6>
        <a class="collapse-item" href="<?= base_url(); ?>/pelanggan">Pelanggan</a>
        <a class="collapse-item" href="<?= base_url(); ?>/pegawai">Pegawai</a>
      </div>
    </div>
  </li>

  <li class="nav-item">
    <a class="nav-link collapsed" href="<?= base_url() ?>/pemesanan" aria-expanded="true">
      <i class="fas fa-fw fa-shopping-cart"></i>
      <span>Data Pemesanan</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link collapsed" href="<?= base_url() ?>/retur" aria-expanded="true">
      <i class="fas fa-fw fa-reply-all"></i>
      <span>Data Retur</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link collapsed" href="<?= base_url() ?>/pengiriman" aria-expanded="true">
      <i class="fas fa-fw fa-paper-plane"></i>
      <span>Data Pengiriman</span>
    </a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
    Laporan
  </div>

  <li class="nav-item">
    <a class="nav-link collapsed" href="<?= base_url() ?>/laporanPembelian" aria-expanded="true">
      <i class="fas fa-fw fa-file"></i>
      <span>Laporan Pembelian</span>
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link collapsed" href="<?= base_url() ?>/laporanKeuangan" aria-expanded="true">
      <i class="fas fa-fw fa-file"></i>
      <span>Laporan Pemasukan</span>
    </a>
  </li>

  <!-- Nav Item - Pages Collapse Menu -->
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
      <i class="fas fa-fw fa-chart-area"></i>
      <span>Laporan Penjualan</span>
    </a>
    <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="<?= base_url(); ?>/laporanBarangTerjual">Daftar Barang Terjual</a>
        <a class="collapse-item" href="<?= base_url(); ?>/laporanPengirimanBarang">Lap. Pengiriman Barang</a>
      </div>
    </div>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->