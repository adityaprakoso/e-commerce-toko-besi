<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Invoice</title>
</head>
<?php
function rupiah($value)
{
    $nilai = "Rp " . number_format($value, 2, ',', '.');
    return $nilai;
}

function penyebut($nilai)
{
    $nilai = abs($nilai);
    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($nilai < 12) {
        $temp = " " . $huruf[$nilai];
    } else if ($nilai < 20) {
        $temp = penyebut($nilai - 10) . " belas";
    } else if ($nilai < 100) {
        $temp = penyebut($nilai / 10) . " puluh" . penyebut($nilai % 10);
    } else if ($nilai < 200) {
        $temp = " seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
        $temp = penyebut($nilai / 100) . " ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
        $temp = " seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
        $temp = penyebut($nilai / 1000) . " ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
        $temp = penyebut($nilai / 1000000) . " juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
        $temp = penyebut($nilai / 1000000000) . " milyar" . penyebut(fmod($nilai, 1000000000));
    } else if ($nilai < 1000000000000000) {
        $temp = penyebut($nilai / 1000000000000) . " trilyun" . penyebut(fmod($nilai, 1000000000000));
    }
    return $temp;
}

function terbilang($nilai)
{
    if ($nilai < 0) {
        $hasil = "minus " . trim(penyebut($nilai));
    } else {
        $hasil = trim(penyebut($nilai));
    }
    return $hasil;
}
?>

<body>

    <table align="right" cellpadding="6">
        <tr>
            <td width="70%">
                Tanggal
            </td>
            <td width="30%">
                <?= ': ', date('d-m-Y', strtotime($transaksi[0]['tanggal'])); ?>
            </td>
        </tr>
        <tr>
            <td>
                Keterangan
            </td>
            <td>
                : Pembelian Produk - <?= $detail[0]['nama_supplier'] ?>
            </td>
        </tr>
        <tr>
            <td>
                Karyawan
            </td>
            <td>
                <?= ': ', $transaksi[0]['nama_lengkap'] ?>
            </td>
        </tr>
    </table><br>

    <table border="0" cellpadding="4">
        <hr style="width:100%;">
        <tr>
            <th style="font:bold">Jumlah</th>
            <th>Satuan</th>
            <th>Nama Barang</th>
            <th>Harga Satuan</th>
            <th>Diskon</th>
            <th>Sub Total</th>
        </tr>
        <hr style="width:100%;">
        <?php
        $total_harga = 0;
        foreach ($detail as $value) : ?>
            <tr>
                <td><?= $value['jumlah'] ?></td>
                <td><?= $value['satuan'] ?></td>
                <td><?= $value['nama_barang'] ?></td>
                <td><?= rupiah($value['harga_beli']) ?></td>
                <td><?= rupiah($value['diskon']) ?></td>
                <td><?= rupiah($value['harga']) ?></td>
            </tr>
        <?php
            $total_harga += $value['harga'];
        endforeach; ?>
        <hr style="width:100%;">
        <tr>
            <td colspan="5" align="right">
                Total Harga :
            </td>
            <td align="right">
                <?= rupiah($total_harga) ?>
            </td>
        </tr>
        <tr>
            <td colspan="5" align="right">
                Pajak :
            </td>
            <td align="right">
                <?= rupiah($transaksi[0]['ppn']) ?>
            </td>
        </tr>
        <hr style="width:100%;">
    </table>
    <div>
    </div>
    <table align="right" cellpadding="4">
        <tr>
            <td>
                Grand Total :
            </td>
            <td>
                <?= rupiah($transaksi[0]['harga_akhir']) ?>
            </td>
        </tr>
        <tr>
            <td>
                Terbilang :
            </td>
            <td>
                <?= terbilang($transaksi[0]['harga_akhir']) ?>
            </td>
        </tr>
        <hr style="width:100%;">
    </table>
    <div>
    </div>
    <div>
    </div>
    <table border="0" style="width:100%;" cellpadding="5">
        <tr>
            <td>
            </td>
            <td align="right">
                <?php $bulan = array("", "Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                ?>
                Yogyakarta, <?php echo date("j") . " " . $bulan[date("n")] . " " . date("Y"); ?>
            </td>
        </tr>
        <tr>
            <td align="center">
                TB.Dadi Makmur
            </td>
            <td align="center">
                Dengan hormat,
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="center">
                <u><?= $transaksi[0]['nama_lengkap'] ?></u>
            </td>
            <td align="center">Penerima</td>
        </tr>
    </table>
</body>

</html>