<?= $this->include('admin/layout/header'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Transaksi barang masuk</h1>
  <p class="mb-4">Pilih dari daftar supplier yang tersedia.</p>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Daftar Supplier</h6>
    </div>
    <div class="card-body">

    <div class="row">
    <?php
      foreach ($supplier as $key => $value) {
    ?>
    
      <div class="card mr-3" style="width: 16rem;">
      <a href="<?= base_url(); ?>/pembelian/<?= $value['id_supplier']; ?>">  
      <img class="card-img-top" src="<?= base_url() ?>/assets/img/supplier1.png" style="width:100%;">
      </a>
        <div class="card-body">
          <h5 class="card-title"><b> <?= $value['nama_supplier']; ?> </b></h5>
          <!-- <p class="card-text"><?= $value['alamat']; ?></p> -->
        </div>
      </div>
      <?php }  ?>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
</div>
<?= $this->include('admin/layout/footer'); ?>
</body>
</html>
<!-- End of Main Content -->