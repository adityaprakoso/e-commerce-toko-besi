<?= $this->include('admin/layout/header'); ?>
<style>
  .nothing {
    border: 0;
    outline: none;
    background: none;
    font-size: 25px;
  }
</style>
<?php
function rupiah($value)
{
  $nilai = "Rp " . number_format($value, 0, ',', '.');
  return $nilai;
}
?>
<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <p class="mb-4">Deskripsi laporan Keuangan</p>

  <div class="card shadow mb-4">
    <div class="card">
      <div class="card-header">
        Filter
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-3">
            <label for="">Tanggal Mulai</label><br>
            <input id="startDate" width="210"><br>
            <label for="">Tanggal Selesai</label><br>
            <input id="endDate" width="210">
          </div>
          <div class="col-md-6">
            <p>
            <h6>Total Pemasukan : </h6>
            <input class="nothing" font="14sp" id="totalPemasukan" value="<?= rupiah($pemasukan[0]['total_bayar']) ?>" disabled />
            </p>
            <h6>Pemasukan dari tanggal</h6>
            <label id="tanggal_mulai">-</label>
            <h6>s/d</h6>
            <label id="tanggal_selesai">-</label><br>
            <input class="nothing" font="14sp" id="pemasukanPeriode" value="<?= rupiah(0); ?>" disabled />
          </div>
        </div>
        <div class="col-md-3" style="margin-top: 20px;">
          <button class="btn btn-primary mb-3" onclick="btnCari()">Cari</button>
        </div>
      </div>
    </div>
  </div>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Laporan Pemasukan</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataPemasukan" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>No.</th>
              <th>Order ID</th>
              <th>Tanggal</th>
              <th>Pemasukan</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
</div>

<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detail Pembelian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <div class="row">
          <div class="col-md-5">
            <div class="card border-dark mb-3" style="max-width: 30rem;">
              <div class="card-header">Detail Pelanggan</div>
              <div class="card-body text-dark">
                <!-- <h5 class="card-title">Dark card title</h5> -->
                <p>
                  Nama : <label id="nama_lengkap"></label><br>
                  Email : <label id="email"></label><br>
                  Telepon : <label id="no_hp"></label><br>
                  Alamat : <label id="alamat"></label><br>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card border-dark mb-3" style="max-width: 60rem;">
              <div class="card-header">Detail Item</div>
              <div class="card-body text-dark">
                <div class="fetched-data"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>



<?= $this->include('admin/layout/footer'); ?>
<script>
  function rupiah(bilangan) {
    if (bilangan == 0) {
      return 'Rp 0';
    } else {
      var number_string = bilangan.toString(),
        sisa = number_string.length % 3,
        rupiah = number_string.substr(0, sisa),
        ribuan = number_string.substr(sisa).match(/\d{3}/g);

      if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }
      return 'Rp ' + rupiah;
    }
  }

  function cekPemasukan() {
    var start = $('#startDate').val();
    var finish = $('#endDate').val();

    $.ajax({
      type: 'POST',
      url: "<?php echo base_url('admin/laporanKeuangan/getPemasukanPeriode'); ?>",
      data: {
        start: start,
        finish: finish
      },
      success: function(data) {
        var $response = $(data);
        $('#tanggal_mulai').text(start);
        $('#tanggal_selesai').text(finish);
        var total = $response.filter('#totalPemasukan').text();
        if (total == null) {
          $('#pemasukanPeriode').val(rupiah(0));
        } else {
          $('#pemasukanPeriode').val(rupiah(total));
        }
      }
    });

  }

  function listPemasukan() {
    dataTable = $('#dataPemasukan').DataTable({
      "processing": true,
      "serverSide": true,
      "ordering": true,
      "order": [
        [2, 'desc']
      ],
      "ajax": {
        url: "<?php echo base_url('admin/laporanKeuangan/getData'); ?>",
        type: "POST",
        data: function(data) {
          data.startDate = $('#startDate').val();
          data.endDate = $('#endDate').val();
        },
      },
      dom: 'Bfrtip',
      buttons: [{
          extend: 'copy',
          exportOptions: {
            columns: ':visible'
          }
        },
        {
          extend: 'csv',
          exportOptions: {
            columns: ':visible'
          }
        },
        {
          extend: 'excel',
          exportOptions: {
            columns: ':visible'
          }
        },
        {
          extend: 'pdf',
          messageTop: 'Dokumen ini milik TB. Dadi Makmur',
          download: 'open',
          exportOptions: {
            columns: ':visible'
          }
        },
        {
          extend: 'print',
          download: 'open',
          messageTop: 'Dokumen ini milik TB. Dadi Makmur',
          exportOptions: {
            columns: ':visible'
          }
        },
        {
          extend: 'colvis',
          collectionLayout: 'fixed two-column'
        },
      ],
      "deferRender": true,
      "columns": [{
          data: null,
          sortable: false,
          render: function(data, type, row, meta) {
            return meta.row + meta.settings._iDisplayStart + 1;
          }
        },
        {
          data: 'order_id',
          name: 'order_id'
        },
        {
          data: 'tanggal',
          name: 'tanggal'
        },
        {
          'render': function(data, type, row) {
            var html = '<p>' + rupiah(row.total_bayar) + '</p>';
            return html;
          }
        },
        {
          'render': function(data, type, row) {
            var html = '<button class="btn btn-success" onclick="modalDetail(\'' + row.order_id + '\')">Detail</button>';
            return html;
          }
        },
      ],
      "columnDefs": [{
        "targets": [0],
        "orderable": false
      }, {
        "targets": [4],
        "orderable": false
      }],
    });
  }

  function btnCari() {
    if ($('#startDate').val() == "") {
      $('#startDate').addClass('is-invalid');
    } else if ($('#endDate').val() == "") {
      $('#endDate').addClass('is-invalid');
    } else {
      dataTable.draw();
      cekPemasukan();
    }
  }

  var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
  $('#startDate').datepicker({
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fontawesome',
    // minDate: today,
    maxDate: function() {
      return $('#endDate').val();
    }
  });
  $('#endDate').datepicker({
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fontawesome',
    minDate: function() {
      return $('#startDate').val();
    }
  });

  function modalDetail(id) {
    $.ajax({
      type: "post",
      url: "<?php echo base_url('admin/laporanKeuangan/getDetail'); ?>",
      data: {
        id: id
      },

      success: function(data) {
        var $response = $(data);
        $('#nama_lengkap').text($response.filter('#nama_lengkap').text());
        $('#email').text($response.filter('#email').text());
        $('#no_hp').text($response.filter('#no_hp').text());
        $('#alamat').text($response.filter('#alamat').text());
        $('.fetched-data').html(data);
        $('#modalDetail').modal('show');
      }
    });
  }

  $(document).ready(function() {
    listPemasukan();
  });
</script>
</body>

</html>
<!-- End of Main Content -->