<?php

use CodeIgniter\Filters\CSRF; ?>
<?= $this->include('admin/layout/header'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Merk Barang</h1>
    <p class="mb-4">Daftar Merk Barang TB. Dadi Makmur</p>
    <div class="mb-3"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Tambah</button></div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Merk</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Merk</th>
                            <th>Nama Merk</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($merk as $value) : ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $value['id_merk'] ?></td>
                                <td><?= $value['nama_merk'] ?></td>
                                <td>
                                    <button class="btn btn-success" onclick="modalUbah('<?= $value['id_merk'] ?>');">Detail</button>
                                    <!-- <button class="btn btn-danger" onclick="hapus('<?= $value['id_merk'] ?>');">Hapus</button> -->
                                </td>

                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>

<!-- Modal Tambah Data -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Merk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form id="form_tambah" action="<?= base_url(); ?>/admin/merk/tambah" method="POST">
                    <?= csrf_field(); ?>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Kode Merk</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="id_merk" id="id_merk" placeholder="ex: rck">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Merk</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" id="nama" placeholder="ex: rucika">
                        </div>
                    </div>
                    <!--- End of Modal Body --->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" onclick="tambah();" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!--- End of Modal --->


<!-- Modal Ubah Data-->
<div class="modal fade" id="modalUbah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ubah Data Merk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form id="form_tambah" action="<?= base_url(); ?>/admin/merk/ubah" method="POST">
                    <?= csrf_field(); ?>
                    <div class="form-group row">
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Kode Merk</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="id_ubah" id="id_ubah" placeholder="ID Merk">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Merk</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama_ubah" id="nama_ubah" placeholder="Nama Merk">
                        </div>
                    </div>
                    <!--- End of Modal Body --->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!--- End of Modal --->

<?= $this->include('admin/layout/footer'); ?>
<!-- End of Main Content -->

<!-- Javascript -->
<script>
    function tambah() {
        Swal.fire({
            title: 'Yakin?',
            text: "Data akan ditambahkan.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya!'
        }).then((result) => {
            if (result.value) {
                $("#form_tambah").submit();
            }
        })
    }

    function hapus($id) {
        Swal.fire({
            title: 'Yakin?',
            text: "Data akan dihapus.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, hapus data!'
        }).then((result) => {
            if (result.value) {
                window.location.href = '<?= base_url(); ?>/admin/merk/hapus/' + $id;
            }
        })
    }

    function modalUbah(id) {
        $.ajax({
            type: "post",
            url: "<?php echo base_url('admin/merk/detail'); ?>",
            data: {
                id_kategori: id
            },

            success: function(data) {
                var $response = $(data);
                $('#id_ubah').val($response.filter('#id_merk').text());
                $('#nama_ubah').val($response.filter('#nama_merk').text());
                // document.getElementById('nama_ubah').textContent = $response.filter('#id_kategori').text();

                $("#modalUbah").modal('show');
            }
        });
    }
</script>

</body>

</html>