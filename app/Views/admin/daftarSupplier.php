<?= $this->include('admin/layout/header'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Daftar Supplier</h1>
  <p class="mb-4">Deskripsi data supplier.</p>

  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Tambah Supplier</button>
  <br><br>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Supplier</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Nama Supplier</th>
              <th>No. HP</th>
              <th>Alamat</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($barang as $value) : ?>
              <tr>
                <td><?= $value['nama_supplier'] ?></td>
                <td><?= $value['no_hp'] ?></td>
                <td><?= $value['alamat'] ?></td>
                <td>
                  <button class="btn btn-success" onclick="modalUbah('<?= $value['id_supplier'] ?>');">Detail</button>
                  <!-- <button class="btn btn-danger" onclick="hapus('<?= $value['id_supplier'] ?>');">Hapus</button> -->
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->

</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Supplier</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form action="<?= base_url(); ?>/supplier/tambah" method="POST">
          <?= csrf_field(); ?>
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Supplier</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="nama_supplier" id="nama_supplier" placeholder="ex: CV. Angkasa Raya" required="">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">No. HP</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="no_hp" id="no_hp" placeholder="ex: 081229500511" required="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-2 col-form-label">Alamat</label>
            <div class="col-sm-10">
              <textarea class="form-control" name="alamat" id="alamat" rows="3" required=""></textarea>
            </div>
          </div>
          <!--- End of Modal Body --->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!--- End of Modal --->

<!-- Modal Ubah Data -->
<div class="modal fade" id="modalUbah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ubah Data Supplier</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="<?= base_url(); ?>/supplier/ubah" method="POST">
          <?= csrf_field(); ?>
      <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Supplier</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="id_supplier_ubah" id="id_supplier_ubah" hidden>
              <input type="text" class="form-control" name="nama_supplier" id="nama_supplier_ubah" placeholder="Nama Barang">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">No. HP</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="no_hp" id="no_hp_ubah" placeholder="Merk Barang">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-2 col-form-label">Alamat</label>
            <div class="col-sm-10">
              <textarea class="form-control" name="alamat" id="alamat_ubah" rows="3"></textarea>
            </div>
          </div>
          <!--- End of Modal Body --->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!--- End of Modal --->

<?= $this->include('admin/layout/footer'); ?>

<script>
 
  function hapus($id) {
        Swal.fire({
            title: 'Yakin?',
            text: "Data akan dihapus!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, hapus!'
        }).then((result) => {
            if (result.value) {
               window.location.href = '<?= base_url();?>/admin/supplier/hapus/'+$id;
            }
        })
    }

// === Fungsi modalUbah === //
function modalUbah(id){
        $.ajax({
            type: "post",
            url: "<?php echo base_url();?>/admin/supplier/detail",
            data: {
                id_supplier : id
            },

            success: function(data) {
                var $response = $(data);
                $('#id_supplier_ubah').val($response.filter('#id_supplier').text());
                $('#nama_supplier_ubah').val($response.filter('#nama_supplier').text());
                $('#no_hp_ubah').val($response.filter('#no_hp').text());
                $('#alamat_ubah').val($response.filter('#alamat').text());
                $("#modalUbah").modal('show');
            }
        });
    }

</script>


</body>

</html>
<!-- End of Main Content -->