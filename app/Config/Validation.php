<?php

namespace Config;

class Validation
{
	//--------------------------------------------------------------------
	// Setup
	//--------------------------------------------------------------------

	/**
	 * Stores the classes that contain the
	 * rules that are available.
	 *
	 * @var array
	 */
	public $ruleSets = [
		\CodeIgniter\Validation\Rules::class,
		\CodeIgniter\Validation\FormatRules::class,
		\CodeIgniter\Validation\FileRules::class,
		\CodeIgniter\Validation\CreditCardRules::class,
		\App\Validation\UserRules::class,
	];

	/**
	 * Specifies the views that are used to display the
	 * errors.
	 *
	 * @var array
	 */
	public $templates = [
		'list'   => 'CodeIgniter\Validation\Views\list',
		'single' => 'CodeIgniter\Validation\Views\single',
	];

	//--------------------------------------------------------------------
	// Rules
	//--------------------------------------------------------------------

	public $register = [
		'nama_lengkap' => 'required|min_length[3]',
		'email' => 'required|valid_email|is_unique[tb_pelanggan.email]',
		'password' => 'required|min_length[8]',
		'konfirmasi_password' => 'required|matches[password]',
		'no_hp' => 'required|min_length[3]',
		'alamat' => 'required|min_length[3]'
	];

	public $register_errors = [
		'email' => [
			'required'          => 'Email wajib diisi',
			'email.valid_email' => 'Email tidak valid'
		],
		'password' => [
			'required'      => 'Password wajib diisi',
			'min_length'    => 'Password minimal terdiri dari 8 karakter'
		],
		'konfirmasi_password' => [
			'matches' => 'Password yang dimasukan tidak sama',
			'required'          => 'Konfirmasi password wajib diisi'
		],
	];
}
