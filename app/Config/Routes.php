<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
//route users
$routes->get('/', 'Home::index');
$routes->get('/login', 'Login::index');
$routes->get('/profil', 'Users/Profil::index');
$routes->get('/store', 'Users/Store::index');
$routes->get('/keranjang', 'Users/Keranjang::index');
$routes->get('/transaksi', 'Users/Transaksi::index');
$routes->get('/detailtransaksi', 'Users/DetailTransaksi::index');
$routes->get('/pembayaran', 'Users/Pembayaran::index');
$routes->post('/pembayaran/simpanTransaksi', 'Users/Pembayaran::simpanTransaksi');
$routes->post('/proses_login', 'Login::proses_login');
$routes->get('/register/(:any)', 'Register::index');
$routes->post('/daftarAkun', 'Register::register');
$routes->post('/profil/update', 'Users/Profil::update');


//routes admin
$routes->get('/admin', 'Admin/Home::index');
$routes->get('/admin/login', 'Login::login_admin');
$routes->get('/pembelian', 'Admin/Pembelian::index');
$routes->get('/pemesanan', 'Admin/Pemesanan::index');
$routes->get('/pengiriman', 'Admin/Pengiriman::index');
$routes->get('/pembelian/(:num)', 'Pembelian::barang_supplier/$1', ['namespace' => 'App\Controllers\Admin']);
$routes->post('/pembelian/tambah', 'Admin/Pembelian::tambah');
$routes->post('/pembelian/ubah', 'Admin/Pembelian::ubah');
$routes->post('/pembelian/stok', 'Admin/Pembelian::stok');
$routes->post('/pembelian/retur', 'Admin/Pembelian::retur');
$routes->get('/retur', 'Admin/Retur::index');
$routes->post('/pembelian/hapus', 'Admin/Pembelian::hapus');
$routes->get('/supplier', 'Admin/Supplier::index'); //ke cardview supplier
$routes->get('/daftarsupplier', 'Admin/Supplier::daftar_supplier'); //tambah data supplier
$routes->post('/supplier/tambah', 'Admin/Supplier::tambah'); //tambah data supplier
$routes->post('/supplier/ubah', 'Admin/Supplier::ubah'); //ubah data supplier
$routes->post('/supplier/hapus', 'Admin/Supplier::hapus'); //ubah data supplier
$routes->get('/kategoribarang', 'Admin/KategoriBarang::index');
$routes->post('/kategoribarang/tambah', 'Admin/KategoriBarang::tambah');
$routes->get('/merk', 'Admin/Merk::index');
$routes->get('/pelanggan', 'Admin/Pelanggan::index');
$routes->get('/pegawai', 'Admin/Pegawai::index');
$routes->get('/laporanPembelian', 'Admin/LaporanPembelian::index');
$routes->get('/laporanPembelian/cetak', 'Admin/LaporanPembelian::cetak');
$routes->get('/pengiriman/cetak', 'Admin/Pengiriman::cetak');
$routes->get('/laporanKeuangan', 'Admin/LaporanKeuangan::index');
$routes->get('/laporanBarangTerjual', 'Admin/LaporanBarangTerjual::index');
$routes->get('/laporanPengirimanBarang', 'Admin/LaporanPengirimanBarang::index');

// $routes->get('/kategoribarang/(:segment)', 'KategoriBarang::detail/$1', ['namespace' => 'App\Controllers\Admin']);
// $routes->get('/pembelian/(:num)', 'pembelian::index/$1', ['namespace' => 'App\Controllers\admin']); //daftar barang berdasarkan supplier

//route store
$routes->get('/kategori/(:any)', 'Store::cari_by_kategori/$1', ['namespace' => 'App\Controllers\Users']);
$routes->get('/merk/(:any)', 'Store::cari_by_merk/$1', ['namespace' => 'App\Controllers\Users']);








/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
