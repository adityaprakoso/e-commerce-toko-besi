<?php 
namespace App\Validation;
use App\Models\PelangganModel;
use App\Models\PegawaiModel;

class UserRules
{
    public function validateUser(string $str, string $fields, array $data)
    {
        $model = new PelangganModel();
        $user = $model->where('email', $data['email'])->first();

        if(!$user)
        return false;

        return password_verify($data['password'], $user['password']);

    }

    public function validateAdmin(string $str, string $fields, array $data)
    {
        $model = new PegawaiModel();
        $user = $model->where('username', $data['username'])->first();

        if(!$user)
        return false;

        return password_verify($data['password'], $user['password']);

    }

}