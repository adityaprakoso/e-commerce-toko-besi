<?php

namespace App\Models;

use CodeIgniter\Model;

class DetailTransaksiModel extends Model
{
    protected $table = 'tb_detail_transaksi';
    protected $useTimestamps = true;
    protected $allowedFields = ['order_id', 'id_barang', 'jumlah'];
    protected $primaryKey = 'id_detail';

    public function simpanDetail($data)
    {
        $builder = $this->db->table('tb_detail_transaksi');

        $builder->insertBatch($data);
    }

    public function getDetail($order_id)
    {
        $builder = $this->db->table('tb_detail_transaksi');
        return $builder->Where('order_id', $order_id)->get()->getResultArray();
    }
}
