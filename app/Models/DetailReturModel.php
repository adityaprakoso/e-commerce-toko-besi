<?php namespace App\Models;

use CodeIgniter\Model;

class DetailReturModel extends Model
{
    protected $table = 'tb_detail_retur';
    protected $useTimestamps = false;
    protected $allowedFields = ['id_detail_retur', 'id_retur', 'id_barang', 'jumlah', 'pengembalian', 'keterangan'];
    protected $primaryKey = 'id_detail_retur';
}