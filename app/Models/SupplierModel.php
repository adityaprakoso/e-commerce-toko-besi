<?php

namespace App\Models;

use CodeIgniter\Model;

class SupplierModel extends Model
{
    protected $table = 'tb_supplier';
    protected $useTimestamps = true;
    protected $allowedFields = ['nama_supplier', 'alamat', 'no_hp'];
    protected $primaryKey = 'id_supplier';

    public function getValue($tabel, $pk = null, $value = null)
    {

        $builder = $this->db->table($tabel);
        if ($pk == null) {
            $query   = $builder->get()->getResultArray();  // Produces: SELECT * FROM mytable
            return $query;
        }
        return $builder->Where($pk, $value)->get()->getResultArray();
    }

    public function getSupplier()
    {
        $builder = $this->db->table('tb_supplier');
        return $builder->countAll();
    }
}
