<?php

namespace App\Models;

use CodeIgniter\Model;
use App\Models\BaseBuilder;

class BarangModel extends Model
{
    protected $table = 'tb_barang';
    protected $useTimestamps = true;
    protected $allowedFields = ['id_supplier', 'nama_barang', 'id_merk', 'id_kategori_barang', 'harga_beli', 'harga_jual', 'satuan', 'stok', 'jumlah_terjual', 'minimal_pembelian', 'gambar', 'deskripsi'];
    protected $primaryKey = 'id_barang';


    public function getTable($tabel, $pk = null, $value = null)
    {
        $builder = $this->db->table($tabel);
        if ($pk == null) {
            if ($tabel == 'tb_barang') {
                $builder->join('tb_kategori_barang', 'tb_kategori_barang.id_kategori = tb_barang.id_kategori_barang');
                $builder->join('tb_merk', 'tb_merk.id_merk = tb_barang.id_merk');
            }
            return $builder->get()->getResultArray();  // Produces: SELECT * FROM mytable
        }
        return $builder->Where($pk, $value)->get()->getResultArray();
    }

    public function cariBarang($table, $value)
    {
        $builder = $this->db->table($table);
        $builder->join('tb_kategori_barang', 'tb_kategori_barang.id_kategori = tb_barang.id_kategori_barang');
        $builder->join('tb_merk', 'tb_merk.id_merk = tb_barang.id_merk');
        $builder->like('tb_barang.nama_barang', $value, 'both');
        return $builder->get()->getResultArray();
    }

    public function getBarangByKategori($tabel1, $tabel2, $value = null)
    {
        $builder = $this->db->table($tabel1);
        $builder->join($tabel2, 'tb_barang.id_kategori_barang = tb_kategori_barang.id_kategori');
        $builder->where('tb_kategori_barang.nama_kategori', $value);
        return $builder->get()->getResultArray();
    }

    public function getBarangByMerk($tabel1, $tabel2, $value = null)
    {
        $builder = $this->db->table($tabel1);
        $builder->join($tabel2, 'tb_barang.id_merk = tb_merk.id_merk');
        $builder->where('tb_merk.nama_merk', $value);
        return $builder->get()->getResultArray();
    }

    public function getJumlah($id)
    {
        $builder = $this->db->table('tb_barang');
        $builder->select('stok,jumlah_terjual');
        $builder->where('id_barang', $id);
        return $builder->get()->getResultArray();
    }

    public function getDetail($param = null, $param2 = null)
    {
        $builder = $this->db->table('tb_barang');
        if ($param == null) {
            if ($param2 = null) {
                $builder->where('jumlah_terjual >', 10);
                $builder->orderBy('id_barang', 'RANDOM');
            } else {
                $builder->where('jumlah_terjual >', 10);
                $builder->orderBy('id_barang', 'RANDOM');
            }
        } else if ($param == 'tb_kategori_barang') {
            if ($param2 == null) {
                $builder->join('tb_kategori_barang', 'tb_kategori_barang.id_kategori = tb_barang.id_kategori_barang');
                $builder->where('nama_kategori', function ($builder) {
                    return $builder->select('nama_kategori')->from('tb_kategori_barang')->orderBy('id_kategori', 'RANDOM')->limit(1);
                });
                $builder->orderBy('id_barang', 'RANDOM');
            } else {
                $builder->join('tb_kategori_barang', 'tb_kategori_barang.id_kategori = tb_barang.id_kategori_barang');
                $builder->where('nama_kategori', function ($builder) {
                    return $builder->select('nama_kategori')->from('tb_kategori_barang')->orderBy('id_kategori', 'RANDOM')->limit(1);
                });
                $builder->orderBy('id_barang', 'RANDOM');
            }
        } else {
            if ($param2 == null) {
                $builder->join('tb_merk', 'tb_merk.id_merk = tb_barang.id_merk');
                $builder->orderBy('nama_merk', 'RANDOM');
            } else {
                $builder->join('tb_merk', 'tb_merk.id_merk = tb_barang.id_merk');
                $builder->orderBy('nama_merk', 'RANDOM');
            }
        }
        $builder->limit(3);
        return $builder->get()->getResultArray();
    }
}
