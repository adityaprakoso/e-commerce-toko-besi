<?php namespace App\Models;

use CodeIgniter\Model;

class PegawaiModel extends Model
{
    protected $table = 'tb_pegawai';
    protected $useTimestamps = true;
    protected $allowedFields = ['nama_lengkap', 'username', 'password', 'alamat', 'no_hp'];
    protected $primaryKey = 'id_pegawai';
    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];

    protected function beforeInsert(array $data){
        $data = $this->passwordHash($data);
        return $data;
    }

    protected function beforeUpdate(array $data){
        $data = $this->passwordHash($data);
        return $data;
    }

    protected function passwordHash(array $data){
        if(isset($data['data']['password']))
        $data['data']['password'] = password_hash($data['data']['password'], PASSWORD_DEFAULT);
        return $data;
    }

    public function getValue($tabel, $id=null){

        if($id == null){
            return $this->findAll();
        }

        return $this->where(['id_pegawai' => $id])->first();
    }
    

}