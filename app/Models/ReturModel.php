<?php

namespace App\Models;

use CodeIgniter\Model;

class ReturModel extends Model
{
    protected $table = 'tb_retur';
    protected $useTimestamps = true;
    protected $allowedFields = ['id_retur', 'id_pegawai', 'tanggal', 'total', 'id_supplier'];
    protected $primaryKey = 'id_retur';


    public function getData($id = null)
    {
        $builder = $this->db->table('tb_retur');
        $builder->where('tb_retur.id_retur', $id);
        return $builder->get()->getResultArray();
    }
    public function getRetur($id = null)
    {
        $builder = $this->db->table('tb_retur');
        $builder->join('tb_pegawai', 'tb_pegawai.id_pegawai = tb_pegawai.id_pegawai');
        $builder->join('tb_detail_retur', 'tb_detail_retur.id_retur = tb_retur.id_retur');
        $builder->join('tb_barang', 'tb_barang.id_barang = tb_detail_retur.id_barang');
        $builder->join('tb_supplier', 'tb_barang.id_supplier = tb_supplier.id_supplier');
        $builder->where('tb_retur.id_retur', $id);
        return $builder->get()->getResultArray();
    }
}
