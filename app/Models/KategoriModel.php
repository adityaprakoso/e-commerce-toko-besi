<?php namespace App\Models;

use CodeIgniter\Model;

class KategoriModel extends Model
{
    protected $table = 'tb_kategori_barang';
    protected $useTimestamps = false;
    protected $allowedFields = ['nama_kategori'];
    protected $primaryKey = 'id_kategori';

    public function getValue($tabel, $id=null){

        if($id == null){
            return $this->findAll();
        }

        return $this->where(['id_kategori' => $id])->first();
    }

}