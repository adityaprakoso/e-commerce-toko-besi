<?php

namespace App\Models;

use CodeIgniter\Model;

class PembelianModel extends Model
{
    protected $table = 'tb_pembelian';
    protected $useTimestamps = true;
    protected $allowedFields = ['id_pembelian', 'tanggal', 'total_bayar', 'ppn', 'harga_akhir', 'id_pegawai', 'faktur'];
    protected $primaryKey = 'id_pembelian';

    public function getData($id = null)
    {
        if ($id == null) {
            $builder = $this->db->table('tb_pembelian');
            $builder->join('tb_pegawai', 'tb_pegawai.id_pegawai=tb_pembelian.id_pegawai');
            $builder->orderBy('tanggal', 'DESC');
        } else {
            $builder = $this->db->table('tb_pembelian');
            $builder->join('tb_pegawai', 'tb_pegawai.id_pegawai=tb_pembelian.id_pegawai');
            $builder->where('tb_pembelian.id_pembelian', $id);
        }
        return $builder->get()->getResultArray();
    }

    public function detailPembelian($id)
    {
        $builder = $this->db->table('tb_detail_pembelian');
        $builder->join('tb_barang', 'tb_barang.id_barang=tb_detail_pembelian.id_barang');
        $builder->join('tb_supplier', 'tb_barang.id_supplier=tb_supplier.id_supplier');
        $builder->where('tb_detail_pembelian.id_pembelian', $id);
        return $builder->get()->getResultArray();
    }

    public function getPengeluaran($table, $tanggal = null)
    {
        $builder = $this->db->table($table);
        if ($tanggal == NULL) {
            $builder->selectSum('harga_akhir');
        } else {
            $builder->selectSum('harga_akhir');
            $builder->where('tanggal', $tanggal);
        }
        return $builder->get()->getResultArray();
    }

    public function getIdPembelian($temp)
    {
        $builder = $this->db->table('tb_pembelian');
        $builder->selectMax('id_pembelian');
        $builder->like('id_pembelian', $temp);
        return $builder->get()->getResultArray();
    }
}
