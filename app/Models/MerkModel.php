<?php namespace App\Models;

use CodeIgniter\Model;

class MerkModel extends Model
{
    protected $table = 'tb_merk';
    protected $useTimestamps = false;
    protected $allowedFields = ['id_merk', 'kode_merk', 'nama_merk'];
    protected $primaryKey = 'id_merk';

    public function getValue($tabel, $id=null){

        if($id == null){
            return $this->findAll();
        }

        return $this->where(['id_merk' => $id])->first();
    }

}