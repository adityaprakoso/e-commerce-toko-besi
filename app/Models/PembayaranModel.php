<?php

namespace App\Models;

use CodeIgniter\Model;

class PembayaranModel extends Model
{
    protected $table = 'tb_pembayaran';
    protected $useTimestamps = true;
    protected $allowedFields = ['status_code', 'status_message', 'transaction_id', 'order_id', 'gross_amount', 'payment_type', 'transaction_time', 'transaction_status', 'bank', 'va_number', 'fraud_status', 'bca_va_number', 'permata_va_number', 'pdf_url', 'finish_redirect_url', 'bill_key', 'biller_code'];
    protected $primaryKey = 'id_pembayaran';

    public function getValue($id)
    {
        $builder = $this->db->table('tb_pembayaran');
        $builder->select('id_pembayaran');
        $builder->where('transaction_id', $id);
        return $builder->get()->getResultArray();
    }
}
