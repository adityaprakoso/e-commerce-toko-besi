<?php namespace App\Models;

use CodeIgniter\Model;

class DetailPembelianModel extends Model
{
    protected $table = 'tb_detail_pembelian';
    protected $useTimestamps = false;
    protected $allowedFields = ['id_detail_pembelian', 'id_pembelian', 'id_barang', 'jumlah','diskon','harga'];
    protected $primaryKey = 'id_detail_pembelian';

}