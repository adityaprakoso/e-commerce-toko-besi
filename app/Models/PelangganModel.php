<?php

namespace App\Models;

use CodeIgniter\Model;

class PelangganModel extends Model
{
    protected $table = 'tb_pelanggan';
    protected $useTimestamps = true;
    protected $allowedFields = ['nama_lengkap', 'email', 'username', 'password', 'alamat', 'no_hp'];
    protected $primaryKey = 'id_pelanggan';
    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];

    protected function beforeInsert(array $data)
    {
        $data = $this->passwordHash($data);
        return $data;
    }

    protected function beforeUpdate(array $data)
    {
        $data = $this->passwordHash($data);
        return $data;
    }

    protected function passwordHash(array $data)
    {
        if (isset($data['data']['password']))
            $data['data']['password'] = password_hash($data['data']['password'], PASSWORD_DEFAULT);
        return $data;
    }

    public function getValue($tabel, $id = null)
    {

        if ($id == null) {
            return $this->findAll();
        }

        return $this->where(['id_pelanggan' => $id])->first();
    }

    public function getPelanggan()
    {
        $builder = $this->db->table('tb_pelanggan');
        return $builder->countAll();
    }
}
