<?php

namespace App\Models;

use CodeIgniter\Model;

class TransaksiModel extends Model
{
    protected $table = 'tb_transaksi';
    protected $useTimestamps = true;
    protected $allowedFields = ['transaction_id', 'order_id', 'id_pelanggan', 'tanggal', 'total_bayar', 'status'];
    protected $primaryKey = 'transaction_id';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    public function getTable($tabel, $status = NULL, $order_id = null)
    {
        $builder = $this->db->table($tabel);
        if ($order_id == null) {
            $builder->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan = tb_transaksi.id_pelanggan');
            $builder->where('tb_transaksi.status', $status);
            return $builder->get()->getResultArray();
        } else {
            $builder->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan = tb_transaksi.id_pelanggan');
            $builder->join('tb_pembayaran', 'tb_pembayaran.transaction_id = tb_transaksi.transaction_id');
            $builder->where('tb_transaksi.order_id', $order_id);
            return $builder->get()->getResultArray();
        }
    }

    public function getDetail($tabel, $order_id = null)
    {
        $builder = $this->db->table($tabel);
        $builder->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan = tb_transaksi.id_pelanggan');
        $builder->join('tb_detail_transaksi', 'tb_detail_transaksi.order_id = tb_transaksi.order_id');
        $builder->join('tb_barang', 'tb_detail_transaksi.id_barang = tb_barang.id_barang');
        $builder->where('tb_transaksi.order_id', $order_id);
        return $builder->get()->getResultArray();
    }

    public function getTablePengiriman($table)
    {
        $builder = $this->db->table($table);
        $builder->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan = tb_transaksi.id_pelanggan');
        $builder->where('tb_transaksi.status', 'Siap Kirim');
        $builder->orWhere('tb_transaksi.status', 'Selesai');
        return $builder->get()->getResultArray();
    }

    public function getTransaksi($table, $status = null)
    {
        $builder = $this->db->table($table);
        if ($status == null) {
            $builder->where('status !=', 'Selesai');
            $builder->where('tanggal', date('Y-m-d'));
        } else {
            $builder->where('status', $status);
            $builder->where('tanggal', date('Y-m-d'));
        }
        return $builder->countAllResults();
    }

    public function getTransaksiUser($table, $user)
    {
        $builder = $this->db->table($table);
        $builder->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan = tb_transaksi.id_pelanggan');
        $builder->where('tb_pelanggan.email', $user);
        return $builder->get()->getResultArray();
    }

    public function getPemasukan($table, $tanggalMulai = null, $tanggalSelesai = null)
    {
        $builder = $this->db->table($table);
        if ($tanggalMulai == NULL) {
            $builder->selectSum('total_bayar');
            $builder->where('status', 'Selesai');
        } else {
            $builder->selectSum('total_bayar');
            $builder->where('status', 'Selesai');
            $array = ['tanggal >=' => $tanggalMulai, 'tanggal <=' => $tanggalSelesai];
            $builder->where($array);
        }
        return $builder->get()->getResultArray();
    }

    public function getBarangTerjual()
    {
        $builder = $this->db->table('tb_transaksi');
        $builder->join('tb_detail_transaksi', 'tb_transaksi.order_id = tb_detail_transaksi.order_id');
        $builder->join('tb_barang', 'tb_barang.id_barang = tb_detail_transaksi.id_barang');
        $builder->where('status', 'Selesai');
        $builder->orderBy('tanggal', 'ASC');
        return $builder->get()->getResultArray();
    }
}
