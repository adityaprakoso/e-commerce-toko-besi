<?php

namespace App\Controllers;

use App\Models\BarangModel;
use Wildanfuady\WFcart\WFcart;


class Produk extends BaseController
{

	protected $barangModel;

	public function __construct()
	{
		$this->barangModel = new BarangModel();
		$this->cart = new WFcart();
	}

	public function index()
	{
		$id_barang = $this->request->getGet('id_barang');
		$barang = $this->barangModel->getTable('tb_barang', 'id_barang', $id_barang);
		// var_dump($barang[0]['stok']);die;
		$data = [
			'title' => 'TB. Dadi Makmur',
			'barang' => $barang,
			'keranjang' => $this->cart->totals()
		];
		return view('users/produk', $data);
	}

	//--------------------------------------------------------------------

}
