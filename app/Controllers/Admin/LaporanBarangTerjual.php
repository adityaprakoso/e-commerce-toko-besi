<?php

namespace App\Controllers\admin;

use App\Controllers\BaseController;
use App\Models\TransaksiModel;

class LaporanBarangTerjual extends BaseController
{
    protected $kategoriModel;

    public function __construct()
    {
        $this->transaksiModel = new TransaksiModel();
    }

    public function index()
    {
        if (!isset($_SESSION['username'])) {
            return redirect()->to(base_url() . '/admin/login');
        }
        $data = [
            'title' => 'Barang Terjual',
            'barangTerjual' => $this->transaksiModel->getBarangTerjual()
        ];

        return view('admin/barangTerjual', $data);
    }
}
