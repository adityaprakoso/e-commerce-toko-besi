<?php

namespace App\Controllers\admin;

use App\Controllers\BaseController;
use App\Models\TransaksiModel;
use phpDocumentor\Reflection\Types\Null_;

class Pemesanan extends BaseController
{
	protected $transaksiModel;

	public function __construct()
	{
		$this->transaksiModel = new TransaksiModel();
	}

	public function index()
	{
		if (!isset($_SESSION['username'])) {
			return redirect()->to(base_url() . '/admin/login');
		}
		$data = [
			'title' => 'Data Pemesanan',
			'transaksi_success' => $this->transaksiModel->getTable('tb_transaksi', 'Success'),
			'transaksi_cancel' => $this->transaksiModel->getTable('tb_transaksi', 'Cancel'),
			'transaksi_pending' => $this->transaksiModel->getTable('tb_transaksi', 'pending'),
			'transaksi_expired' => $this->transaksiModel->getTable('tb_transaksi', 'Expired')
		];
		return view('admin/pemesanan', $data);
	}

	public function getDetail()
	{
		$order_id = $this->request->getVar('order_id');
		$status = Null;
		$data = $this->transaksiModel->getTable('tb_transaksi', $status, $order_id);
		$detail =  $this->transaksiModel->getDetail('tb_transaksi', $order_id);

		foreach ($data as $value) :
			echo '<div id="transaction_id">' . $value['transaction_id'] . '</div>';
			echo '<div id="order_id">' . $value['order_id'] . '</div>';
			echo '<div id="payment_type">' . $value['payment_type'] . '</div>';
			echo '<div id="tanggal">' . $value['tanggal'] . '</div>';
			echo '<div id="total_bayar">' . $value['total_bayar'] . '</div>';
			echo '<div id="status">' . $value['status'] . '</div>';
			echo '<div id="transaction_status">' . $value['transaction_status'] . '</div>';
			echo '<div id="status_message">' . $value['status_message'] . '</div>';
			echo '<div id="nama_lengkap">' . $value['nama_lengkap'] . '</div>';
			echo '<div id="email">' . $value['email'] . '</div>';
			echo '<div id="no_hp">' . $value['no_hp'] . '</div>';
			echo '<div id="alamat">' . $value['alamat'] . '</div>';
			echo '<div id="bank">' . $value['bank'] . '</div>';
			echo '<div id="fraud_status">' . $value['fraud_status'] . '</div>';
			echo '<div id="va_number">' . $value['va_number'] . '</div>';
		endforeach;
		echo '<div id="tabel">'
?>
		<table class="table">
			<tr>
				<td>No</td>
				<td>Nama Barang</td>
				<td>Kuantitas</td>
				<td>Unit Price</td>
				<td>Subtotal</td>
			</tr>
			<?php
			$no = 1;
			foreach ($detail as $key) {
			?>
				<tr>
					<td><?php echo $no++;  ?></td>
					<td><?php echo $key['nama_barang']; ?></td>
					<td><?php echo $key['jumlah']; ?></td>
					<td><?php echo $key['harga_jual']; ?></td>
					<td><?php echo $key['jumlah'] * $key['harga_jual']; ?></td>
				</tr>
			<?php } ?>
			<tr>
				<td colspan="4"><label><b>Total</b></label></td>
				<td><label><b><?php echo "Rp " . number_format($key['total_bayar'], 2, ',', '.'); ?></b></label></td>
			</tr>
		</table>
<?php
		'</div>';
	}

	public function kirimBarang($transaction_id)
	{
		$data = array('status' => 'Siap Kirim',);
		$this->transaksiModel->update($transaction_id, $data);
		return redirect()->to(base_url() . '/pemesanan');
	}
}
