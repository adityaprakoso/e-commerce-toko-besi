<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\TransaksiModel;
use TCPDF;

class Pengiriman extends BaseController
{
    public function __construct()
    {
        $this->transaksiModel = new TransaksiModel();
    }

    public function index()
    {
        if (!isset($_SESSION['username'])) {
            return redirect()->to(base_url() . '/admin/login');
        }
        $data = [
            'title' => 'Barang Siap Kirim',
            'pengiriman' => $this->transaksiModel->getTablePengiriman('tb_transaksi'),
        ];
        return view('admin/pengiriman', $data);
    }

    public function selesai()
    {
        $id = $this->request->getPost('id');
        $data = array('status' => 'Selesai');
        $this->transaksiModel->update($id, $data);
    }

    public function cetak()
    {
        $order_id = base64_decode($this->request->getVar('id'));
        $status = null;
        $transaksi = $this->transaksiModel->getTable('tb_transaksi', $status, $order_id);
        $detail = $this->transaksiModel->getDetail('tb_transaksi', $order_id);

        $html = view('admin/layout/suratJalan', [
            'transaksi' => $transaksi,
            'detail' => $detail,
        ]);

        $pdf = new TCPDF('L', PDF_UNIT, 'A4', true, 'UTF-8', false);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('TB.DADI MAKMUR');
        $pdf->SetTitle('Surat Jalan');
        $pdf->SetSubject('Surat Jalan');

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
        $pdf->setFooterData(array(0, 64, 0), array(0, 64, 128));

        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->AddPage();

        $pdf->writeHTML($html, true, false, true, false, '');
        $this->response->setContentType('application/pdf');
        $pdf->Output('invoice.pdf', 'I');
    }
}
