<?php

namespace App\Controllers\admin;

use App\Controllers\BaseController;
use App\Models\BarangModel;
use App\Models\DetailTransaksiModel;
use App\Models\TransaksiModel;
use App\Models\PembayaranModel;


class Notification extends BaseController
{
    public function __construct()
    {
        $this->transaksiModel = new TransaksiModel();
        $this->pembayaranModel = new PembayaranModel();
        $this->detailTransaksiModel = new DetailTransaksiModel();
        $this->barangModel = new BarangModel();
    }

    public function restore($order_id)
    {
        $item = $this->detailTransaksiModel->getDetail($order_id);
        foreach ($item as $key) {

            $jumlah = $this->barangModel->getJumlah($key['id_barang']);

            $jumahBeli = $key['jumlah'];

            $updateJumlah = $jumlah['0']['jumlah_terjual'] - $jumahBeli;
            $updateStok = $jumlah['0']['stok'] + $jumahBeli;

            $data = array(
                'stok' => $updateStok,
                'jumlah_terjual' => $updateJumlah,
            );
            $this->barangModel->update($key['id_barang'], $data);
        }
    }

    public function index()
    {

        \Midtrans\Config::$isProduction = false;
        \Midtrans\Config::$serverKey = 'SB-Mid-server-brjTBDPqUphNNM2v5cxulJQN';
        $notif = new \Midtrans\Notification();

        $transaction_status = $notif->transaction_status;
        $type = $notif->payment_type;
        $order_id = $notif->order_id;
        $transaction_id = $notif->transaction_id;
        $fraud = $notif->fraud_status;
        $id_pembayaran = $this->getIdPembayaran($transaction_id);


        if ($transaction_status == 'capture') {
            // For credit card transaction, we need to check whether transaction is challenge by FDS or not
            if ($type == 'credit_card') {
                if ($fraud == 'challenge') {
                    // TODO set payment status in merchant's database to 'Challenge by FDS'
                    //update data transaksi
                    $data = [
                        'status' => 'Challenge by FDS'
                    ];
                    $data_pembayaran = [
                        'transaction_status' => 'Challenge by FDS'
                    ];
                    $this->transaksiModel->update($transaction_id, $data);
                    $this->pembayaranModel->update($id_pembayaran, $data_pembayaran);

                    // TODO merchant should decide whether this transaction is authorized or not in MAP
                    // echo "Transaction order_id: " . $order_id . " is challenged by FDS";
                } else {
                    // TODO set payment status in merchant's database to 'Success'
                    $data = [
                        'status' => 'Success'
                    ];
                    $data_pembayaran = [
                        'transaction_status' => 'Settlement'
                    ];
                    $this->transaksiModel->update($transaction_id, $data);
                    $this->pembayaranModel->update($id_pembayaran, $data_pembayaran);
                    // echo "Transaction order_id: " . $order_id . " successfully captured using " . $type;
                }
            }
        } else if ($transaction_status == 'settlement') {
            // TODO set payment status in merchant's database to 'Settlement'
            $data = [
                'status' => 'Success'
            ];
            $data_pembayaran = [
                'transaction_status' => 'Settlement'
            ];
            $this->transaksiModel->update($transaction_id, $data);
            $this->pembayaranModel->update($id_pembayaran, $data_pembayaran);
            // echo "Transaction order_id: " . $order_id . " successfully transfered using " . $type;

        } else if ($transaction_status == 'pending') {
            // TODO set payment status in merchant's database to 'Pending'
            $data = [
                'status' => 'Pending'
            ];
            $data_pembayaran = [
                'transaction_status' => 'Settlement'
            ];
            $this->transaksiModel->update($transaction_id, $data);
            $this->pembayaranModel->update($id_pembayaran, $data_pembayaran);
            // echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;

        } else if ($transaction_status == 'deny') {
            $data = [
                'status' => 'Denied'
            ];
            $data_pembayaran = [
                'transaction_status' => 'Failed'
            ];
            // TODO set payment status in merchant's database to 'Denied'
            $this->restore($order_id);
            $this->transaksiModel->update($transaction_id, $data);
            $this->pembayaranModel->update($id_pembayaran, $data_pembayaran);
            // echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";

        } else if ($transaction_status == 'expire') {
            $data = [
                'status' => 'Expired'
            ];
            $data_pembayaran = [
                'transaction_status' => 'Failed'
            ];
            // TODO set payment status in merchant's database to 'expire'
            $this->restore($order_id);
            $this->transaksiModel->update($transaction_id, $data);
            $this->pembayaranModel->update($id_pembayaran, $data_pembayaran);
            // echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is expired.";

        } else if ($transaction_status == 'cancel') {
            $data = [
                'status' => 'Cancel'
            ];
            $data_pembayaran = [
                'transaction_status' => 'Failed'
            ];
            // TODO set payment status in merchant's database to 'Denied'
            $this->restore($order_id);
            $this->transaksiModel->update($transaction_id, $data);
            $this->pembayaranModel->update($id_pembayaran, $data_pembayaran);
            // echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is canceled.";
        }
    }

    public function getIdPembayaran($id)
    {
        $value = $this->pembayaranModel->getValue($id);
        return $value[0]['id_pembayaran'];
    }
}
