<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\TransaksiModel;

class LaporanPengirimanBarang extends BaseController
{
    public function __construct()
    {
        $this->transaksiModel = new TransaksiModel();
    }

    public function index()
    {
        if (!isset($_SESSION['username'])) {
            return redirect()->to(base_url() . '/admin/login');
        }
        $data = [
            'title' => 'Barang Siap Kirim',
            'pengiriman' => $this->transaksiModel->getTablePengiriman('tb_transaksi'),
        ];
        return view('admin/laporanPengiriman', $data);
    }
}
