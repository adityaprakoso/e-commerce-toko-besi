<?php

namespace App\Controllers\admin;

use App\Controllers\BaseController;
use App\Models\TransaksiModel;
use App\Libraries\Datatables;

class LaporanKeuangan extends BaseController
{
	public function __construct()
	{
		$this->transaksiModel = new TransaksiModel();
	}

	public function index()
	{
		if (!isset($_SESSION['username'])) {
			return redirect()->to(base_url() . '/admin/login');
		}
		$data = [
			'title' => 'Laporan Keuangan',
			'pemasukan' => $this->transaksiModel->getPemasukan('tb_transaksi'),
		];

		return view('admin/laporanKeuangan', $data);
	}

	public function getData()
	{
		$datatables = new Datatables;

		$startDate = date('Y-m-d', strtotime($this->request->getPost('startDate')));
		$endDate = date('Y-m-d', strtotime($this->request->getPost('endDate')));

		$datatables->table('tb_transaksi')->select('order_id, tanggal, total_bayar');
		$datatables->where(['status' => 'Selesai']);

		if ($startDate == "1969-12-31") {
			echo $datatables->draw();
		} else {
			$datatables->where(['tb_transaksi.tanggal >=' => $startDate, 'tb_transaksi.tanggal <=' => $endDate]);
			echo $datatables->draw();
		}
	}

	public function getPemasukanPeriode()
	{
		$startDate = date('Y-m-d', strtotime($this->request->getPost('start')));
		$endDate = date('Y-m-d', strtotime($this->request->getPost('finish')));
		$jumlah = $this->transaksiModel->getPemasukan('tb_transaksi', $startDate, $endDate);
		echo '<div id="totalPemasukan">' . $jumlah[0]['total_bayar'] . '</div>';
	}

	public function getDetail()
	{
		$id = $this->request->getPost('id');
		$detail =  $this->transaksiModel->getDetail('tb_transaksi', $id);

		echo '<div id="nama_lengkap">' . $detail[0]['nama_lengkap'] . '</div>';
		echo '<div id="email">' . $detail[0]['email'] . '</div>';
		echo '<div id="no_hp">' . $detail[0]['no_hp'] . '</div>';
		echo '<div id="alamat">' . $detail[0]['alamat'] . '</div>';

		echo '<div id="tabel">'
?>
		<table class="table">
			<tr>
				<td>No</td>
				<td>Nama Barang</td>
				<td>Kuantitas</td>
				<td>Unit Price</td>
				<td>Subtotal</td>
			</tr>
			<?php
			$no = 1;
			foreach ($detail as $key) {
			?>
				<tr>
					<td><?php echo $no++;  ?></td>
					<td><?php echo $key['nama_barang']; ?></td>
					<td><?php echo $key['jumlah']; ?></td>
					<td><?php echo $key['harga_jual']; ?></td>
					<td><?php echo $key['jumlah'] * $key['harga_jual']; ?></td>
				</tr>
			<?php } ?>
			<tr>
				<td colspan="4"><label><b>Total Pemasukan</b></label></td>
				<td><label><b><?php echo "Rp " . number_format($key['total_bayar'], 2, ',', '.'); ?></b></label></td>
			</tr>
		</table>
<?php
		'</div>';
	}
}
