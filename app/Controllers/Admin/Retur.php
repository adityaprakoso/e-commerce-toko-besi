<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\ReturModel;
use App\Models\SupplierModel;
use App\Libraries\Datatables;

class Retur extends BaseController
{
    public function __construct()
    {
        $this->supplierModel = new SupplierModel();
        $this->returModel = new ReturModel();
    }

    public function index()
    {
        if (!isset($_SESSION['username'])) {
            return redirect()->to(base_url() . '/admin/login');
        }
        $data = [
            'title' => 'Barang Retur',
            // 'retur' => $this->returModel->getRetur(),
            'supplier' => $this->supplierModel->getValue('tb_supplier')
        ];
        return view('admin/retur', $data);
    }

    public function getDetail()
    {
        $id_retur = $this->request->getVar('id');
        $detail = $this->returModel->getRetur($id_retur);
?>
        <table class="table">
            <tr>
                <td>No</td>
                <td>Barang</td>
                <td>Jumlah Retur</td>
                <td>Keterangan</td>
                <td>Total</td>
            </tr>
            <?php
            $no = 1;
            $total = 0;
            foreach ($detail as $key) {
            ?>
                <tr>
                    <td><?php echo $no++;  ?></td>
                    <td><?php echo $key['nama_barang']; ?></td>
                    <td><?php echo $key['jumlah']; ?></td>
                    <td><?php echo $key['keterangan']; ?></td>
                    <td><?php echo $key['pengembalian']; ?></td>
                </tr>
            <?php $total += $key['pengembalian'];
            } ?>
            <tr>
                <td colspan="4"><label><b>Total Pengembalian</b></label></td>
                <td><label><b><?php echo "Rp " . number_format($total, 2, ',', '.'); ?></b></label></td>
            </tr>
        </table>
<?php
    }

    public function getRetur()
    {
        $datatables = new Datatables;

        $supplier = $this->request->getPost('supplier');
        $startDate = date('Y-m-d', strtotime($this->request->getPost('startDate')));;
        $endDate = date('Y-m-d', strtotime($this->request->getPost('endDate')));

        $datatables->table('tb_retur');
        $datatables->join('tb_pegawai', 'tb_pegawai.id_pegawai = tb_pegawai.id_pegawai');
        $datatables->join('tb_supplier', 'tb_retur.id_supplier = tb_supplier.id_supplier');

        if ($supplier == "") {
            echo $datatables->draw();
        } else {
            $datatables->where(['tb_retur.id_supplier' => $supplier, 'tb_retur.tanggal >=' => $startDate, 'tb_retur.tanggal <=' => $endDate]);
            echo $datatables->draw();
        }
    }
}
