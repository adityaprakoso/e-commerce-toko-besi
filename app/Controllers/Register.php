<?php

namespace App\Controllers;

use App\Models\PelangganModel;

class Register extends BaseController
{
    protected $pelangganModel;

    public function __construct()
    {
        helper(['form']);
        $this->form_validation = \Config\Services::validation();
        $this->pelangganModel = new PelangganModel();
    }
    public function index()
    {
        return view('users/register');
    }

    public function register()
    {
        $data = $this->request->getPost();
        if ($this->form_validation->run($data, 'register') == FALSE) {
            // mengembalikan nilai input yang sudah dimasukan sebelumnya
            session()->setFlashdata('inputs', $this->request->getPost());
            // memberikan pesan error pada saat input data
            session()->setFlashdata('errors', $this->form_validation->getErrors());
            // kembali ke halaman form
            return redirect()->to(base_url('register'));
        } else {
            $this->pelangganModel->save($data);
            session()->setFlashdata('swal', 'ditambah');
            return redirect()->to(base_url() . '/login');
        }

        return view('register', $data);
    }
}
