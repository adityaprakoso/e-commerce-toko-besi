<?php

namespace App\Controllers\users;

use App\Controllers\BaseController;
use App\Models\PembayaranModel;
use App\Models\TransaksiModel;
use App\Models\DetailTransaksiModel;
use App\Models\BarangModel;
use Wildanfuady\WFcart\WFcart;

class Pembayaran extends BaseController
{
    protected $barangModel;
    protected $pembayaranModel;
    protected $transaksiModel;
    protected $detailTransaksiModel;

    public function __construct()
    {
        $this->cart = new WFcart();
        $this->barangModel = new BarangModel();
        $this->pembayaranModel = new PembayaranModel();
        $this->transaksiModel = new TransaksiModel();
        $this->detailTransaksiModel = new DetailTransaksiModel();
    }

    public function index()
    {
        if (!isset($_SESSION['email'])) {
            return redirect()->to(base_url() . '/login');
        }

        $data = [
            'cart' => $this->cart->totals(),
            'profil' => session()->get(),
            'title' => "Pembayaran",
            'snapToken' => $this->getTokenSnap()
        ];

        return view('users/metode_pembayaran', $data);
    }

    public function tambah()
    {
        $id_barang = $this->request->getPost('id_barang');
        $jumlah = $this->request->getPost('jumlah');
        $barang = $this->barangModel->getTable('tb_barang', 'id_barang', $id_barang);

        $id = $id_barang; // ambil dari kode product
        $item = [
            'id'        => $id_barang,
            'name'        => $barang[0]['nama_barang'],
            'price'        => $barang[0]['harga_jual'],
            'photo'        => $barang[0]['gambar'],
            'quantity'    => $jumlah
        ];

        $this->cart->add_cart($id, $item);
        // $this->cart->remove($id);
        echo '<div id="jumlah">' . $jumlah . '</div>';
    }

    public function getTokenSnap()
    {
        \Midtrans\Config::$serverKey = "SB-Mid-server-brjTBDPqUphNNM2v5cxulJQN";
        // Config::$isProduction = true;

        // Enable sanitization
        \Midtrans\Config::$isSanitized = true;

        // Enable 3D-Secure
        \Midtrans\Config::$is3ds = true;

        // Uncomment for append and override notification URL
        // \Midtrans\Config::$appendNotifUrl = "https://example111.com";
        // \Midtrans\Config::$overrideNotifUrl = "https://example1111.com";

        // Required
        $transaction_details = array(
            'order_id' => rand(),
            'gross_amount' => 10000, // no decimal allowed for creditcard
        );

        $item_details = array();
        $item = $this->cart->totals();
        foreach ($item as $value) {
            array_push($item_details, $value);
        }


        $profil = session()->get();
        $shipping_address = array(
            'first_name'   => $profil['nama_lengkap'],
            'last_name'    => "",
            'address'      => $profil['alamat'],
            'phone'        => $profil['no_hp'],
            'country_code' => 'IDN'
        );

        // Optional
        $customer_details = array(
            'first_name'    => $profil['nama_lengkap'],
            'last_name'     => "",
            'email'         => $profil['email'],
            'phone'         => $profil['no_hp'],
            'shipping_address' => $shipping_address
        );

        // Optional, remove this to display all available payment methods
        // $enable_payments = array('credit_card','cimb_clicks','mandiri_clickpay','echannel');

        // Fill transaction details
        $transaction = array(
            // 'enabled_payments' => $enable_payments,
            'transaction_details' => $transaction_details,
            'customer_details' => $customer_details,
            'item_details' => $item_details,
        );

        $snapToken = \Midtrans\Snap::getSnapToken($transaction);
        return $snapToken;
    }


    public function simpanTransaksi()
    {
        $status = $this->request->getPost('status');
        if ($status == 'error') {
            $data = [
                'profil' => session()->get(),
                'title' => "Status Transaksi",
                'status' => "error"
            ];
            return view('users/status', $data);
        } else {

            $result = json_decode($this->request->getPost('result_data'));

            if (isset($result->va_numbers[0]->bank)) {
                $bank = $result->va_numbers[0]->bank;
            } else {
                $bank = '-';
            }

            if (isset($result->va_numbers[0]->va_number)) {
                $va_number = $result->va_numbers[0]->va_number;
            } else {
                $va_number = '-';
            }

            if (isset($result->bca_va_number)) {
                $bca_va_number = $result->bca_va_number;
            } else {
                $bca_va_number = '-';
            }

            if (isset($result->permata_va_number)) {
                $permata_va_number = $result->permata_va_number;
            } else {
                $permata_va_number = '-';
            }

            if (isset($result->bill_key)) {
                $bill_key = $result->bill_key;
            } else {
                $bill_key = '-';
            }

            if (isset($result->biller_code)) {
                $biller_code = $result->biller_code;
            } else {
                $biller_code = '-';
            }

            if (isset($result->fraud_status)) {
                $fraud_status = $result->fraud_status;
            } else {
                $fraud_status = '-';
            }

            if (isset($result->pdf_url)) {
                $pdf_url = $result->pdf_url;
            } else {
                $pdf_url = '-';
            }

            if (isset($result->finish_redirect_url)) {
                $finish_redirect_url = $result->finish_redirect_url;
            } else {
                $finish_redirect_url = '-';
            }


            $profil = session()->get();

            $data_transaksi = array(
                'transaction_id' => $result->transaction_id,
                'order_id' => $result->order_id,
                'id_pelanggan' => $profil['id_pelanggan'],
                'tanggal' => $result->transaction_time,
                'total_bayar' => $result->gross_amount,
                'status' => $result->transaction_status,
            );
            $this->transaksiModel->insert($data_transaksi);

            $item_details = array();
            $data_detail = array();
            $item = $this->cart->totals();

            foreach ($item as $value) {

                $data_detail = array(
                    'order_id' => $result->order_id,
                    'id_barang' => $value['id'],
                    'jumlah' => $value['quantity']
                );
                array_push($item_details, $data_detail);

                $id = $value['id'];
                $jumlahTerjual = $this->barangModel->getJumlah($id);
                $data = array(
                    'jumlah_terjual' => $value['quantity'] + $jumlahTerjual[0]['jumlah_terjual'],
                    'stok'           => $value['max'] - $value['quantity']
                );
                // dd($data);
                $this->barangModel->update($id, $data);
            }
            $this->detailTransaksiModel->simpanDetail($item_details);

            $data_pembayaran = array(
                'status_code' => $result->status_code,
                'status_message' => $result->status_message,
                'transaction_id' => $result->transaction_id,
                'order_id' => $result->order_id,
                'gross_amount' => $result->gross_amount,
                'payment_type' => $result->payment_type,
                'transaction_time' => $result->transaction_time,
                'transaction_status' => $result->transaction_status,
                'bank' => $bank,
                'va_number' => $va_number,
                'fraud_status' => $fraud_status,
                'bca_va_number' => $bca_va_number,
                'permata_va_number' => $permata_va_number,
                'pdf_url' => $pdf_url,
                'finish_redirect_url' => $finish_redirect_url,
                'bill_key' => $bill_key,
                'biller_code' => $biller_code,
            );
            $this->pembayaranModel->save($data_pembayaran);

            $data = [
                'profil' => session()->get(),
                'title' => "Status Transaksi",
                'status' => $status
            ];
            unset($_SESSION['cart']); //destroy session cart
            return view('users/status', $data);
        }
    }
}
