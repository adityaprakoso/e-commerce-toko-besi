<?php

namespace App\Controllers\users;

use App\Controllers\BaseController;
use App\Models\PelangganModel;

class Profil extends BaseController
{
	public function __construct()
	{
		$this->pelangganModel = new PelangganModel();
	}
	public function index()
	{
		if (!isset($_SESSION['email'])) {
			return redirect()->to(base_url() . '/login');
		}

		$data = [
			'title' => 'Profil',
			'profil' => session()->get()
		];
		return view('users/profil', $data);
	}

	public function update()
	{
		$id_pelanggan = $this->request->getPost('id');
		$data = array(
			'id_pelanggan' => $this->request->getPost('id'),
			'nama_lengkap' => $this->request->getPost('nama_lengkap'),
			'email' => $this->request->getPost('email'),
			'alamat' => $this->request->getPost('alamat'),
			'no_hp' => $this->request->getPost('no_hp'),
		);
		$this->pelangganModel->update($id_pelanggan, $data);
		session()->set($data);
		session()->setFlashdata('swal', 'dirubah');
		return redirect()->to(base_url() . '/profil');
	}
}
