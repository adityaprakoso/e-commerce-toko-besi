<?php

namespace App\Controllers\users;

use App\Controllers\BaseController;

use App\Models\BarangModel;

class Store extends BaseController
{


	public function __construct()
	{
		$this->barangModel = new BarangModel();
	}

	public function index()
	{
		$data = [
			'title' => 'Store',
			'kategori' => $this->barangModel->getTable('tb_kategori_barang'),
			'merk' => $this->barangModel->getTable('tb_merk'),
			'barang' => $this->barangModel->getTable('tb_barang')
		];
		return view('users/store', $data);
	}

	public function cari()
	{

		$value = $this->request->getVar('nama_barang');
		$data = [
			'title' => 'Pencarian barang',
			'nama_barang' => $value,
			'kategori' => $this->barangModel->getTable('tb_kategori_barang'),
			'merk' => $this->barangModel->getTable('tb_merk'),
			'barang' => $this->barangModel->cariBarang('tb_barang', $value)
		];
		return view('users/store', $data);
	}

	public function cari_by_kategori($value)
	{
		$data = [
			'title' => 'Pencarian barang berdasarkan',
			'nama_barang' => $value,
			'kategori' => $this->barangModel->getTable('tb_kategori_barang'),
			'merk' => $this->barangModel->getTable('tb_merk'),
			'barang' => $this->barangModel->getBarangByKategori('tb_barang', 'tb_kategori_barang', $value)
		];
		return view('users/store', $data);
	}

	public function cari_by_merk($value)
	{
		$data = [
			'title' => 'Pencarian barang berdasarkan',
			'nama_barang' => $value,
			'kategori' => $this->barangModel->getTable('tb_kategori_barang'),
			'merk' => $this->barangModel->getTable('tb_merk'),
			'barang' => $this->barangModel->getBarangByMerk('tb_barang', 'tb_merk', $value)
		];
		return view('users/store', $data);
	}
}
