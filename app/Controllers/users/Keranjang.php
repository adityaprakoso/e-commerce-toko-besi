<?php

namespace App\Controllers\users;

use App\Controllers\BaseController;
use App\Models\BarangModel;
use Wildanfuady\WFcart\WFcart;

class Keranjang extends BaseController
{

    protected $barangModel;

    public function __construct()
    {
        $this->cart = new WFcart();
        $this->barangModel = new BarangModel();
    }

    public function index()
    {
        if (!isset($_SESSION['email'])) {
            return redirect()->to(base_url() . '/login');
        }

        $data = [
            'cart' => $this->cart->totals(),
            'title' => "Keranjang"
        ];

        return view('users/keranjang', $data);
    }

    public function tambah()
    {
        if (!isset($_SESSION['email'])) {
            return "tidak";
        }

        $id_barang = $this->request->getPost('id_barang');
        $jumlah = $this->request->getPost('jumlah');
        $barang = $this->barangModel->getTable('tb_barang', 'id_barang', $id_barang);

        $id = $id_barang; // ambil dari kode product
        $item = [
            'id'        => $id_barang,
            'name'        => $barang[0]['nama_barang'],
            'price'        => $barang[0]['harga_jual'],
            'photo'        => $barang[0]['gambar'],
            'quantity'    => $jumlah,
            'min'         => $barang[0]['minimal_pembelian'],
            'max'         => $barang[0]['stok']
        ];

        $this->cart->add_cart($id, $item);
        // $this->cart->remove($id);

        echo '<div id="jumlah">' . $jumlah . '</div>';
    }

    public function set_jumlah() //untuk menyesuaikan jumlah barang dikeranjang
    {
        $id = $this->request->getPost('id');
        $jumlah = $this->request->getPost('jumlah');
        $_SESSION['cart'][$id]['quantity'] = $jumlah;
        return $jumlah;
    }

    public function hapus()
    {
        $id = $this->request->getPost('id');
        $this->cart->remove($id);
    }

    //--------------------------------------------------------------------

}
