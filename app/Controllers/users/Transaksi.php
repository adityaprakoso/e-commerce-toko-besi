<?php

namespace App\Controllers\users;

use App\Controllers\BaseController;
use App\Models\BarangModel;
use App\Models\TransaksiModel;
use Wildanfuady\WFcart\WFcart;

class Transaksi extends BaseController
{

    protected $barangModel;

    public function __construct()
    {
        $this->cart = new WFcart();
        $this->barangModel = new BarangModel();
        $this->transaksiModel = new TransaksiModel();
    }

    public function index()
    {
        if (!isset($_SESSION['email'])) {
            return redirect()->to(base_url() . '/login');
        }

        $data = [
            'cart' => $this->cart->totals(),
            'title' => "Transaksi",
            'transaksi' => $this->transaksiModel->getTransaksiUser('tb_transaksi', $_SESSION['email'])
        ];

        return view('users/transaksi', $data);
    }

    public function batal()
    {
        $orderId = $this->request->getPost('id');
        \Midtrans\Transaction::cancel($orderId);
    }

    public function finish()
    {
        $data = [
            'profil' => session()->get(),
            'title' => "Status Transaksi",
            'status' => "sukses"
        ];
        unset($_SESSION['cart']);
        return view('users/status', $data);
    }

    public function unfinish()
    {
        $data = [
            'profil' => session()->get(),
            'title' => "Status Transaksi",
            'status' => "gagal"
        ];
        unset($_SESSION['cart']);
        return view('users/status', $data);
    }

    public function error()
    {

        $data = [
            'profil' => session()->get(),
            'title' => "Status Transaksi",
            'status' => "error"
        ];
        unset($_SESSION['cart']);
        return view('users/status', $data);
    }
}
