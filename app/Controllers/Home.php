<?php

namespace App\Controllers;

use App\Models\BarangModel;


class Home extends BaseController
{

	protected $barangModel;

	public function __construct()
	{
		$this->barangModel = new BarangModel();
	}

	public function index()
	{
		$data = [
			'title' => 'TB. Dadi Makmur',
			'barang' => $this->barangModel->getTable('tb_barang'),
			'barang_kategori' => $this->barangModel->getDetail('tb_kategori_barang', null),
			'barang_kategori_2' => $this->barangModel->getDetail('tb_kategori_barang', 2),
			'barang_merk' => $this->barangModel->getDetail('tb_merk'),
			'barang_merk_2' => $this->barangModel->getDetail('tb_merk', 2),
			'barang_laku' => $this->barangModel->getDetail(),
			'barang_laku_2' => $this->barangModel->getDetail(null, 2),
		];
		return view('users/home', $data);
	}

	//--------------------------------------------------------------------

}
