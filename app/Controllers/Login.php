<?php

namespace App\Controllers;

use App\Models\PegawaiModel;
use App\Models\PelangganModel;

class Login extends BaseController
{
	protected $pelangganModel;

	public function __construct()
	{
		$this->pelangganModel = new PelangganModel();
		$this->pegawaiModel = new PegawaiModel();
	}

	public function index()
	{
		return view('users/login');
	}

	public function login_admin()
	{
		return view('admin/login');
	}

	public function proses_login()
	{
		$data = [];
		helper(['form']);

		$rules = [
			'email' => 'required|valid_email|min_length[3]',
			'password' => 'required|min_length[3]|validateUser[email,password]'
		];

		$errors = [
			'password' => [
				'validateUser' => 'Email atau password tidak sesuai.'
			]
		];

		if (!$this->validate($rules, $errors)) {
			//jika username atau password tidak sesuai
			$data['validation'] = $this->validator;
			return view('users/login', $data);
		} else {
			//berhasil login
			$email = $this->request->getPost('email');
			$user = $this->pelangganModel->where('email', $email)->first();
			$this->simpanSession($user);
			// dd(session()->get('email'));
			return redirect()->to(base_url() . '/home');
		}
	}


	public function proses_login_admin()
	{
		$data = [];
		helper(['form']);

		$rules = [
			'username' => 'required|min_length[3]',
			'password' => 'required|min_length[3]|validateAdmin[username,password]'
		];

		$errors = [
			'password' => [
				'validateUser' => 'Username atau password tidak sesuai.'
			]
		];

		if (!$this->validate($rules, $errors)) {
			//jika username atau password tidak sesuai
			$data['validation'] = $this->validator;
			return view('admin/login', $data);
		} else {
			//berhasil login
			$username = $this->request->getPost('username');
			$user = $this->pegawaiModel->where('username', $username)->first();
			$data = [
				'id_pegawai' => $user['id_pegawai'],
				'nama_lengkap' => $user['nama_lengkap'],
				'username' => $user['username'],
				'no_hp' => $user['no_hp'],
				'alamat' => $user['alamat'],
			];
			session()->set($data);
			return redirect()->to(base_url() . '/admin');
		}
	}

	//=== Simpan session data pengguna
	private function simpanSession($user)
	{
		$data = [
			'id_pelanggan' => $user['id_pelanggan'],
			'nama_lengkap' => $user['nama_lengkap'],
			'email' => $user['email'],
			'no_hp' => $user['no_hp'],
			'alamat' => $user['alamat']
		];
		session()->set($data);
		return true;
	}

	public function logout()
	{
		session()->destroy();
		return redirect()->to(base_url() . '/login');
	}
}
