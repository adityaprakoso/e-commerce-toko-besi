<?php

namespace App\Controllers\admin;

use App\Controllers\BaseController;
use App\Models\BarangModel;
use App\Models\DetailPembelianModel;
use App\Models\PembelianModel;
use App\Models\ReturModel;
use App\Models\DetailReturModel;

class Pembelian extends BaseController
{

	public function __construct()
	{
		$this->barangModel = new BarangModel();
		$this->pembelianModel = new PembelianModel();
		$this->returModel = new ReturModel();
		$this->detailpembelianModel = new DetailPembelianModel();
		$this->detailReturModel = new DetailReturModel();
	}

	public function index()
	{
		if (!isset($_SESSION['username'])) {
			return redirect()->to(base_url() . '/admin/login');
		}
		$data = [
			'title' => 'Barang',
			'barang' => $this->barangModel->getTable('tb_barang'),
			'merk' => $this->barangModel->getTable('tb_merk'),
			'kategori' => $this->barangModel->getTable('tb_kategori_barang')
		];
		return view('admin/pembelian', $data);
	}

	public function barang_supplier($id_supplier)
	{
		$data = [
			'title' => 'Barang Supplier',
			'barang' => $this->barangModel->getTable('tb_barang', 'id_supplier', $id_supplier),
			'supplier' => $this->barangModel->getTable('tb_supplier', 'id_supplier', $id_supplier),
			'merk' => $this->barangModel->getTable('tb_merk'),
			'kategori' => $this->barangModel->getTable('tb_kategori_barang')
		];
		return view('admin/pembelian', $data);
	}

	public function tambah()
	{
		//=== Simpan Gambar ===//
		$file = $this->request->getFile('file_gambar');

		$nama_file = $file->getName();

		//=== Insert Data Barang ===//
		$this->barangModel->save([
			'id_supplier' => $this->request->getVar('id_supplier'),
			'nama_barang' => $this->request->getVar('nama_barang'),
			'id_merk' => $this->request->getVar('merk_barang'),
			'id_kategori_barang' => $this->request->getVar('kategori_barang'),
			'harga_beli' => $this->request->getVar('harga_beli'),
			'harga_jual' => $this->request->getVar('harga_jual'),
			'satuan' => $this->request->getVar('satuan'),
			'stok' => 0,
			'jumlah_terjual' => 0,
			'minimal_pembelian' => $this->request->getVar('minimal_pembelian'),
			'gambar' => $nama_file,
			'deskripsi' => $this->request->getVar('deskripsi')
		]);

		$file->move('./upload');

		session()->setFlashdata('swal', 'ditambah');
		return redirect()->to(base_url() . '/pembelian/' . $this->request->getVar('id_supplier'));
	}

	public function ubah()
	{
		$id_barang = $this->request->getVar('id_barang');
		$file = $this->request->getFile('file_gambar');
		$nama_file = $file->getName();

		$data = [
			'nama_barang' => $this->request->getVar('nama_barang'),
			'id_merk' => $this->request->getVar('merk_barang'),
			'id_kategori_barang' => $this->request->getVar('kategori_barang'),
			'harga_beli' => $this->request->getVar('harga_beli'),
			'harga_jual' => $this->request->getVar('harga_jual'),
			'stok' => $this->request->getVar('stok'),
			'satuan' => $this->request->getVar('satuan'),
			'minimal_pembelian' => $this->request->getVar('minimal_pembelian'),
			'deskripsi' => $this->request->getVar('deskripsi')
		];

		if ($nama_file != "") {
			$file->move('./upload');

			$gambar = [
				'gambar' => $nama_file
			];
			$data = array_merge($data, $gambar);
		}

		$this->barangModel->update($id_barang, $data);

		session()->setFlashdata('swal', 'diubah');
		return redirect()->to(base_url() . '/pembelian');
	}

	public function hapus($id)
	{
		$this->pembelianModel->delete($id);
		$this->barangModel->delete($id);
		session()->setFlashdata('swal', 'dihapus');
		return redirect()->to(base_url() . '/pembelian');
	}

	public function getIdPembelian($id_supplier)
	{
		$temp = $id_supplier . date("Ymd");
		$id = $this->pembelianModel->getIdPembelian($temp);

		if ($id[0]['id_pembelian'] == null) {
			$temp = $temp . 0;
		} else {
			$temp = $id[0]['id_pembelian'];
		}
		return $temp;
	}

	public function stok()
	{
		$id_barang = $this->request->getVar('id_barang');
		$id_supplier = $this->request->getVar('id_supplier');
		$stok = $this->request->getVar('stok');
		$stok += $this->request->getVar('jumlah');
		$total_beli = $this->request->getVar('jumlah') * $this->request->getVar('harga_beli');
		$total_bayar = $this->request->getVar('total_bayar'); // kondisi sudah di diskon
		$diskon = $total_beli * ($this->request->getVar('diskon') / 100);


		//=== Insert Data Pembelian ===//

		$id_pembelian = $this->getIdPembelian($id_supplier);
		// dd($id_pembelian);

		//cek id_pembelian sudah ada pada tgl tsbt
		$cek_data = $this->pembelianModel->where('id_pembelian', $id_pembelian)->first();

		// kalau belum ada
		if (!$cek_data) {
			$ppn = ($total_bayar * 0.1);
			$harga_akhir = $total_bayar + $ppn;
			$this->pembelianModel->insert([
				'id_pembelian' => $id_pembelian,
				'tanggal' => date('Y-m-d'),
				'total_bayar' => $total_bayar,
				'ppn' => $ppn,
				'harga_akhir' => $harga_akhir,
				'id_pegawai' => session()->get('id_pegawai'),
				'faktur' => 'Belum',
			]);

			$this->detailpembelianModel->save([
				'id_pembelian' => $id_pembelian,
				'id_barang' => $id_barang,
				'jumlah' => $this->request->getVar('jumlah'),
				'diskon' => $diskon,
				'harga' => $total_bayar,
			]);
		} else {
			#cek sudah dicetak belum
			if ($cek_data['faktur'] == 'Cetak') {
				$id_pembelian = $cek_data['id_pembelian'] + 1;

				$ppn = ($total_bayar * 0.1);
				$harga_akhir = $total_bayar + $ppn;
				$this->pembelianModel->insert([
					'id_pembelian' => $id_pembelian,
					'tanggal' => date('Y-m-d'),
					'total_bayar' => $total_bayar,
					'ppn' => $ppn,
					'harga_akhir' => $harga_akhir,
					'id_pegawai' => session()->get('id_pegawai'),
					'faktur' => 'Belum',
				]);

				$this->detailpembelianModel->save([
					'id_pembelian' => $id_pembelian,
					'id_barang' => $id_barang,
					'jumlah' => $this->request->getVar('jumlah'),
					'diskon' => $diskon,
					'harga' => $total_bayar,
				]);
			} else {

				$id_pembelian = $cek_data['id_pembelian'];
				$this->detailpembelianModel->save([
					'id_pembelian' => $id_pembelian,
					'id_barang' => $id_barang,
					'jumlah' => $this->request->getVar('jumlah'),
					'diskon' => $diskon,
					'harga' => $total_bayar,
				]);

				$data = $this->pembelianModel->where('id_pembelian', $id_pembelian)->first();
				$total_harga = $total_bayar += $data['total_bayar'];
				$ppn = ($total_bayar * 0.1);
				$harga_akhir = $total_bayar + $ppn;

				$updatePembelian = array(
					'id_pembelian' => $data['id_pembelian'],
					'total_bayar' => $total_harga,
					'ppn' => $total_bayar * 0.1,
					'harga_akhir' => $harga_akhir,
				);
				$this->pembelianModel->save($updatePembelian);
			}
		}

		//=== Update Stok dan Harga ===//
		$data = [
			'harga_beli' => $this->request->getVar('harga_beli'),
			'harga_jual' => $this->request->getVar('harga_jual'),
			'stok' => $stok
		];
		$this->barangModel->update($id_barang, $data);

		session()->setFlashdata('swal', 'stok');
		return redirect()->to(base_url() . '/pembelian/' . $this->request->getVar('id_supplier'));
	}

	public function retur()
	{
		$id_barang = $this->request->getVar('id_barang');
		$id_supplier = $this->request->getVar('id_supplier');
		$stok = $this->request->getVar('stok');
		$stok -= $this->request->getVar('jumlah');
		$keterangan = $this->request->getVar('keterangan');
		$total_retur = $this->request->getVar('total_retur');

		//=== Insert Data Retur ===//
		$id_retur = $id_supplier . date("Ymd");
		$cek_data = $this->returModel->where('id_retur', $id_retur)->first();

		if (!$cek_data) {
			$this->returModel->insert([
				'id_retur' => $id_retur,
				'id_pegawai' => session()->get('id_pegawai'),
				'tanggal' => date('Y-m-d'),
				'total' => $total_retur,
				'id_supplier' => $id_supplier,
			]);

			$this->detailReturModel->save([
				'id_retur' => $id_retur,
				'id_barang' => $id_barang,
				'jumlah' => $this->request->getVar('jumlah'),
				'pengembalian' => $total_retur,
				'keterangan' => $keterangan,
			]);
		} else {
			$this->detailReturModel->save([
				'id_retur' => $id_retur,
				'id_barang' => $id_barang,
				'jumlah' => $this->request->getVar('jumlah'),
				'pengembalian' => $total_retur,
				'keterangan' => $keterangan,
			]);

			$data = $this->returModel->getData($id_retur);
			$total = $total_retur += $data[0]['total'];
			$updateretur = array(
				'id_retur' => $data[0]['id_retur'],
				'total' => $total,
			);
			$this->returModel->save($updateretur);
		}

		//=== Update Stok===//
		$data = [
			'stok' => $stok
		];
		$this->barangModel->update($id_barang, $data);

		session()->setFlashdata('swal', 'diretur');
		return redirect()->to(base_url() . '/pembelian/' . $this->request->getVar('id_supplier'));
	}

	public function detail()
	{
		$id = $this->request->getVar('id_barang');
		$status = $this->request->getVar('status');
		$value = $this->barangModel->getTable('tb_barang', 'id_barang', $id);

		if ($status == 'stok') {

			foreach ($value as $value) :
				echo '<div id="id_barang">' . $value['id_barang'] . '</div>';
				echo '<div id="id_supplier">' . $value['id_supplier'] . '</div>';
				echo '<div id="nama_barang">' . $value['nama_barang'] . '</div>';
				echo '<div id="harga_beli">' . $value['harga_beli'] . '</div>';
				echo '<div id="harga_jual">' . $value['harga_jual'] . '</div>';
				echo '<div id="stok">' . $value['stok'] . '</div>';
				echo '<div id="status">' . $status . '</div>';
			endforeach;
		} elseif ($status == 'retur') {

			foreach ($value as $value) :
				echo '<div id="id_barang">' . $value['id_barang'] . '</div>';
				echo '<div id="id_supplier">' . $value['id_supplier'] . '</div>';
				echo '<div id="nama_barang">' . $value['nama_barang'] . '</div>';
				echo '<div id="harga_beli">' . $value['harga_beli'] . '</div>';
				echo '<div id="harga_jual">' . $value['harga_jual'] . '</div>';
				echo '<div id="stok">' . $value['stok'] . '</div>';
				echo '<div id="status">' . $status . '</div>';
			endforeach;
		} else {

			foreach ($value as $value) :
				echo '<div id="id_barang">' . $value['id_barang'] . '</div>';
				echo '<div id="nama_barang">' . $value['nama_barang'] . '</div>';
				echo '<div id="id_merk">' . $value['id_merk'] . '</div>';
				echo '<div id="kategori_barang">' . $value['id_kategori_barang'] . '</div>';
				echo '<div id="harga_beli">' . $value['harga_beli'] . '</div>';
				echo '<div id="harga_jual">' . $value['harga_jual'] . '</div>';
				echo '<div id="stok">' . $value['stok'] . '</div>';
				echo '<div id="satuan">' . $value['satuan'] . '</div>';
				echo '<div id="minimal_pembelian">' . $value['minimal_pembelian'] . '</div>';
				echo '<div id="gambar">' . $value['gambar'] . '</div>';
				echo '<div id="deskripsi">' . $value['deskripsi'] . '</div>';
			endforeach;
		}
	}
}
