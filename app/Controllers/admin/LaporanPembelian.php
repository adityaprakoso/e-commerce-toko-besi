<?php

namespace App\Controllers\admin;

use App\Controllers\BaseController;
use App\Models\PembelianModel;
use TCPDF;

class LaporanPembelian extends BaseController
{
	public function __construct()
	{
		$this->laporanPembelian = new PembelianModel();
	}
	public function index()
	{
		if (!isset($_SESSION['username'])) {
			return redirect()->to(base_url() . '/admin/login');
		}
		$data = [
			'title' => 'Laporan',
			'laporan' => $this->laporanPembelian->getData()
		];
		return view('admin/laporanPembelian', $data);
	}

	public function cetak()
	{

		$id = base64_decode($this->request->getVar('id'));

		$data = array(
			'faktur' => 'Cetak',
		);
		$this->laporanPembelian->update($id, $data);

		$transaksi = $this->laporanPembelian->getData($id);
		$detail = $this->laporanPembelian->detailPembelian($id);

		$html = view('admin/layout/invoice', [
			'detail' => $detail,
			'transaksi' => $transaksi,
		]);

		$pdf = new TCPDF('L', PDF_UNIT, 'A4', true, 'UTF-8', false);

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('TB.DADI MAKMUR');
		$pdf->SetTitle('Invoice Pembelian');
		$pdf->SetSubject('Invoice');

		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
		$pdf->setFooterData(array(0, 64, 0), array(0, 64, 128));

		$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$pdf->AddPage();

		$pdf->writeHTML($html, true, false, true, false, '');
		$this->response->setContentType('application/pdf');
		$pdf->Output('invoice.pdf', 'I');
	}
}
