<?php

namespace App\Controllers\admin;

use App\Controllers\BaseController;
use App\Models\SupplierModel;

class Supplier extends BaseController
{
	protected $barangModel;

	public function __construct()
	{
		$this->supplierModel = new SupplierModel();
	}

	public function index()
	{
		if (!isset($_SESSION['username'])) {
			return redirect()->to(base_url() . '/admin/login');
		}
		$data = [
			'title' => 'Supplier',
			'supplier' => $this->supplierModel->getValue('tb_supplier')
		];
		return view('admin/supplier', $data);
	}

	public function daftar_supplier()
	{

		$data = [
			'title' => 'Supplier',
			'barang' => $this->supplierModel->getValue('tb_supplier')
		];
		return view('admin/daftarSupplier', $data);
	}

	public function tambah()
	{
		//=== Insert Data Supplier ===//
		$this->supplierModel->save([
			'nama_supplier' => $this->request->getVar('nama_supplier'),
			'no_hp' => $this->request->getVar('no_hp'),
			'alamat' => $this->request->getVar('alamat')
		]);

		session()->setFlashdata('swal', 'ditambah');
		return redirect()->to(base_url() . '/daftarsupplier');
	}

	public function ubah()
	{
		$id_supplier = $this->request->getVar('id_supplier_ubah');

		$data = [
			'nama_supplier' => $this->request->getVar('nama_supplier'),
			'no_hp' => $this->request->getVar('no_hp'),
			'alamat' => $this->request->getVar('alamat')
		];
		$this->supplierModel->update($id_supplier, $data);

		session()->setFlashdata('swal', 'diubah');
		return redirect()->to(base_url() . '/daftarsupplier');
	}

	public function hapus($id)
	{
		$this->supplierModel->delete($id);
		session()->setFlashdata('swal', 'dihapus');
		return redirect()->to(base_url() . '/daftarsupplier');
	}

	public function detail()
	{
		$id = $this->request->getVar('id_supplier');
		// dd($this->request->getVar());
		$value = $this->supplierModel->getValue('tb_supplier', 'id_supplier', $id);

		foreach ($value as $value) :
			echo '<div id="id_supplier">' . $value['id_supplier'] . '</div>';
			echo '<div id="nama_supplier">' . $value['nama_supplier'] . '</div>';
			echo '<div id="no_hp">' . $value['no_hp'] . '</div>';
			echo '<div id="alamat">' . $value['alamat'] . '</div>';
		endforeach;
	}

	//--------------------------------------------------------------------

}
