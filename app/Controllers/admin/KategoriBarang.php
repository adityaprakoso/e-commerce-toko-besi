<?php

namespace App\Controllers\admin;

use App\Controllers\BaseController;
use App\Models\KategoriModel;

class KategoriBarang extends BaseController
{
	protected $kategoriModel;

	public function __construct()
	{
		$this->kategoriModel = new KategoriModel();
	}

	public function index()
	{
		if (!isset($_SESSION['username'])) {
			return redirect()->to(base_url() . '/admin/login');
		}
		$data = [
			'title' => 'Kategori Barang',
			'kategori' => $this->kategoriModel->getValue('tb_kategori_barang')
		];

		return view('admin/kategoriBarang', $data);
	}

	public function tambah()
	{
		$this->kategoriModel->save([
			'nama_kategori' => $this->request->getVar('nama')
		]);
		session()->setFlashdata('swal', 'ditambah');
		return redirect()->to(base_url() . '/kategoribarang');
	}

	public function ubah()
	{
		$id_kategori = $this->request->getVar('id_ubah');
		$data = [
			'nama_kategori' => $this->request->getVar('nama_ubah')
		];
		$this->kategoriModel->update($id_kategori, $data);

		session()->setFlashdata('swal', 'diubah');
		return redirect()->to(base_url() . '/kategoribarang');
	}

	public function hapus($id)
	{
		$this->kategoriModel->delete($id);
		session()->setFlashdata('swal', 'dihapus');
		return redirect()->to(base_url() . '/kategoribarang');
	}

	public function detail()
	{
		$id = $this->request->getVar('id_kategori');
		$value = $this->kategoriModel->getValue('tb_kategori_barang', $id);

		echo '<div id="id_kategori">' . $value['id_kategori'] . '</div>';
		echo '<div id="nama_kategori">' . $value['nama_kategori'] . '</div>';
	}

	//--------------------------------------------------------------------

}
