<?php

namespace App\Controllers\admin;

use App\Controllers\BaseController;
use App\Models\PelangganModel;
use App\Models\PembelianModel;
use App\Models\SupplierModel;
use App\Models\TransaksiModel;

class Home extends BaseController
{
	public function __construct()
	{
		$this->pelangganModel = new PelangganModel();
		$this->supplierModel = new SupplierModel();
		$this->transaksiModel = new TransaksiModel();
		$this->pembelianModel = new PembelianModel();
	}

	public function index()
	{
		if (!isset($_SESSION['username'])) {
			return redirect()->to(base_url() . '/admin/login');
		}

		$data = [
			'title' => 'Beranda',
			'pelanggan' => $this->pelangganModel->getPelanggan(),
			'supplier' => $this->supplierModel->getSupplier(),
			'pesanan' => $this->transaksiModel->getTransaksi('tb_transaksi'),
			'terkirim' => $this->transaksiModel->getTransaksi('tb_transaksi', 'Selesai'),
			'pemasukan_today' => $this->transaksiModel->getPemasukan('tb_transaksi', date('Y-m-d'), date('Y-m-d')),
			'pemasukan' => $this->transaksiModel->getPemasukan('tb_transaksi'),
			'pengeluaran_today' => $this->pembelianModel->getPengeluaran('tb_pembelian', date('Y-m-d')),
			'pengeluaran' => $this->pembelianModel->getPengeluaran('tb_pembelian'),
		];

		return view('admin/home', $data);
	}

	public function logout()
	{
		session()->destroy();
		return view('admin/login');
	}

	//--------------------------------------------------------------------

}
