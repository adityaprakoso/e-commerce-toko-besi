<?php

namespace App\Controllers\admin;

use App\Controllers\BaseController;
use App\Models\MerkModel;

class Merk extends BaseController
{
	protected $merkModel;

	public function __construct()
	{
		$this->merkModel = new MerkModel();
	}

	public function index()
	{
		if (!isset($_SESSION['username'])) {
			return redirect()->to(base_url() . '/admin/login');
		}
		$data = [
			'title' => 'Merk Barang',
			'merk' => $this->merkModel->getValue('tb_merk')
		];

		return view('admin/merk', $data);
	}

	public function tambah()
	{

		$data = [
			'id_merk' => $this->request->getVar('id_merk'),
			'nama_merk' => $this->request->getVar('nama')
		];

		$this->merkModel->insert($data);
		session()->setFlashdata('swal', 'ditambah');
		return redirect()->to(base_url() . '/merk');
	}

	public function tambahMerk()
	{
		$data = [
			'id_merk' => $this->request->getVar('kode'),
			'nama_merk' => $this->request->getVar('merk')
		];

		$this->merkModel->insert($data);
	}

	public function ubah()
	{
		$id_kategori = $this->request->getVar('id_ubah');
		$data = [
			'nama_merk' => $this->request->getVar('nama_ubah')
		];
		$this->merkModel->update($id_kategori, $data);

		session()->setFlashdata('swal', 'diubah');
		return redirect()->to(base_url() . '/merk');
	}

	public function hapus($id)
	{
		$this->merkModel->delete($id);
		session()->setFlashdata('swal', 'dihapus');
		return redirect()->to(base_url() . '/merk');
	}

	public function detail()
	{
		$id = $this->request->getVar('id_merk');
		$value = $this->merkModel->getValue('tb_merk', $id);

		echo '<div id="id_merk">' . $value[0]['id_merk'] . '</div>';
		echo '<div id="nama_merk">' . $value[0]['nama_merk'] . '</div>';
	}

	//--------------------------------------------------------------------

}
