<?php

namespace App\Controllers\admin;

use App\Controllers\BaseController;
use App\Models\PegawaiModel;

class Pegawai extends BaseController
{
	protected $pegawaiModel;

	public function __construct()
	{
		$this->pegawaiModel = new PegawaiModel();
	}

	public function index()
	{
		if (!isset($_SESSION['username'])) {
			return redirect()->to(base_url() . '/admin/login');
		}
		$data = [
			'title' => 'Pegawai',
			'pegawai' => $this->pegawaiModel->getValue('tb_pegawai')
		];

		return view('admin/pegawai', $data);
	}

	public function tambah()
	{
		$data = $this->request->getPost();

		// dd($data);
		$this->pegawaiModel->save($data);

		session()->setFlashdata('swal', 'ditambah');
		return redirect()->to(base_url() . '/pegawai');
	}

	public function ubah()
	{
		$id_pegawai = $this->request->getPost('id_pegawai');
		$data = [
			'nama_lengkap' => $this->request->getPost('nama_lengkap'),
			'username' => $this->request->getPost('username'),
			'alamat' => $this->request->getPost('alamat'),
			'no_hp' => $this->request->getPost('no_hp')
		];
		$this->pegawaiModel->update($id_pegawai, $data);

		session()->setFlashdata('swal', 'diubah');
		return redirect()->to(base_url() . '/pegawai');
	}

	public function hapus($id)
	{
		$this->pegawaiModel->delete($id);
		session()->setFlashdata('swal', 'dihapus');
		return redirect()->to(base_url() . '/pegawai');
	}

	public function detail()
	{
		$id = $this->request->getVar('id_pegawai');
		$value = $this->pegawaiModel->getValue('tb_pegawai', $id);

		echo '<div id="id_pegawai">' . $value['id_pegawai'] . '</div>';
		echo '<div id="nama_lengkap">' . $value['nama_lengkap'] . '</div>';
		echo '<div id="username">' . $value['username'] . '</div>';
		echo '<div id="alamat">' . $value['alamat'] . '</div>';
		echo '<div id="no_hp">' . $value['no_hp'] . '</div>';
	}

	//--------------------------------------------------------------------

}
