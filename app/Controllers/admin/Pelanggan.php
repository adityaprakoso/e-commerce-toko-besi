<?php

namespace App\Controllers\admin;

use App\Controllers\BaseController;
use App\Models\PelangganModel;

class Pelanggan extends BaseController
{
	protected $pelangganModel;

	public function __construct()
	{
		$this->pelangganModel = new PelangganModel();
	}

	public function index()
	{
		if (!isset($_SESSION['username'])) {
			return redirect()->to(base_url() . '/admin/login');
		}
		$data = [
			'title' => 'Pelanggan',
			'pelanggan' => $this->pelangganModel->getValue('tb_pelanggan')
		];

		return view('admin/pelanggan', $data);
	}

	public function tambah()
	{
		$this->pelangganModel->save([
			'nama_kategori' => $this->request->getVar('nama')
		]);
		session()->setFlashdata('swal', 'ditambah');
		return redirect()->to(base_url() . '/kategoribarang');
	}

	public function ubah()
	{
		$id_kategori = $this->request->getVar('id_ubah');
		$data = [
			'nama_kategori' => $this->request->getVar('nama_ubah')
		];
		$this->pelangganModel->update($id_kategori, $data);

		session()->setFlashdata('swal', 'diubah');
		return redirect()->to(base_url() . '/pelanggan');
	}

	public function hapus($id)
	{
		$this->pelangganModel->delete($id);
		session()->setFlashdata('swal', 'dihapus');
		return redirect()->to(base_url() . '/pelanggan');
	}
}
