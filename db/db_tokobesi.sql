-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 08 Des 2020 pada 15.28
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_tokobesi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_barang`
--

CREATE TABLE `tb_barang` (
  `id_barang` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `nama_barang` varchar(60) NOT NULL,
  `id_merk` varchar(25) NOT NULL,
  `id_kategori_barang` int(11) NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `satuan` varchar(25) NOT NULL,
  `stok` int(11) NOT NULL,
  `jumlah_terjual` int(11) DEFAULT NULL,
  `minimal_pembelian` int(11) NOT NULL,
  `gambar` text NOT NULL,
  `deskripsi` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_barang`
--

INSERT INTO `tb_barang` (`id_barang`, `id_supplier`, `nama_barang`, `id_merk`, `id_kategori_barang`, `harga_beli`, `harga_jual`, `satuan`, `stok`, `jumlah_terjual`, `minimal_pembelian`, `gambar`, `deskripsi`, `created_at`, `updated_at`) VALUES
(3, 2, 'Pipa PVC 1/2 In', 'rck', 1, 14000, 15000, 'pcs', 100, 20, 5, 'Pipa1.jpg', 'PIpa pvc kualitas terbaik, tahan lama. Terbuat dari bahan uPVC (Unplastized Polyvinyl Chloride) yang memiliki banyak keunggulan dibandingkan dengan bahan polymeric lainnya, seperti: Calcium Zinc (Tanpa Timbal), tahan terhadap korosi, kuat, ringan, mudah disambungkan dan dirawat. RUCIKA STANDARD diproduksi sesuai dengan standar JIS dan ISO dengan sertifikasi sistem manajemen mutu ISO 9001: 2000.', '2020-08-03 10:30:50', '2020-11-27 00:15:30'),
(4, 2, 'Pipa PVC 3/4 In', 'rck', 1, 30000, 32000, 'pcs', 87, 8, 5, 'Pipa2.jpg', 'Terbuat dari bahan uPVC (Unplastized Polyvinyl Chloride) yang memiliki banyak keunggulan dibandingkan dengan bahan polymeric lainnya, seperti: Calcium Zinc (Tanpa Timbal), tahan terhadap korosi, kuat, ringan, mudah disambungkan dan dirawat. RUCIKA STANDARD diproduksi sesuai dengan standar JIS dan ISO dengan sertifikasi sistem manajemen mutu ISO 9001: 2000.', '2020-08-04 07:49:18', '2020-12-08 01:16:15'),
(8, 2, 'Pipa PVC 1 In', 'rck', 1, 42000, 44000, 'pcs', 0, 0, 5, 'Pipa3.jpg', 'RUCIKA STANDARD adalah sistem perpipaan air dan pipa limbah. Terbuat dari bahan uPVC (Unplastized Polyvinyl Chloride) yang memiliki banyak keunggulan dibandingkan dengan bahan polymeric lainnya, seperti: Calcium Zinc (Tanpa Timbal), tahan terhadap korosi, kuat, ringan, mudah disambungkan dan dirawat. RUCIKA STANDARD diproduksi sesuai dengan standar JIS dan ISO dengan sertifikasi sistem manajemen mutu ISO 9001: 2000.', '2020-09-17 02:10:00', '2020-11-26 23:31:04'),
(9, 2, 'Pipa PVC 2 In', 'rck', 1, 92000, 95000, 'pcs', 0, 1, 2, 'Pipa4.jpg', 'RUCIKA STANDARD adalah sistem perpipaan air dan pipa limbah. Terbuat dari bahan uPVC (Unplastized Polyvinyl Chloride) yang memiliki banyak keunggulan dibandingkan dengan bahan polymeric lainnya, seperti: Calcium Zinc (Tanpa Timbal), tahan terhadap korosi, kuat, ringan, mudah disambungkan dan dirawat. RUCIKA STANDARD diproduksi sesuai dengan standar JIS dan ISO dengan sertifikasi sistem manajemen mutu ISO 9001: 2000.', '2020-09-17 02:12:28', '2020-11-26 23:31:16'),
(11, 4, 'Cat Tembok / Putih / 1Kg', 'vlx', 6, 34000, 38000, 'pcs', 100, 0, 5, 'cat1.jpg', 'Vinilex (with Silver-Ion) merupakan cat tembok berkualitas tinggi yang diformulasikan khusus dengan teknologi Silver-Ion yang dapat membantu melindungi secara efektif dari berbagai jenis kuman seperti MRSA, E-Coli dan Staphylococcus Aureus untuk mencegah asma, flu, dan diare. Selain itu, Vinilex (with Silver-Ion) juga mempunyai keunggulan lain yaitu cepat kering dan minimal cipratan. Tersedia lebih dari 2.000 pilihan warna menarik untuk dinding interior, exterior, dan langit-langit berbahan dasar asbes, batako, beton, papan, dan mortar.', '2020-09-17 02:33:05', '2020-10-22 05:07:55'),
(12, 4, 'Cat Tembok / Putih / 5 Kg', 'vlx', 6, 115000, 120000, 'pcs', 0, 45, 2, 'cat2.jpg', 'Vinilex (with Silver-Ion) merupakan cat tembok berkualitas tinggi yang diformulasikan khusus dengan teknologi Silver-Ion yang dapat membantu melindungi secara efektif dari berbagai jenis kuman seperti MRSA, E-Coli dan Staphylococcus Aureus untuk mencegah asma, flu, dan diare. Selain itu, Vinilex (with Silver-Ion) juga mempunyai keunggulan lain yaitu cepat kering dan minimal cipratan. Tersedia lebih dari 2.000 pilihan warna menarik untuk dinding interior, exterior, dan langit-langit berbahan dasar asbes, batako, beton, papan, dan mortar.', '2020-09-17 02:36:16', '2020-11-26 23:15:38'),
(13, 4, 'Cat Tembok / Putih / 25 Kg', 'vlx', 6, 580000, 590000, 'pcs', 0, 30, 2, 'cat3.jpg', 'Vinilex (with Silver-Ion) merupakan cat tembok berkualitas tinggi yang diformulasikan khusus dengan teknologi Silver-Ion yang dapat membantu melindungi secara efektif dari berbagai jenis kuman seperti MRSA, E-Coli dan Staphylococcus Aureus untuk mencegah asma, flu, dan diare. Selain itu, Vinilex (with Silver-Ion) juga mempunyai keunggulan lain yaitu cepat kering dan minimal cipratan. Tersedia lebih dari 2.000 pilihan warna menarik untuk dinding interior, exterior, dan langit-langit berbahan dasar asbes, batako, beton, papan, dan mortar.', '2020-09-17 02:37:36', '2020-11-26 23:17:06'),
(14, 5, 'Besi Beton 8 TJ SNI', 'tj', 7, 41000, 42000, 'pcs', 0, 0, 5, 'besi8.jpg', 'Besi beton merk TJ SNI murah ini merupakan jenis besi beton berkualitas dengan standar mutu tinggi. Hal ini dibuktikan dengan memiliki sertifikasi SNI, yang berarti mempunyai diameter dan panjang yang sesuai dengan ukurannya. Kualitasnya sendiri tidaklah kalah dengan besi beton merk PSNI atau yang lainnya.', '2020-11-27 00:19:48', '2020-11-27 00:19:48'),
(15, 5, 'Besi Beton 10 TJ SNI', 'tj', 7, 63000, 68000, 'pcs', 0, 0, 5, 'Besi10.jpg', 'Besi beton merk TJ SNI murah ini merupakan jenis besi beton berkualitas dengan standar mutu tinggi. Hal ini dibuktikan dengan memiliki sertifikasi SNI, yang berarti mempunyai diameter dan panjang yang sesuai dengan ukurannya. Kualitasnya sendiri tidaklah kalah dengan besi beton merk PSNI atau yang lainnya.', '2020-11-27 00:22:04', '2020-11-27 00:22:04'),
(16, 5, 'Besi Beton 12 TJ SNI', 'tj', 7, 83000, 90000, 'pcs', 0, 0, 5, 'Besi12.jpg', 'Besi beton merk TJ SNI murah ini merupakan jenis besi beton berkualitas dengan standar mutu tinggi. Hal ini dibuktikan dengan memiliki sertifikasi SNI, yang berarti mempunyai diameter dan panjang yang sesuai dengan ukurannya. Kualitasnya sendiri tidaklah kalah dengan besi beton merk PSNI atau yang lainnya', '2020-11-27 00:25:19', '2020-11-27 00:25:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_detail_pembelian`
--

CREATE TABLE `tb_detail_pembelian` (
  `id_detail_pembelian` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `diskon` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_detail_pembelian`
--

INSERT INTO `tb_detail_pembelian` (`id_detail_pembelian`, `id_pembelian`, `id_barang`, `jumlah`, `diskon`, `harga`) VALUES
(1, 220201126, 3, 70, 49000, 931000),
(3, 220201126, 4, 100, 150000, 2850000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_detail_retur`
--

CREATE TABLE `tb_detail_retur` (
  `id_detail_retur` int(11) NOT NULL,
  `id_retur` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `pengembalian` int(11) NOT NULL,
  `keterangan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_detail_retur`
--

INSERT INTO `tb_detail_retur` (`id_detail_retur`, `id_retur`, `id_barang`, `jumlah`, `pengembalian`, `keterangan`) VALUES
(1, 420200925, 11, 1, 34000, 'Segel rusak'),
(2, 420200925, 12, 1, 115000, 'Bocor'),
(3, 220201122, 3, 3, 42000, 'Pecah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_detail_transaksi`
--

CREATE TABLE `tb_detail_transaksi` (
  `id_detail` int(11) NOT NULL,
  `order_id` varchar(10) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_detail_transaksi`
--

INSERT INTO `tb_detail_transaksi` (`id_detail`, `order_id`, `id_barang`, `jumlah`) VALUES
(1, '1457419629', 4, 2),
(2, '1744576116', 9, 1),
(3, '1970534992', 4, 1),
(4, '183848809', 4, 1),
(5, '240198541', 4, 5),
(6, '555603844', 4, 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kategori_barang`
--

CREATE TABLE `tb_kategori_barang` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kategori_barang`
--

INSERT INTO `tb_kategori_barang` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Pipa PVC'),
(6, 'Cat'),
(7, 'Besi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_merk`
--

CREATE TABLE `tb_merk` (
  `id_merk` varchar(25) CHARACTER SET latin1 NOT NULL,
  `nama_merk` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_merk`
--

INSERT INTO `tb_merk` (`id_merk`, `nama_merk`) VALUES
('rck', 'Rucika'),
('tj', 'tj'),
('vlx', 'Vinilex');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pegawai`
--

CREATE TABLE `tb_pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` text NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pegawai`
--

INSERT INTO `tb_pegawai` (`id_pegawai`, `nama_lengkap`, `username`, `password`, `alamat`, `no_hp`, `created_at`, `updated_at`) VALUES
(5, 'Adit', 'admin', '$2y$10$xS0htrbIbw/5lifrO7/D0uKmyEYTEh9jzyqeX88IfEOgAP3arcHd6', 'Jalan Mangkubumi No.102', '082212312312', '2020-07-29 03:16:07', '2020-09-25 20:34:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pelanggan`
--

CREATE TABLE `tb_pelanggan` (
  `id_pelanggan` int(11) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` text NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pelanggan`
--

INSERT INTO `tb_pelanggan` (`id_pelanggan`, `nama_lengkap`, `email`, `password`, `alamat`, `no_hp`, `created_at`, `updated_at`) VALUES
(5, 'Muhammad', 'emrafi28@gmail.com', '$2y$10$w6x6tzX9LhrtnZUneDPgjOGj/pNEKzDuyWsduWpO54nS/rP2CTP.a', 'Jogja', '02745668908', '2020-10-07 00:03:21', '2020-10-07 00:03:21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pembayaran`
--

CREATE TABLE `tb_pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `status_code` varchar(3) NOT NULL,
  `status_message` varchar(50) NOT NULL,
  `transaction_id` varchar(100) NOT NULL,
  `order_id` varchar(10) NOT NULL,
  `gross_amount` decimal(20,2) NOT NULL,
  `payment_type` varchar(40) DEFAULT NULL,
  `transaction_time` datetime DEFAULT NULL,
  `transaction_status` varchar(40) DEFAULT NULL,
  `bank` varchar(40) DEFAULT NULL,
  `va_number` varchar(40) DEFAULT NULL,
  `fraud_status` varchar(40) DEFAULT NULL,
  `bca_va_number` varchar(40) DEFAULT NULL,
  `permata_va_number` varchar(40) DEFAULT NULL,
  `pdf_url` varchar(200) NOT NULL,
  `finish_redirect_url` varchar(200) NOT NULL,
  `bill_key` varchar(20) DEFAULT NULL,
  `biller_code` varchar(10) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pembayaran`
--

INSERT INTO `tb_pembayaran` (`id_pembayaran`, `status_code`, `status_message`, `transaction_id`, `order_id`, `gross_amount`, `payment_type`, `transaction_time`, `transaction_status`, `bank`, `va_number`, `fraud_status`, `bca_va_number`, `permata_va_number`, `pdf_url`, `finish_redirect_url`, `bill_key`, `biller_code`, `created_at`, `updated_at`) VALUES
(1, '201', 'Transaksi sedang diproses', 'f8f75215-d24f-44bd-806a-8c8b93f3141c', '1457419629', '64000.00', 'bank_transfer', '2020-11-05 19:42:55', 'Settlement', 'bca', '92104658913', 'accept', '92104658913', '-', 'https://app.sandbox.midtrans.com/snap/v1/transactions/4eb40dff-f85c-42e5-8a2c-224023a01312/pdf', '?order_id=1457419629&status_code=201&transaction_status=pending', '-', '-', '2020-11-05 06:42:57', '2020-11-05 06:45:50'),
(2, '201', 'Transaksi sedang diproses', 'e9907140-49fe-42cc-8af9-a98ff5ff9221', '1744576116', '95000.00', 'echannel', '2020-11-05 22:10:01', 'pending', '-', '-', 'accept', '-', '-', 'https://app.sandbox.midtrans.com/snap/v1/transactions/8d345735-dedd-43b3-99d0-6608f19e26ab/pdf', '?order_id=1744576116&status_code=201&transaction_status=pending', '558877224932', '70012', '2020-11-05 09:10:04', '2020-11-05 09:10:04'),
(3, '201', 'Transaksi sedang diproses', 'a7ccfe36-b6e8-4c26-b568-6f047d59ce51', '1970534992', '32000.00', 'bank_transfer', '2020-11-08 06:17:29', 'Failed', 'bca', '92104137942', 'accept', '92104137942', '-', 'https://app.sandbox.midtrans.com/snap/v1/transactions/2beb461a-484f-40e6-866c-b1c026d847bf/pdf', '?order_id=1970534992&status_code=201&transaction_status=pending', '-', '-', '2020-11-07 17:17:27', '2020-11-07 17:40:55'),
(4, '201', 'Transaksi sedang diproses', '62ca8ed8-a484-4e87-9a88-188c286ef943', '183848809', '32000.00', 'echannel', '2020-11-17 21:14:06', 'pending', '-', '-', 'accept', '-', '-', 'https://app.sandbox.midtrans.com/snap/v1/transactions/3d76ff42-b06e-4633-a6ac-f1f2c5214731/pdf', '?order_id=183848809&status_code=201&transaction_status=pending', '67602360115', '70012', '2020-11-17 08:14:10', '2020-11-17 08:14:10'),
(5, '201', 'Transaksi sedang diproses', 'b2203279-2dd1-4344-a68e-133a20ee8cf3', '240198541', '160000.00', 'bank_transfer', '2020-11-30 22:04:51', 'pending', 'bca', '92104697030', 'accept', '92104697030', '-', 'https://app.sandbox.midtrans.com/snap/v1/transactions/c7186994-e805-45d1-ae57-58f0d5ed55d7/pdf', '?order_id=240198541&status_code=201&transaction_status=pending', '-', '-', '2020-11-30 09:04:45', '2020-11-30 09:04:45'),
(6, '201', 'Transaksi sedang diproses', '50db94f1-b51f-472e-9039-5558d42e03f6', '555603844', '256000.00', 'echannel', '2020-12-08 14:16:23', 'pending', '-', '-', 'accept', '-', '-', 'https://app.sandbox.midtrans.com/snap/v1/transactions/713b4d09-e4d2-487d-892e-f726b2c9b580/pdf', '?order_id=555603844&status_code=201&transaction_status=pending', '940151801090', '70012', '2020-12-08 01:16:15', '2020-12-08 01:16:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pembelian`
--

CREATE TABLE `tb_pembelian` (
  `id_pembelian` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `total_bayar` int(11) NOT NULL,
  `ppn` int(11) NOT NULL,
  `harga_akhir` int(11) NOT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pembelian`
--

INSERT INTO `tb_pembelian` (`id_pembelian`, `tanggal`, `total_bayar`, `ppn`, `harga_akhir`, `id_pegawai`, `created_at`, `updated_at`) VALUES
(220201126, '2020-11-26', 3781000, 378100, 4159100, 5, '2020-11-26 23:37:33', '2020-11-26 23:57:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_retur`
--

CREATE TABLE `tb_retur` (
  `id_retur` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `total` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_retur`
--

INSERT INTO `tb_retur` (`id_retur`, `id_pegawai`, `tanggal`, `total`, `id_supplier`, `created_at`, `updated_at`) VALUES
(220201122, 5, '2020-11-22', 42000, 2, '2020-11-22 05:28:35', '2020-11-22 05:28:35'),
(420200925, 5, '2020-09-25', 149000, 4, '2020-09-25 05:22:14', '2020-09-25 05:26:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_supplier`
--

CREATE TABLE `tb_supplier` (
  `id_supplier` int(11) NOT NULL,
  `nama_supplier` varchar(100) NOT NULL,
  `no_hp` varchar(14) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_supplier`
--

INSERT INTO `tb_supplier` (`id_supplier`, `nama_supplier`, `no_hp`, `alamat`, `created_at`, `updated_at`) VALUES
(2, 'CV. Putra Sejati', '08533323222', 'Jl. Perintis Kemerdekaan No.23', '2020-07-19 07:50:10', '2020-07-19 07:50:10'),
(4, 'PT. Sinar Pewarna', '08777352811', 'Jl. Muja Muju No.100', '2020-09-17 02:15:11', '2020-09-17 02:15:11'),
(5, 'PT. Besi Tua', '089887322200', 'Jl. Halmahera Utara No.2 ', '2020-11-26 21:21:35', '2020-11-26 21:21:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_transaksi`
--

CREATE TABLE `tb_transaksi` (
  `transaction_id` varchar(100) NOT NULL,
  `order_id` varchar(10) NOT NULL,
  `id_pelanggan` int(11) DEFAULT NULL,
  `tanggal` date NOT NULL,
  `total_bayar` int(11) NOT NULL,
  `status` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_transaksi`
--

INSERT INTO `tb_transaksi` (`transaction_id`, `order_id`, `id_pelanggan`, `tanggal`, `total_bayar`, `status`, `created_at`, `updated_at`) VALUES
('50db94f1-b51f-472e-9039-5558d42e03f6', '555603844', 5, '2020-12-08', 256000, 'pending', '2020-12-08 01:16:14', '2020-12-08 01:16:14'),
('62ca8ed8-a484-4e87-9a88-188c286ef943', '183848809', 5, '2020-11-17', 32000, 'pending', '2020-11-17 08:14:10', '2020-11-17 20:46:05'),
('a7ccfe36-b6e8-4c26-b568-6f047d59ce51', '1970534992', 5, '2020-11-30', 32000, 'Cancel', '2020-11-07 17:17:27', '2020-11-17 20:46:05'),
('b2203279-2dd1-4344-a68e-133a20ee8cf3', '240198541', 5, '2020-11-30', 160000, 'pending', '2020-11-30 09:04:45', '2020-11-30 09:04:45'),
('e9907140-49fe-42cc-8af9-a98ff5ff9221', '1744576116', 5, '2020-11-30', 95000, 'Selesai', '2020-11-05 09:10:04', '2020-12-06 01:33:42'),
('f8f75215-d24f-44bd-806a-8c8b93f3141c', '1457419629', 5, '2020-12-07', 64000, 'Selesai', '2020-11-05 06:42:57', '2020-11-27 20:48:21');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_barang`
--
ALTER TABLE `tb_barang`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `id_supplier` (`id_supplier`),
  ADD KEY `id_kategori_barang` (`id_kategori_barang`),
  ADD KEY `id_merk` (`id_merk`);

--
-- Indeks untuk tabel `tb_detail_pembelian`
--
ALTER TABLE `tb_detail_pembelian`
  ADD PRIMARY KEY (`id_detail_pembelian`),
  ADD KEY `id_pembelian` (`id_pembelian`),
  ADD KEY `id_barang` (`id_barang`);

--
-- Indeks untuk tabel `tb_detail_retur`
--
ALTER TABLE `tb_detail_retur`
  ADD PRIMARY KEY (`id_detail_retur`),
  ADD KEY `id_retur` (`id_retur`),
  ADD KEY `id_barang` (`id_barang`);

--
-- Indeks untuk tabel `tb_detail_transaksi`
--
ALTER TABLE `tb_detail_transaksi`
  ADD PRIMARY KEY (`id_detail`),
  ADD KEY `id_barang` (`id_barang`),
  ADD KEY `tb_detail_transaksi_ibfk_2` (`order_id`);

--
-- Indeks untuk tabel `tb_kategori_barang`
--
ALTER TABLE `tb_kategori_barang`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indeks untuk tabel `tb_merk`
--
ALTER TABLE `tb_merk`
  ADD PRIMARY KEY (`id_merk`);

--
-- Indeks untuk tabel `tb_pegawai`
--
ALTER TABLE `tb_pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indeks untuk tabel `tb_pelanggan`
--
ALTER TABLE `tb_pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indeks untuk tabel `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`),
  ADD KEY `tb_pembayaran_ibfk_1` (`transaction_id`);

--
-- Indeks untuk tabel `tb_pembelian`
--
ALTER TABLE `tb_pembelian`
  ADD PRIMARY KEY (`id_pembelian`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indeks untuk tabel `tb_retur`
--
ALTER TABLE `tb_retur`
  ADD PRIMARY KEY (`id_retur`),
  ADD KEY `tb_retur_ibfk_1` (`id_pegawai`),
  ADD KEY `tb_retur_ibfk_2` (`id_supplier`);

--
-- Indeks untuk tabel `tb_supplier`
--
ALTER TABLE `tb_supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indeks untuk tabel `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  ADD PRIMARY KEY (`transaction_id`),
  ADD KEY `id_pelanggan` (`id_pelanggan`),
  ADD KEY `order_id` (`order_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_barang`
--
ALTER TABLE `tb_barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `tb_detail_pembelian`
--
ALTER TABLE `tb_detail_pembelian`
  MODIFY `id_detail_pembelian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_detail_retur`
--
ALTER TABLE `tb_detail_retur`
  MODIFY `id_detail_retur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_detail_transaksi`
--
ALTER TABLE `tb_detail_transaksi`
  MODIFY `id_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_kategori_barang`
--
ALTER TABLE `tb_kategori_barang`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tb_pegawai`
--
ALTER TABLE `tb_pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_pelanggan`
--
ALTER TABLE `tb_pelanggan`
  MODIFY `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  MODIFY `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_retur`
--
ALTER TABLE `tb_retur`
  MODIFY `id_retur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=420200926;

--
-- AUTO_INCREMENT untuk tabel `tb_supplier`
--
ALTER TABLE `tb_supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_barang`
--
ALTER TABLE `tb_barang`
  ADD CONSTRAINT `tb_barang_ibfk_1` FOREIGN KEY (`id_supplier`) REFERENCES `tb_supplier` (`id_supplier`),
  ADD CONSTRAINT `tb_barang_ibfk_2` FOREIGN KEY (`id_kategori_barang`) REFERENCES `tb_kategori_barang` (`id_kategori`),
  ADD CONSTRAINT `tb_barang_ibfk_3` FOREIGN KEY (`id_merk`) REFERENCES `tb_merk` (`id_merk`);

--
-- Ketidakleluasaan untuk tabel `tb_detail_pembelian`
--
ALTER TABLE `tb_detail_pembelian`
  ADD CONSTRAINT `tb_detail_pembelian_ibfk_1` FOREIGN KEY (`id_pembelian`) REFERENCES `tb_pembelian` (`id_pembelian`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_detail_pembelian_ibfk_2` FOREIGN KEY (`id_barang`) REFERENCES `tb_barang` (`id_barang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_detail_retur`
--
ALTER TABLE `tb_detail_retur`
  ADD CONSTRAINT `tb_detail_retur_ibfk_1` FOREIGN KEY (`id_retur`) REFERENCES `tb_retur` (`id_retur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_detail_retur_ibfk_2` FOREIGN KEY (`id_barang`) REFERENCES `tb_barang` (`id_barang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_detail_transaksi`
--
ALTER TABLE `tb_detail_transaksi`
  ADD CONSTRAINT `tb_detail_transaksi_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `tb_barang` (`id_barang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_detail_transaksi_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `tb_transaksi` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  ADD CONSTRAINT `tb_pembayaran_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `tb_transaksi` (`transaction_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_retur`
--
ALTER TABLE `tb_retur`
  ADD CONSTRAINT `tb_retur_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `tb_pegawai` (`id_pegawai`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_retur_ibfk_2` FOREIGN KEY (`id_supplier`) REFERENCES `tb_supplier` (`id_supplier`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  ADD CONSTRAINT `tb_transaksi_ibfk_1` FOREIGN KEY (`id_pelanggan`) REFERENCES `tb_pelanggan` (`id_pelanggan`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
